using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using MavenSocial.Droid;
using Android.Media;

[assembly: Dependency(typeof(NotificationSound_Android))]
namespace MavenSocial.Droid
{
  public class NotificationSound_Android : INotificationSound
  {
    public void Sound()
    {
      var player = new MediaPlayer();
      var fd = global::Android.App.Application.Context.Assets.OpenFd("beep2.mp3");
      player.Prepared += (s, e) =>
      {
        player.Start();
      };
      player.SetDataSource(fd.FileDescriptor, fd.StartOffset, fd.Length);
      player.Prepare();
    }
  }
}