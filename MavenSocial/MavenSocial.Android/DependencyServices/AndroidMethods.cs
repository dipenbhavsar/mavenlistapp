﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MavenSocial.DependencyServices;
using MavenSocial.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidMethods))]
namespace MavenSocial.Droid.DependencyServices
{
  public class AndroidMethods : IAndroidMethods
  {
    public AndroidMethods() { }
    public void CloseApp()
    {
      Process.KillProcess(Process.MyPid());
    }
  }
}