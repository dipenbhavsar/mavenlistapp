using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using MavenSocial.Droid.CustomRenderer;

[assembly: ExportRenderer(typeof(Entry), typeof(AndroidCustomEntry))]
namespace MavenSocial.Droid.CustomRenderer
{
  public class AndroidCustomEntry : EntryRenderer
  {
    protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
    {
      base.OnElementChanged(e);

      Control.SetTextColor(Android.Graphics.Color.Black);
      Control.SetHintTextColor(Android.Graphics.Color.Gray);
      Control.Background = Resources.GetDrawable(Resource.Drawable.entry);
      Control.SetSelectAllOnFocus(true);
    }
  }
}