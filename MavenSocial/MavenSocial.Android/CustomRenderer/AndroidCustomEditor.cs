using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using MavenSocial.Droid.CustomRenderer;

[assembly: ExportRenderer(typeof(Editor), typeof(AndroidCustomEditor))]
namespace MavenSocial.Droid.CustomRenderer
{
    public class AndroidCustomEditor : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Editor> e)
        {
            base.OnElementChanged(e);
            this.Control.SetTextColor(Android.Graphics.Color.Black);
        }
    }
}