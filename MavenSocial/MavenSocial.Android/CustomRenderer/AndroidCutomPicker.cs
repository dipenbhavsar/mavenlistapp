using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using MavenSocial.Droid.CustomRenderer;

[assembly: ExportRenderer(typeof(Picker), typeof(AndroidCutomPicker))]
namespace MavenSocial.Droid.CustomRenderer
{
  public class AndroidCutomPicker : PickerRenderer
  {
    protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
    {
      base.OnElementChanged(e);

      this.Control.SetTextColor(Android.Graphics.Color.Black);
      this.Control.SetHintTextColor(Android.Graphics.Color.Gray);
      this.Control.Background = this.Resources.GetDrawable(Resource.Drawable.downarrow);
    }
  }
}