﻿using MavenSocial.iOS.CustomRenderer;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Entry), typeof(IOSEntryRenderer))]
namespace MavenSocial.iOS.CustomRenderer
{
  class IOSEntryRenderer : EntryRenderer
  {
    protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
    {
      base.OnElementChanged(e);

      //Control.BorderStyle = UITextBorderStyle.RoundedRect;
    }
  }
}
