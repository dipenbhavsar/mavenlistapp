﻿using MavenSocial.iOS.CustomRenderer;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Picker), typeof(IOSPickerRenderer))]
namespace MavenSocial.iOS.CustomRenderer
{
    class IOSPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            UIImage image = new UIImage("dropdownarrow.png");
            Control.RightViewMode = UITextFieldViewMode.Always;
            Control.RightView = new UIImageView(image);
            Control.BorderStyle = UITextBorderStyle.Line;
        }
    }
}
