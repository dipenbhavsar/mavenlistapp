﻿using MavenSocial.DependencyServices;
using MavenSocial.iOS.DependencyServices;
using MavenSocial.Models;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(FacebookService_iOS))]
namespace MavenSocial.iOS.DependencyServices
{
  public class FacebookService_iOS : IFacebookService
  {
		Action<FacebookUser, Exception> _onLoginComplete;

		LoginManager LoginManager = null;

    #region Constructor

    public FacebookService_iOS()
		{
		}

    #endregion

    #region Request Handler

    void OnRequestHandler(GraphRequestConnection connection, NSObject result, NSError error)
		{
			if (error != null || result == null)
			{
				_onLoginComplete?.Invoke(null, new Exception(error.LocalizedDescription));
			}
			else
			{
				string id = string.Empty;
				string first_name = string.Empty;
				string email = string.Empty;
				string last_name = string.Empty;
				string url = string.Empty;

				try
				{
					id = result.ValueForKey(new NSString("id"))?.ToString();
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.Message);
				}

				try
				{
					first_name = result.ValueForKey(new NSString("first_name"))?.ToString();
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.Message);
				}

				try
				{
					email = result.ValueForKey(new NSString("email"))?.ToString();
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.Message);
				}

				try
				{
					last_name = result.ValueForKey(new NSString("last_name"))?.ToString();
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.Message);
				}

				try
				{
					url = ((result.ValueForKey(new NSString("picture")) as NSDictionary).ValueForKey(new NSString("data")) as NSDictionary).ValueForKey(new NSString("url")).ToString();
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.Message);
				}
				
				_onLoginComplete?.Invoke(new FacebookUser(id, AccessToken.CurrentAccessToken.TokenString, first_name, last_name, email, url, NSDateToDateTime(AccessToken.CurrentAccessToken.ExpirationDate)), null);
			}
		}
		void OnLoginHandler(LoginManagerLoginResult result, NSError error)
		{
			if (error != null || result == null || result.IsCancelled)
			{
				if (result != null && result.IsCancelled)
					_onLoginComplete?.Invoke(null, new Exception("Login Canceled."));

				if (error != null)
					_onLoginComplete?.Invoke(null, new Exception(error.LocalizedDescription));
			}
			else
			{
				var request = new GraphRequest("me", new NSDictionary("fields", "id, first_name, email, last_name, picture.width(500).height(500)"));
				request.Start(OnRequestHandler);
			}
		}

    #endregion

    #region IFacebookManager

    public void Login(Action<FacebookUser, Exception> OnLoginComplete)
		{
			if (_onLoginComplete == null)
				_onLoginComplete = OnLoginComplete;

			var window = UIApplication.SharedApplication.KeyWindow;
			var vc = window.RootViewController;
			while (vc.PresentedViewController != null)
			{
				vc = vc.PresentedViewController;
			}
			if (LoginManager == null)
				LoginManager = new LoginManager();
			LoginManager.Init();
			LoginManager.LogOut();
			// LoginManager.beh = LoginBehavior.SystemAccount;
			LoginManager.LogIn(new string[] { "public_profile", "email" }, vc, OnLoginHandler);
		}

		public void Logout()
		{
			LoginManager.LogOut();
		}

		#endregion

		#region private methods

		/// <summary>
		/// method to get DateTime from NsDate
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		private DateTime NSDateToDateTime(NSDate date)
		{
			DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
				new DateTime(2001, 1, 1, 0, 0, 0));
			return reference.AddSeconds(date.SecondsSinceReferenceDate);
		}

		#endregion
	}
}