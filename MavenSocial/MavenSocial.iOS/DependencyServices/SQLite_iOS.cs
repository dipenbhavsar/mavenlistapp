﻿using MavenSocial.DependencyServices;
using MavenSocial.iOS.DependencyServices;
using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_iOS))]
namespace MavenSocial.iOS.DependencyServices
{
  public class SQLite_iOS : ISQLite
  {
    public SQLite_iOS()
    {
    }

    #region ISQLite implementation
    public SQLiteConnection GetConnection()
    {
      var sqliteFilename = "WpMavelSQLite.db3";
      string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // Documents folder
      string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
      var path = Path.Combine(libraryPath, sqliteFilename);

      // This is where we copy in the prepopulated database
      Console.WriteLine(path);
      if (!File.Exists(path))
      {
        File.Copy(sqliteFilename, path);
      }

      var conn = new SQLiteConnection(path);

      // Return the database connection 
      return conn;
    }
    #endregion
  }
}
