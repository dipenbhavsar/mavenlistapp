﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MavenSocial.iOS;

[assembly: Dependency(typeof(NotificationSound_iOS))]
namespace MavenSocial.iOS
{
  public class NotificationSound_iOS : INotificationSound
  {
    public NotificationSound_iOS()
    {
    }

    public void Sound()
    {
      try
      {
        var url = Foundation.NSUrl.FromFilename("Sounds/beep2.aiff");
        AudioToolbox.SystemSound newSound = new AudioToolbox.SystemSound(url);
        newSound.PlaySystemSound();
      }
      catch (Exception ex)
      {
        string errorMessage = ex.Message;
        throw;
      }
    }


  }
}
