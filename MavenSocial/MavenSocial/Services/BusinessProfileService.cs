﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.Services
{
    public class BusinessProfileService
    {
        #region Private Properties

        private MainViewModel _mainViewModel;
        public BusinessProfile businessProfileObj;
        public ResponseStatus responseStatusObj;
        public List<BusinessProfile> businessProfileList;
        public BusinessProfileResponse businessProfileResponse;
      //  public List<UserProfile> userProfileObjList = new List<UserProfile>();
        
        #endregion

        #region Private Methods

        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnSuccess(BusinessProfile responseData)
        {
            businessProfileObj = responseData;
        }

        private void svcManager_OnSuccess(List<BusinessProfile> responseData)
        {
            businessProfileList = responseData;
        }
        private void svcManager_OnSuccess(BusinessProfileResponse responseData)
        {
            businessProfileResponse = responseData;
        }
        
        /// <summary>
        /// Handles Error Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnError(ResponseStatus responseData)
        {
            responseStatusObj = responseData;
        }

        #endregion

        #region Constructor

        public BusinessProfileService(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method to get BusinessProfile Details By Id
        /// </summary>
        /// <param name="businessProfile"></param>
        /// <returns></returns>
        public async Task GetBusinessProfileDetailsByID(BusinessProfile businessProfile)
        {
            try
            {
                var svcManager = new ServiceManager<BusinessProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetBusinessProfileDetailsById;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(businessProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        /// <summary>
        /// Method to get BusinessProfile Details By PlaceId
        /// </summary>
        /// <param name="businessProfile"></param>
        /// <returns></returns>
        public async Task GetBusinessProfileByPlaceId(BusinessProfile businessProfile)
        {
            try
            {
                var svcManager = new ServiceManager<BusinessProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetBusinessProfileByPlaceId;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(businessProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }        

        public async Task GetAllBusinessProfileDetailsByFilter(Google data)
        {
            try
            {
                var svcManager = new ServiceManager<List<BusinessProfile>>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetAllBusinessProfileDetailsByFilter;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(data);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        public async Task GetAllBusinessProfileDetailsByPageFilter(Google data)
        {
            try
            {
                var svcManager = new ServiceManager<BusinessProfileResponse>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetAllBusinessProfileDetailsByPageFilter;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(data);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        public async Task InsertUpdateBusinessProfileToDB(BusinessProfile businessProfile)
        {
            try
            {
                var svcManager = new ServiceManager<BusinessProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.InsertUpdateBusinessProfileToDB;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(businessProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        public async Task GetBusinessProfilePageListItems(Int64 userId, Int64 businessProfileId)
        {
            try
            {
                var svcManager = new ServiceManager<BusinessProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetBusinessProfilePageListItems + "/" + userId + "/" + businessProfileId;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

    
    ///// <summary>
    ///// Method To get User's Mylist By Userid
    ///// </summary>
    ///// <param name="userProfile"></param>
    ///// <returns></returns>
    //public async Task GetMyListItemByUserId(BusinessProfile businessProfile)
    //{
    //  try
    //  {
    //    var svcManager = new ServiceManager<List<BusinessProfile>>(_mainViewModel);
    //    var strWebRequestUrl = ServiceURL.UserListItem;
    //    svcManager.OnError += svcManager_OnError;
    //    svcManager.OnSuccess += svcManager_OnSuccess;
    //    string strData = JsonConvert.SerializeObject(businessProfile);
    //    await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
    //  }
    //  catch (Exception ex)
    //  {
    //    ExceptionMessage.ShowErrorMessage(ex);
    //  }
    //}
    #endregion
  }
}
