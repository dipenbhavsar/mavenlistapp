﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.Services
{
    class RelationsService
    {
        #region Private Properties
        private MainViewModel _mainViewModel;
        public Relations relationsObj;
        public ResponseStatus responseStatusObj;
        #endregion


        #region Private Methods

        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnSuccess(Relations responseData)
        {
            relationsObj = responseData;
        }

        /// <summary>
        /// Handles Error Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnError(ResponseStatus responseData)
        {
            responseStatusObj = responseData;
        }

        #endregion

        #region Constructor

        public RelationsService(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        #endregion
        #region Public user Relation Methods

        public async Task SaveUserRelations(Relations relations)
        {
            try
            {
                var svcManager = new ServiceManager<Relations>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.SaveUserRelations;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(relations);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }
        #endregion
    }
}
