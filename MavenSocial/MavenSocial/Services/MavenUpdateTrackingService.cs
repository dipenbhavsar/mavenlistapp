﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.Services
{
    class MavenUpdateTrackingService
    {
        #region Private Properties
        private MainViewModel _mainViewModel;
        public MavenUpdateTracking trackingObj;
        public ObservableCollection<MavenUpdateTracking> trackingList;
        public ResponseStatus responseStatusObj;
        #endregion

        #region Private Methods

        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="tracking"></param>
        private void svcManager_OnSuccess(MavenUpdateTracking tracking)
        {
            trackingObj = tracking;
        }

        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="tracking"></param>
        private void svcManager_OnSuccess(ObservableCollection<MavenUpdateTracking> tracking)
        {
          trackingList = tracking;
        }


    /// <summary>
    /// Handles Error Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnError(ResponseStatus responseData)
        {
            responseStatusObj = responseData;
        }

        #endregion

        #region Constructor

        public MavenUpdateTrackingService(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        #endregion

        #region Public MavenUpdateTracking Methods

        public async Task SaveMavenUpdateTracking(MavenUpdateTracking tracking)
        {
            try
            {
                var svcManager = new ServiceManager<MavenUpdateTracking>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.SaveMavenUpdateTracking;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(tracking);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

    public async Task GetMavenUpdateTrackingByUserId(long userId)
    {
      try
      {
        var svcManager = new ServiceManager<ObservableCollection<MavenUpdateTracking>>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.GetMavenUpdateTrackingByUserId + "/" +userId;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
        await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }
    #endregion
  }
}
