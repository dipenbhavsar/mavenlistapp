﻿using MavenSocial.ViewModels;
using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.Text;
using MavenSocial.Common;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.ObjectModel;

namespace MavenSocial.Services
{
    class HomeService
    {
        #region Private Properties

        private MainViewModel _mainViewModel;
        public HomeNotification homeNotificationObj;
        public ObservableCollection<HomeNotification> homeNotificationObjList;
        public ResponseStatus responseStatusObj;

        #endregion

        #region Private Methods
        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnSuccess(ObservableCollection<HomeNotification> responseData)
        {
            homeNotificationObjList = responseData;
        }

        /// <summary>
        /// Handles Error Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnError(ResponseStatus responseData)
        {

            responseStatusObj = responseData;
        }

        #endregion

        #region Constructor

        public HomeService(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        #endregion

        #region Public Methods
        public async Task GetAllHomeNotificationByUserId(HomeNotification data)
        {
            try
            {
                var svcManager = new ServiceManager<ObservableCollection<HomeNotification>>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetAllHomeNotificationByUserId;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(data);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }
        #endregion
    }
}
