﻿using MavenSocial.ViewModels;
using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.Text;
using MavenSocial.Common;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;

namespace MavenSocial.Services
{
 public class BusinessProfileBookmarksService
  {
    #region Private Properties

    private MainViewModel _mainViewModel;
    public BusinessProfileBookmarks businessProfileBookmarksObj;
    public ResponseStatus responseStatusObj;

    #endregion

    #region Private Methods

    /// <summary>
    /// Handles Success Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnSuccess(BusinessProfileBookmarks responseData)
    {
      businessProfileBookmarksObj = responseData;
    }



    /// <summary>
    /// Handles Error Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnError(ResponseStatus responseData)
    {
      responseStatusObj = responseData;
    }

    #endregion

    #region Constructor

    public BusinessProfileBookmarksService(MainViewModel mainViewModel)
    {
      _mainViewModel = mainViewModel;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Method to Save Business Profile Bookmarks
    /// </summary>
    /// <param name="userProfile"></param>
    /// <returns></returns>
    public async Task SaveBusinessProfileBookmarks(BusinessProfileBookmarks businessProfileBookmarks)
    {
      try
      {
        var svcManager = new ServiceManager<BusinessProfileBookmarks>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.SaveBusinessProfileBookmarks;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
        string strData = JsonConvert.SerializeObject(businessProfileBookmarks);
        await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    

    #endregion
  }
}
