﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.Services
{
    public class RecommendatationService
    {
        #region Private Properties

        private MainViewModel _mainViewModel;
        public Recommendatation recommendatationObj;
        public ResponseStatus responseStatusObj;

        #endregion

        #region Private Methods

        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnSuccess(Recommendatation responseData)
        {
            recommendatationObj = responseData;
        }


        /// <summary>
        /// Handles Error Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnError(ResponseStatus responseData)
        {
            responseStatusObj = responseData;
        }

        #endregion

        #region Constructor

        public RecommendatationService(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method to get Recommendatation Details By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task GetRecommendatationDetailById(Int64 id,Int64 userId)
        {
            try
            {
                var svcManager = new ServiceManager<Recommendatation>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetRecommendatationById + "/" + id + "/" + userId;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                // string strData = JsonConvert.SerializeObject(businessProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        /// <summary>
        /// Method to Save User recommendatation Inforamtion 
        /// </summary>
        /// <param name="recommendatation"></param>
        /// <returns></returns>
        public async Task SaveRecommendatationPost(Recommendatation recommendatation)
        {
            try
            {
                var svcManager = new ServiceManager<Recommendatation>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.SaveRecommendatation;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(recommendatation);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }
        #endregion
    }
}
