﻿using MavenSocial.ViewModels;
using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.Text;
using MavenSocial.Common;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.ObjectModel;

namespace MavenSocial.Services
{
  public class SavedUserProfileBookmarksService
  {
    #region Private Properties

    private MainViewModel _mainViewModel;
    public SavedUserProfileBookmarks savedUserProfileBookmarksObj;
    public List<SavedUserProfileBookmarks> savedUserProfileBookmarksList;
    public ObservableCollection<RecommendadPhotos> savedRecommendationPhotosList;
    public ResponseStatus responseStatusObj;

    #endregion

    #region Private Methods

    /// <summary>
    /// Handles Success Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnSuccess(SavedUserProfileBookmarks responseData)
    {
      savedUserProfileBookmarksObj = responseData;
    }

    /// <summary>
    /// Handles Success Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnSuccess(List<SavedUserProfileBookmarks> responseData)
    {
      savedUserProfileBookmarksList = responseData;
    }

    /// <summary>
    /// Handles Success Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnSuccess(ObservableCollection<RecommendadPhotos> responseData)
    {
      savedRecommendationPhotosList = responseData;
    }

    /// <summary>
    /// Handles Error Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnError(ResponseStatus responseData)
    {
      responseStatusObj = responseData;
    }

    #endregion

    #region Constructor

    public SavedUserProfileBookmarksService(MainViewModel mainViewModel)
    {
      _mainViewModel = mainViewModel;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Method to do Get User Location Bookmark By UserId
    /// </summary>
    /// <param name="userLocationBookmark"></param>
    /// <returns></returns>
    public async Task GetSavedUserProfileBookmarksByUserId(Int64 userId)
    {
      try
      {
        var svcManager = new ServiceManager<List<SavedUserProfileBookmarks>>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.GetSavedUserProfileBookmarksByUserId + "/" + userId;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
        await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    /// <summary>
    /// Method to do Save User Profile Bookmark 
    /// </summary>
    /// <param name="userLocationBookmark"></param>
    /// <returns></returns>

    public async Task SaveSavedUserProfileBookmarks(SavedUserProfileBookmarks savedUserProfileBookmarks)
    {
      try
      {
        var svcManager = new ServiceManager<SavedUserProfileBookmarks>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.SaveSavedUserProfileBookmarks;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
        string strData = JsonConvert.SerializeObject(savedUserProfileBookmarks);
        await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    /// <summary>
    /// Method to do Save User Profile Bookmark 
    /// </summary>
    /// <param name="userLocationBookmark"></param>
    /// <returns></returns>

    public async Task GetSavedRecommendationPhotosByUserId(long userId)
    {
      try
      {
        var svcManager = new ServiceManager<ObservableCollection<RecommendadPhotos>>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.GetSavedRecommendationPhotosByUserId + "/" + userId;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
       // string strData = JsonConvert.SerializeObject(savedUserProfileBookmarks);
        await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }



    #endregion
  }
}
