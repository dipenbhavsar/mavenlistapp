﻿using MavenSocial.ViewModels;
using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.Text;
using MavenSocial.Common;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.ObjectModel;

namespace MavenSocial.Services
{
    public class UserProfileService
    {
        #region Private Properties

        private MainViewModel _mainViewModel;
        public UserProfile userProfileObj;
        public ResponseStatus responseStatusObj;
        public ObservableCollection<UserProfile> userProfileList;

        #endregion

        #region Private Methods

        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnSuccess(UserProfile responseData)
        {
            userProfileObj = responseData;
        }


        /// <summary>
        /// Handles Success Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnSuccess(ObservableCollection<UserProfile> responseData)
        {
            userProfileList = responseData;
        }

        /// <summary>
        /// Handles Error Event
        /// </summary>
        /// <param name="responseData"></param>
        private void svcManager_OnError(ResponseStatus responseData)
        {
            responseStatusObj = responseData;
        }

        #endregion

        #region Constructor

        public UserProfileService(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method to do User Login By Username and Password
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task DoUserLoginWithUsernameAndPassowrd(UserProfile userProfile)
        {
            try
            {
                var svcManager = new ServiceManager<UserProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.DoUserLoginByUserNameAndPassword;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        /// <summary>
        /// Method to do User Login By Username and Password
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task CheckUsernameAvailablity(UserProfile userProfile)
        {
            try
            {
                var svcManager = new ServiceManager<UserProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.CheckUsernameAvaliablity;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        /// <summary>
        /// Method to Save User Registration Inforamtion 
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task SaveUserRegistrationInformation(UserProfile userProfile)
        {
            try
            {
                var svcManager = new ServiceManager<UserProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.SaveUserRegistrationInformation;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }


        /// <summary>
        /// Method to Save User Profile Inforamtion 
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task SaveUserProfileInformation(UserProfile userProfile)
        {
            try
            {
                var svcManager = new ServiceManager<UserProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.SaveUserProfile;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }


        /// <summary>
        /// Method to Get User Profile Inforamtion By Facebook Id
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task GetUserProfileInformationByFacebookId(UserProfile userProfile)
        {
            try
            {
                var svcManager = new ServiceManager<UserProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.SelectUserProfileByFacebookId;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }


        /// <summary>
        /// Method to Get User Profile Inforamtion By Facebook Id
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task UpdatePasswordById(UserProfile userProfile)
        {
            try
            {
                var svcManager = new ServiceManager<UserProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.UpdateUserPassword;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task GetUserProfileInformationById(UserProfile userProfile)
        {
            try
            {
                var svcManager = new ServiceManager<UserProfile>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetUserProfileById;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        /// <summary>
        /// Method to call WebApi to Get UserProfile Information By Name
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task GetUserProfileInformationByName(string name, Int64 userId)
        {
            try
            {
                var svcManager = new ServiceManager<ObservableCollection<UserProfile>>(_mainViewModel);
                var strWebRequestUrl = ServiceURL.GetUserProfileByName + "/" + name + "/" + userId;
                svcManager.OnError += svcManager_OnError;
                svcManager.OnSuccess += svcManager_OnSuccess;
                // string strData = JsonConvert.SerializeObject(userProfile);
                await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
            }
            catch (Exception ex)
            {
                ExceptionMessage.ShowErrorMessage(ex);
            }
        }

        #endregion

      
    }
}
