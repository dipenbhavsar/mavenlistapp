﻿using MavenSocial.ViewModels;
using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.Text;
using MavenSocial.Common;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;

namespace MavenSocial.Services
{
  public class UserLocationBookmarkService
  {
    #region Private Properties

    private MainViewModel _mainViewModel;
    public UserLocationBookmark userLocationBookmarkObj;
    public List<UserLocationBookmark> userLocationBookmarkList;
    public ResponseStatus responseStatusObj;

    #endregion

    #region Private Methods

    /// <summary>
    /// Handles Success Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnSuccess(UserLocationBookmark responseData)
    {
      userLocationBookmarkObj = responseData;
    }

    /// <summary>
    /// Handles Success Event of List
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnSuccess(List<UserLocationBookmark> responseData)
    {
      userLocationBookmarkList = responseData;
    }

    /// <summary>
    /// Handles Error Event
    /// </summary>
    /// <param name="responseData"></param>
    private void svcManager_OnError(ResponseStatus responseData)
    {
      responseStatusObj = responseData;
    }

    #endregion

    #region Constructor

    public UserLocationBookmarkService(MainViewModel mainViewModel)
    {
      _mainViewModel = mainViewModel;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Method to do Get User Location Bookmark By UserId
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    public async Task GetUserLocationBookmarkByUserId(Int64 userId)
    {
      try
      {
        var svcManager = new ServiceManager<List<UserLocationBookmark>>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.GetUserLocationBookmarkByUserId +"/" + userId ;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
        await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    /// <summary>
    /// Method to do Get User Location Bookmark By Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task GetUserLocationBookmarkById(Int64 id)
    {
      try
      {
        var svcManager = new ServiceManager<UserLocationBookmark>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.GetUserLocationBookmarkById + "/" + id;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
        await svcManager.JsonWebRequest(strWebRequestUrl, null, HttpMethod.Get);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    public async Task SaveUserLocationBookmark(UserLocationBookmark userLocationBookmark)
    {
      try
      {
        var svcManager = new ServiceManager<UserLocationBookmark>(_mainViewModel);
        var strWebRequestUrl = ServiceURL.SaveUserLocationBookmark;
        svcManager.OnError += svcManager_OnError;
        svcManager.OnSuccess += svcManager_OnSuccess;
        string strData = JsonConvert.SerializeObject(userLocationBookmark);
        await svcManager.JsonWebRequest(strWebRequestUrl, strData, HttpMethod.Post);
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    #endregion
  }
}
