﻿using DurianCode.PlacesSearchBar;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MavenSocial.Common
{
  public class CommonFunctions
  {
    /// <summary>
    /// Method to Check Business Exist or Not 
    /// </summary>
    private async static Task<BusinessProfile> checkProfileExistOrNot(BusinessProfile businessProfile)
    {
      try
      {
        BusinessProfileViewModel business = new BusinessProfileViewModel();
        businessProfile = await business.GetBusinessProfileByPlaceId(businessProfile);// (ApplicationConfiguration.UserId);
        return businessProfile;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        Utility.ShowToastInfo(ex.Message);
      }
      return null;
    }

    /// <summary>
    /// Method to check Whether Business is available or not in DB if not then save to DB and Get BusinessProfileId
    /// </summary>
    public static async Task<Int64> GetBusinessProfileId(BusinessProfile businessProfile)
    {
      BusinessProfileViewModel businessProfileViewModel = new BusinessProfileViewModel();
      BusinessProfile businessProfileObj = await checkProfileExistOrNot(businessProfile);
      if (businessProfileObj != null)
      {
         return businessProfileObj.Id;
      }
      else
      {
        var place = await Places.GetPlace(businessProfile.PlaceId, ApplicationConfiguration.ApiKey);
        businessProfile = new BusinessProfile()
        {
          Name = place.Name,
          Address = place.FormattedAddress,
          WebSiteUrl = place.Website,
          Country = place.Country.ToString(),
          Lat = place.Latitude,
          Longi = place.Longitude,
          PostCode = place.PostalCode.ToString(),
          PlaceId = place.Place_ID,
          Isdeleted = 0
        };
        await businessProfileViewModel.InsertUpdateBusinessProfileToDB(businessProfile);
        return businessProfileViewModel.businessProfileObj.Id;
      }
    }

        public async static Task<MavenUpdateTracking> SaveMavenUpdateTracking(MavenUpdateTracking tracking)
        {
            try
            {
                MavenUpdateTrackingViewModel mavenUpdateTracking = new MavenUpdateTrackingViewModel();
                tracking = await mavenUpdateTracking.SaveMavenUpdateTracking(tracking);
                return tracking;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }
            return null;
        }
    }
}
