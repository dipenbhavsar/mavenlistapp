﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using MavenSocial.ViewModels;
using Xamarin.Forms;
using MavenSocial.Common;

namespace MavenSocial.Common
{
    public class ServiceManager<T>
    {
        #region Varibles

        private readonly MainViewModel _mainViewModel;

        public delegate void SucessEventHandler(T responseData);
        public delegate void ErrorEventHandler(ResponseStatus responseData);

        public event SucessEventHandler OnSuccess;
        public event ErrorEventHandler OnError;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mainViewModel"></param>
        public ServiceManager(MainViewModel mainViewModel = null)
        {
            _mainViewModel = mainViewModel;
        }

        /// <summary>
        /// Set Busy
        /// </summary>
        /// <param name="status"></param>
        public void SetBusy(bool status)
        {
            if (_mainViewModel != null)
                _mainViewModel.IsBusy = status;
        }

        /// <summary>
        /// Handles Json Web Request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="contents"></param>
        /// <param name="methodType"></param>
        /// <param name="isTokenNeeded"></param>
        /// <param name="isForRelogin"></param>
        /// <param name="mediaStream"></param>
        /// <returns></returns>
        public async Task JsonWebRequest(string url, string contents, HttpMethod methodType, bool isTokenNeeded = true, string mediaStream = "application/json")
        {
            try
            {
                SetBusy(true);
                await Task.Delay(100);
                Task t = Request(url, contents, methodType, isTokenNeeded, mediaStream);

                await t;

                SetBusy(false);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Manage Web Request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="contents"></param>
        /// <param name="methodType"></param>
        /// <param name="isTokenNeeded"></param>
        /// <param name="isForRelogin"></param>
        /// <param name="mediaStream"></param>
        /// <returns></returns>
        public async Task Request(string url, string contents, HttpMethod methodType, bool isTokenNeeded = true, string mediaStream = "application/json")
        {
            bool isSuccessRequest = true;
            string responseBodyAsText;

            try
            {
                if (_mainViewModel != null)
                    _mainViewModel.IsInternetAvailable = true;

                //if (ApplicationConfiguration.LastLoginTime > DateTime.MinValue &&
                //    (DateTime.Now - ApplicationConfiguration.LastLoginTime).TotalSeconds > 0)
                //  Utility.CheckRelogin(isForRelogin);

                HttpClientHandler handler = new HttpClientHandler();
                using (HttpClient httpClient = new HttpClient(handler))
                {
                    HttpRequestMessage message = new HttpRequestMessage(methodType, url);

                    if (methodType == HttpMethod.Post)
                    {
                        message.Headers.ExpectContinue = false;
                        message.Content = new StringContent(contents);
                        // message.Content.Headers.ContentLength = contents.Length;
                        message.Content.Headers.ContentType = new MediaTypeHeaderValue(mediaStream);
                    }

                    if (isTokenNeeded)
                    {
                        httpClient.DefaultRequestHeaders.Add("WpSmcsSecurityToken",
                            ApplicationConfiguration.WpSmcsServiceToken);
                    }

                    httpClient.Timeout = new TimeSpan(0, 0, 10, 0, 0);

                    HttpResponseMessage response = await httpClient.SendAsync(message);
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        var error = new ResponseStatus() { ErrorMessage = SystemMessages.OfflineServer };
                        OnError?.Invoke(error);
                        return;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        responseBodyAsText = response.Content.ReadAsStringAsync().Result;
                        var error = JsonConvert.DeserializeObject<ResponseStatus>(responseBodyAsText);
                        OnError?.Invoke(error);
                        return;
                    }
                    else
                    {
                        response.EnsureSuccessStatusCode();
                        responseBodyAsText = response.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            catch (HttpRequestException hre)
            {
                responseBodyAsText = "Exception : " + hre.Message;
                isSuccessRequest = false;
            }
            catch (Exception ex)
            {
                responseBodyAsText = "Exception : " + ex.Message;
                isSuccessRequest = false;
                OnError?.Invoke(new ResponseStatus
                {
                    ErrorMessage = ex.Message
                });
            }

            if (isSuccessRequest)
            {
                if (typeof(T) == typeof(string))
                {
                    OnSuccess?.Invoke((T)(object)responseBodyAsText);
                }
                else
                {
                    try
                    {
                        var data = JsonConvert.DeserializeObject<T>(Convert.ToString(responseBodyAsText));
                        OnSuccess?.Invoke(data);
                    }
                    catch (Exception ex)
                    {
                        OnError?.Invoke(new ResponseStatus
                        {
                            ErrorMessage = ex.Message
                        });
                    }
                }
            }
            else
            {
                OnError?.Invoke(new ResponseStatus
                {
                    ErrorMessage = responseBodyAsText
                });
            }
        }
    }

    public class ServiceManager
    {
        /// <summary>
        /// Async method to ping service
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        public static async Task<bool> PingAsync(string requestUrl = null)
        {
            try
            {

                using (var httpClient = GetHttpClient(requestUrl))
                {
                    httpClient.Timeout = new TimeSpan(0, 0, 0, 30); // Try for 30 seconds if api server is accessible
                    var response = await httpClient.GetAsync(requestUrl);
                    if (response != null)
                    {
                        if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            // return true if service is found.
                            return true;
                        }
                    }
                }
            }
            catch
            {
                // ignored
            }
            return false;
        }

        /// <summary>
        /// Get HttpClient based on string request Url and mediaType as json(default)
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        private static HttpClient GetHttpClient(string requestUrl, string mediaType = null)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.Timeout = new TimeSpan(0, 0, 30);
            if (string.IsNullOrWhiteSpace(mediaType))
                mediaType = "application/json";
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));
            httpClient.BaseAddress = new Uri(requestUrl);

            return httpClient;
        }

        /// <summary>
        /// Ping: It is used to ping service
        /// </summary>
        /// <param name="requestUrl">string</param>
        /// <returns>boolean</returns>
        public static bool Ping(string requestUrl = null)
        {
            return Task.Run(() => PingAsync(requestUrl)).Result;
        }
    }

    public class ResponseStatus : EventArgs
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Result
    {
        [JsonProperty(PropertyName = "ErrorMessage")]
        public string ErrorMessage { get; set; }


        [JsonProperty(PropertyName = "Data")]
        public object Data { get; set; }


        [JsonProperty(PropertyName = "HasMoreRecord")]
        public bool? HasMoreRecord { get; set; }
    }
}
