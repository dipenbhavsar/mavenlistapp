﻿using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MavenSocial.Common
{
    public static class ApplicationConfiguration
    {
        #region Static Properties

        public static string WpSmcsServiceToken { get; set; }

        public static string WebServiceHost = (Debugger.IsAttached) ? "http://192.168.0.137:555/api" : "https://mavenlist.azurewebsites.net";
        // "http://192.168.43.50:8881";

        public static Int64 UserId { get; set; }
        public static string Username { get; set; }
        public static string Email { get; set; }
        public static string Name { get; set; }
        public static string ProfilePicture { get; set; }
        public static double Longitude { get; set; }
        public static double Latitude { get; set; }

        public static string ApiKey = Device.RuntimePlatform == Device.iOS ? "AIzaSyBimF4GlBwLh0jDl2iBoxVJav1OI8k2mPE" : "AIzaSyC66dgIyOnhFk7JHaXGJ0rh6SxdXTyZIv0";

        #endregion

        #region Public Method

        public static void ClearLoggedInUserValues()
        {
            UserId = 0;
            Username = "";
            Email = "";
            Name = "";
            ProfilePicture = "";
        }

        #endregion
    }
}
