﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MavenSocial.Common
{
  public class Cryptography
  {
    #region "Token Key"
    /// <summary>
    /// Encryption Key
    /// </summary>    
    static string CryptoKey = "RtLJy9PAsj78";

    #endregion

    #region "Methods"    

    /// <summary>
    /// Method to Encypt string eith different Salt Key
    /// </summary>
    /// <param name="stringToEncrypt"></param>
    /// <returns></returns>
    public static string EncryptString(string stringToEncrypt, string UserSaltKey)
    {
      byte[] bCryptoKey = Encoding.UTF8.GetBytes(CryptoKey);

      // Hash the password with SHA256
      byte[] bCryptoKeyHash = SHA256Managed.Create().ComputeHash(bCryptoKey);


      byte[] bStringToEncrypt = Encoding.UTF8.GetBytes(stringToEncrypt);

      byte[] bUserSaltKey = Encoding.UTF8.GetBytes(UserSaltKey);
      byte[] bEncrypted = new byte[bUserSaltKey.Length + bStringToEncrypt.Length];

      // Combine Salt + Text
      for (int i = 0; i < bUserSaltKey.Length; i++)
        bEncrypted[i] = bUserSaltKey[i];
      for (int i = 0; i < bStringToEncrypt.Length; i++)
        bEncrypted[i + bUserSaltKey.Length] = bStringToEncrypt[i];

      bEncrypted = Final_Encrypt(bEncrypted, bCryptoKeyHash);

      return Convert.ToBase64String(bEncrypted); ;
    }

    /// <summary>
    /// Decrypt the String using salt key
    /// </summary>
    /// <param name="stringToDecrypt"></param>
    /// <param name="UserSaltKey"></param>
    /// <returns></returns>
    public static string DecryptString(string stringToDecrypt, string UserSaltKey)
    {
      byte[] bCryptoKey = Encoding.UTF8.GetBytes(CryptoKey);

      // Hash the password with SHA256
      byte[] bCryptoKeyHash = SHA256Managed.Create().ComputeHash(bCryptoKey);

      byte[] bStringToDecrypt = Convert.FromBase64String(stringToDecrypt);

      byte[] baDecrypted = Final_Decrypt(bStringToDecrypt, bCryptoKeyHash);

      byte[] bUserSaltKey = Encoding.UTF8.GetBytes(UserSaltKey);

      // Remove salt
      int saltLength = bUserSaltKey.Length;
      byte[] bDecryptResult = new byte[baDecrypted.Length - saltLength];
      for (int i = 0; i < bDecryptResult.Length; i++)
        bDecryptResult[i] = baDecrypted[i + saltLength];

      return Encoding.UTF8.GetString(bDecryptResult); ;
    }

    /// <summary>
    /// Encrypt
    /// </summary>
    /// <param name="bytesToBeEncrypted"></param>
    /// <param name="passwordBytes"></param>
    /// <returns></returns>
    public static byte[] Final_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
    {
      byte[] encryptedBytes = null;
      byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

      using (MemoryStream ms = new MemoryStream())
      {
        using (RijndaelManaged RiObj = new RijndaelManaged())
        {
          RiObj.KeySize = 256;
          RiObj.BlockSize = 128;

          var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
          RiObj.Key = key.GetBytes(RiObj.KeySize / 8);
          RiObj.IV = key.GetBytes(RiObj.BlockSize / 8);

          RiObj.Mode = CipherMode.CBC;

          using (var cs = new CryptoStream(ms, RiObj.CreateEncryptor(), CryptoStreamMode.Write))
          {
            cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
            cs.Close();
          }
          encryptedBytes = ms.ToArray();
        }
      }

      return encryptedBytes;
    }

    /// <summary>
    /// Decrypt
    /// </summary>
    /// <param name="bytesToBeDecrypted"></param>
    /// <param name="passwordBytes"></param>
    /// <returns></returns>
    public static byte[] Final_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
    {
      byte[] decryptedBytes = null;

      // Set your salt here, change it to meet your flavor:
      // The salt bytes must be at least 8 bytes.
      byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

      using (MemoryStream ms = new MemoryStream())
      {
        using (RijndaelManaged RiObj = new RijndaelManaged())
        {
          RiObj.KeySize = 256;
          RiObj.BlockSize = 128;

          var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
          RiObj.Key = key.GetBytes(RiObj.KeySize / 8);
          RiObj.IV = key.GetBytes(RiObj.BlockSize / 8);

          RiObj.Mode = CipherMode.CBC;

          using (var cs = new CryptoStream(ms, RiObj.CreateDecryptor(), CryptoStreamMode.Write))
          {
            cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
            cs.Close();
          }
          decryptedBytes = ms.ToArray();
        }
      }

      return decryptedBytes;
    }

    /// <summary>
    /// Create Random Salt Key
    /// </summary>
    /// <returns></returns>
    public static string CreateSalt()
    {
      //Generate a cryptographic random number.
      RNGCryptoServiceProvider rngSalt = new RNGCryptoServiceProvider();
      byte[] bSaltKey = new byte[32];
      rngSalt.GetBytes(bSaltKey);

      // Return a Base64 string representation of the random number.
      return Convert.ToBase64String(bSaltKey);
    }

    #endregion
  }
}
