﻿namespace MavenSocial.Common
{
    public static class ServiceURL
    {
        #region Login
        public static string DoUserLoginByUserNameAndPassword => $"{ApplicationConfiguration.WebServiceHost}/api/Auth/DoUserLogin";

        #endregion

        #region Registration

        public static string SaveUserRegistrationInformation => $"{ApplicationConfiguration.WebServiceHost}/api/User/SaveUserRegistrationInformation";

        #endregion

        #region User Profile

        public static string SaveUserProfile => $"{ApplicationConfiguration.WebServiceHost}/api/User/SaveUserProfile";

        public static string SelectUserProfileByFacebookId => $"{ApplicationConfiguration.WebServiceHost}/api/User/SelectUserProfileByFacebookId";

        public static string CheckUsernameAvaliablity => $"{ApplicationConfiguration.WebServiceHost}/api/User/CheckUsernameAvailablity";

        public static string UpdateUserPassword => $"{ApplicationConfiguration.WebServiceHost}/api/User/UpdateUserPassword";

        public static string GetUserProfileById => $"{ApplicationConfiguration.WebServiceHost}/api/User/GetUserProfileById";

        public static string CheckServiceAvailability => $"{ApplicationConfiguration.WebServiceHost}/api/MavelSocial/Service/CheckServiceAvailability/";

        public static string GetUserProfileByName => $"{ApplicationConfiguration.WebServiceHost}/api/User/GetUserProfileByName";

        #endregion

        #region Business Profile
        public static string GetBusinessProfileDetailsById => $"{ApplicationConfiguration.WebServiceHost}/api/BusinessProfile/GetBusinessProfileById";
        public static string GetBusinessProfileByPlaceId => $"{ApplicationConfiguration.WebServiceHost}/api/BusinessProfile/GetBusinessProfileByPlaceId";
        public static string InsertUpdateBusinessProfileToDB => $"{ApplicationConfiguration.WebServiceHost}/api/BusinessProfile/InsertUpdateBusinessProfile";
        public static string GetAllBusinessProfileDetailsByFilter => $"{ApplicationConfiguration.WebServiceHost}/api/BusinessProfile/GetBusinessProfilePins";

        public static string GetAllBusinessProfileDetailsByPageFilter => $"{ApplicationConfiguration.WebServiceHost}/api/BusinessProfile/GetBusinessProfilePinsPageWise";
        public static string GetBusinessProfilePageListItems => $"{ApplicationConfiguration.WebServiceHost}/api/BusinessProfile/GetBusinessProfilePageListItems";

        #endregion

        #region Home
        public static string GetAllHomeNotificationByUserId => $"{ApplicationConfiguration.WebServiceHost}/api/Home/GetHomeNotificationByUserId";

        #endregion

        #region userLocationBookmark
        public static string GetUserLocationBookmarkByUserId => $"{ApplicationConfiguration.WebServiceHost}/api/User/GetUserLocationBookmarkByUserId";
        public static string GetUserLocationBookmarkById => $"{ApplicationConfiguration.WebServiceHost}/api/User/GetUserLocationBookmarkById";
        public static string SaveUserLocationBookmark => $"{ApplicationConfiguration.WebServiceHost}/api/User/SaveUserLocationBookmark";
        #endregion

        #region SavedUserProfileBookmarks
        public static string GetSavedUserProfileBookmarksByUserId => $"{ApplicationConfiguration.WebServiceHost}/api/User/GetSavedUserProfileBookmarksByUserId";
        public static string SaveSavedUserProfileBookmarks => $"{ApplicationConfiguration.WebServiceHost}/api/User/SaveSavedUserProfileBookmarks";
        public static string GetSavedRecommendationPhotosByUserId => $"{ApplicationConfiguration.WebServiceHost}/api/User/GetSavedRecommendationPhotosByUserId";
        #endregion

        #region BusinessProfileBookmarks
        public static string SaveBusinessProfileBookmarks => $"{ApplicationConfiguration.WebServiceHost}/api/BusinessProfile/SaveBusinessProfileBookmarks";

        #endregion

        #region Recommendatation
        public static string GetRecommendatationById => $"{ApplicationConfiguration.WebServiceHost}/api/Recommendatation/GetRecommendatationById";
        public static string SaveRecommendatation => $"{ApplicationConfiguration.WebServiceHost}/api/Recommendatation/SaveRecommendatation";
        #endregion

        #region User Relations
        public static string SaveUserRelations => $"{ApplicationConfiguration.WebServiceHost}/api/User/SaveUserRelations";
        #endregion

        #region User Updates Tracking
        public static string SaveMavenUpdateTracking => $"{ApplicationConfiguration.WebServiceHost}/api/MavenUpdateTracking/SaveNewMavenUpdateTracking";
        public static string GetMavenUpdateTrackingByUserId => $"{ApplicationConfiguration.WebServiceHost}/api/MavenUpdateTracking/GetMavenUpdateTrackingByUserId";
        #endregion
    }
}
