﻿using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.CommunityToolkit.Extensions;
using Xamarin.CommunityToolkit.UI.Views.Options;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace MavenSocial.Common
{
    public static class Utility
    {
        public static string GetAppendedConst()
        {
            return "100%C3rt@1nTy!";
        }

        /// <summary>
        /// Encode Url
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static string EncodeUrl(this string stringValue)
        {
            return System.Net.WebUtility.UrlEncode(stringValue);
        }

        /// <summary>
        /// Get Index of Dictionary Item by Value
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetIndex(Dictionary<int, string> dictionary, int key)
        {
            for (int index = 0; index < dictionary.Count; index++)
            {
                if (dictionary.Skip(index).First().Key == key)
                    return index;
            }

            return -1;
        }

        /// <summary>
        /// Method to check if internet is available or not
        /// </summary>
        /// <returns></returns>
        public static bool CheckIfInternetAvailable()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                var profiles = Connectivity.ConnectionProfiles;

                if (profiles.Contains(ConnectionProfile.WiFi) || current == NetworkAccess.Internet)
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Method to get Images Source From Base64 string
        /// </summary>
        /// <param name="imageString"></param>
        /// <returns></returns>
        public static ImageSource GetImageSourceFromBase64String(string imageString)
        {
            if (string.IsNullOrWhiteSpace(imageString) || imageString == "AA==")
            {
                return "Logo_Purple.png";
            }
            else
            {
                byte[] byteData = Convert.FromBase64String(imageString);
                if (byteData != null)
                {
                    return ImageSource.FromStream(() => new MemoryStream(byteData));
                }

                return "Logo_Purple.png";
            }
        }

        /// <summary>
        /// Method to Show Toast MEssagge
        /// </summary>
        /// <param name="message"></param>
        public static void ShowToastInfo(string message)
        {
            Application.Current.MainPage.DisplayToastAsync(
               new ToastOptions
               {
                   BackgroundColor = Color.Black,
                   Duration = TimeSpan.FromMilliseconds(3000),
                   MessageOptions = new MessageOptions
                   {
                       Foreground = Color.White,
                       Message = message
                   }
               }
               );
        }

        /// <summary>
        /// Method To check Permission
        /// </summary>
        /// <returns></returns>
        public static async Task<PermissionStatus> CheckAndRequestLocationPermission()
        {
            var status = await Permissions.CheckStatusAsync<Permissions.LocationWhenInUse>();

            if (status == PermissionStatus.Granted)
                return status;

            if (status == PermissionStatus.Denied && DeviceInfo.Platform == DevicePlatform.iOS)
            {
                // Prompt the user to turn on in settings
                // On iOS once a permission has been denied it may not be requested again from the application
                DependencyService.Get<INotificationSound>().Sound();
                await Application.Current.MainPage.DisplayAlert(SystemMessages.MavenSocial, SystemMessages.LocationPermissionDenied, SystemMessages.OK);
                return status;
            }

            if (Permissions.ShouldShowRationale<Permissions.LocationWhenInUse>())
            {
                // Prompt the user with additional information as to why the permission is needed
            }

            status = await Permissions.RequestAsync<Permissions.LocationWhenInUse>();

            return status;
        }

        /// <summary>
        /// Method To GetCurrent Location Of User
        /// </summary>
        /// <returns></returns>
        public static async Task GetCurrentLocation()
        {
            try
            {
                Geocoder geoCoder;
                CancellationTokenSource cts;
                PermissionStatus permissionStatus = await Utility.CheckAndRequestLocationPermission();
                if (permissionStatus == PermissionStatus.Granted)
                {
                    var request = new GeolocationRequest(GeolocationAccuracy.Medium, TimeSpan.FromSeconds(2));
                    cts = new CancellationTokenSource();
                    var location = await Geolocation.GetLocationAsync(request, cts.Token);

                    if (location != null)
                    {
                        ApplicationConfiguration.Latitude = location.Latitude;
                        ApplicationConfiguration.Longitude = location.Longitude;                       
                    }
                }
                
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                ShowToastInfo(ex.Message);
            }
            
        }
    }

    public static class ExceptionMessage
    {
        /// <summary>
        /// Show Error Message
        /// </summary>
        /// <param name="e"></param>
        public static void ShowErrorMessage(Exception e)
        {
#if DEBUG
            {
                ShowErrorMessage(e.Message);
                return;
            }
#else
            ShowErrorMessage(e.Message);

#endif
        }

        /// <summary>
        /// Show Error Message
        /// </summary>
        /// <param name="e"></param>
        /// <param name="message"></param>
        public static void ShowErrorMessage(Exception e, string message)
        {
            try
            {
#if DEBUG
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    //await Application.Current.MainPage.DisplayAlert(SystemMessages.MavelSocial, e.Message, SystemMessages.OK);
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await Application.Current.MainPage.DisplayAlert(SystemMessages.MavenSocial, e.Message, SystemMessages.OK);
                    });
                    return;
                }
#else
                {


                    DependencyService.Get<INotificationSound>().Sound();
                    //await Application.Current.MainPage.DisplayAlert(SystemMessages.MavelSocial, message, SystemMessages.OK);
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await Application.Current.MainPage.DisplayAlert(SystemMessages.MavenSocial, message, SystemMessages.OK);
                    });
                    return;
                }
#endif
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Show Error Message
        /// </summary>
        /// <param name="message"></param>
        public static void ShowErrorMessage(string message)
        {
            try
            {
#if DEBUG
                DependencyService.Get<INotificationSound>().Sound();
                //await Application.Current.MainPage.DisplayAlert(SystemMessages.MavelSocial, message, SystemMessages.OK);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Application.Current.MainPage.DisplayAlert(SystemMessages.MavenSocial, message, SystemMessages.OK);
                });
                return;
#else
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    //await Application.Current.MainPage.DisplayAlert(SystemMessages.MavelSocial, message, SystemMessages.OK);
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await Application.Current.MainPage.DisplayAlert(SystemMessages.MavenSocial, message, SystemMessages.OK);
                    });
                    return;
                }
#endif
            }
            catch (Exception)
            {

            }
        }

    }
}
