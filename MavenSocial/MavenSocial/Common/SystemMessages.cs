﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.Common
{
    public class SystemMessages
    {
        public const string ConfirmExit = "Confirm Exit From App?";
        public const string OK = "OK";
        public const string Cancel = "CANCEL";
        public const string Yes = "YES";
        public const string No = "NO";
        public const string MavenSocial = "Maven List";
        public const string ConfirmLogout = "Are you sure you want to Logout?";
        public const string OfflineServer = "Error: Offline Detected - Unable to Retrieve Data From Server";
        public const string InValidSiteKey = "Invalid Site Key";
        public const string EmailEmpty = "Email must be filled in.";
        public const string PasswordEmpty = "Password must be filled in.";
        public const string CurrentPasswordEmpty = "Current Password must be filled in.";
        public const string NewPasswordEmpty = "New Password must be filled in.";
        public const string ConfirmPasswordEmpty = "Confirm Password must be filled in.";
        public const string PasswordNotMatch = "New password and confirm password dose not match.";
        public const string EmailPasswordEmpty = "Email/Password is Empty or Invalid.";
        public const string Username = "Username is Empty or Invalid.";
        public const string Name = "Name is Empty or Invalid.";
        public const string Country = "Country is Empty or Invalid.";
        public const string State = "State is Empty or Invalid.";
        public const string City = "City is Empty or Invalid.";
        public const string PhoneNo = "Phoneno is Empty or Invalid.";
        public const string Website = "Website is Empty or Invalid.";
        public const string Bio = "Bio is Empty or Invalid.";
        public const string InvalidUsernamePassword = "Invalid Username or Password.";
        public const string AvailableUsername = "This Username is Already Available please Try With Other Username.";
        public const string RegistrationFailed = "Registration Faild, Please Try Again.";
        public const string PickPhoto = "No PickPhoto available.";
        public const string NoDetilsFound = "No Details Found";
        public const string NoResult = "No Result Found";
        public const string PincsCountFound = " Records Found";
        public const string ListItemAdded = "New Item Added into List Successfully";
        public const string PasswordValidationMessage = "Password must be minimum 6 characters long and must contain at least 1 character 1 number and 1 special character";
        public const string TermsAndConditions = "Please check agree with terms and conditions.";
        public const string LocationPermissionDenied = "Please Grant Location Permission from Settings For The Application.";
        public const string SavedBusinessProfileToList = "Business Profile Saved into List Successfully";
        public const string SomethingWenttoWrong = "Something went to wrong Please Try again";
        public const string SelectBookMark = "Please Select Bookmark List";
        public const string AlreadyRecommended = "This Business is already Recommended";
        public const string RecommendedSuccessfully = "This Business is Recommended Sucessfully";
        public const string EnterProperDetails = "Please Enter proper Details";
        public const string Followed = "Followed Successfully";
        public const string SavedSucessfully = "Saved Sucessfully";
        public const string AlreadyListSaved = "This List is already Saved";

  }
}
