﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.DependencyServices
{
  public interface IVirtualKeyboard
  {
    void ShowKeyboard();
    void HideKeyboard();
  }
}
