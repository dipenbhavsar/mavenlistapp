﻿using MavenSocial.ExtendedControls;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.DependencyServices
{
  public interface ISoftwareKeyboardService
  {
    event EventHandler<SoftwareKeyboardEventArgs> KeyboardHeightChanged;
    bool IsKeyboardVisible { get; }
  }
}
