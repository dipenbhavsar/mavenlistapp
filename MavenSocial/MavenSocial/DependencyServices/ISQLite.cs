﻿using SQLite;

namespace MavenSocial.DependencyServices
{
  public interface ISQLite
  {
    SQLiteConnection GetConnection();
  }
}
