﻿using MavenSocial.Models;
using System;

namespace MavenSocial.DependencyServices
{
  public interface IFacebookService
  {
    void Login(Action<FacebookUser, Exception> OnLoginCompleted);
    void Logout();
  }
}
