﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.DependencyServices
{
  public interface IDevice
  {
    string GetIdentifier();
  }
}
