﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ProfileUserInformation : ContentPage
  {
    #region Properties

    private UserProfileViewModel ViewModel => BindingContext as UserProfileViewModel;

    #endregion
    public ProfileUserInformation()
    {
      InitializeComponent();
      BindingContext = new UserProfileViewModel();
      GetUserProfileDetail();
      BindPhotoTab();
    }

    #region Control Events

    private void txtSearch_Focused(object sender, FocusEventArgs e)
    {
      SearchFrame.Opacity = 0.9;
    }

    private void txtSearch_Unfocused(object sender, FocusEventArgs e)
    {
      SearchFrame.Opacity = 0.3;
    }

    private void imgPhotos_Clicked(object sender, EventArgs e)
    {
      imgPhotos.Source = "Photos_Black.png";
      imgSaved.Source = "Saved.png";
      imgList.Source = "List_Gray.png";

      ShowPhotoTab();
    }

    private void imgList_Clicked(object sender, EventArgs e)
    {
      imgSaved.Source = "Saved.png";
      imgPhotos.Source = "PhototsIconLightGray.png";
      imgList.Source = "List_Black.png";
      BindMyList();
    }

    private void imgSaved_Clicked(object sender, EventArgs e)
    {
      imgSaved.Source = "Saved_Black.png";
      imgPhotos.Source = "PhototsIconLightGray.png";
      imgList.Source = "List_Gray.png";

      if (myItemList.ItemsSource == null)
      {
        ShowPhotoTab();
      }
      else
      {
        ShowPhotoTab();
      }
    }

    private void btnMyList_Clicked(object sender, EventArgs e)
    {
      BindMyList();
    }

    private void btnSavedLists_Clicked(object sender, EventArgs e)
    {
      BindSavedList();
    }

    private async void btnEdit_Clicked(object sender, EventArgs e)
    {
      await Navigation.PushModalAsync(new EditProfile());
    }

    #endregion

    #region Private Methods

    private async void GetUserProfileDetail()
    {
      try
      {
        ViewModel.IsBusy = true;
        UserProfile userProfile = new UserProfile();
        userProfile.Id = Convert.ToInt32(ApplicationConfiguration.UserId);
        UserProfile userProfileObj = await ViewModel.GetUserProfileInformationById(userProfile);

        if (userProfileObj != null)
        {
          if (userProfileObj.ProfilePictureBase64String == "AA==")
          {
            imgProfilePicture.Source = "logo.png";
           // lblTitle.TextColor = Color.Black;
            imgProfilePicture.HorizontalOptions = LayoutOptions.CenterAndExpand;
            imgProfilePicture.VerticalOptions = LayoutOptions.CenterAndExpand;
            imgProfilePicture.HeightRequest = 100;
            imgProfilePicture.WidthRequest = 100;
            btnBack.Source = "leftArrow.png";
            FrameOfSearchTextBox.BorderColor = Color.Black;
          }
          else
          {
            byte[] byteData = Convert.FromBase64String(userProfileObj.ProfilePictureBase64String);
            if (byteData != null)
            {
              imgProfilePicture.Source = ImageSource.FromStream(() => new MemoryStream(byteData));
            }
          }
         // lblTitle.Text = userProfileObj.Name;
          lblName.Text = userProfileObj.Name;
          lblCity.Text = userProfileObj.City;
          lblUserName.Text = "@" + userProfileObj.Username;
          lblPersonalBio.Text = userProfileObj.PersonalBio;
          lblWebSiteUrl.Text = userProfileObj.WebSiteUrl;
        }
        else
        {
          DependencyService.Get<INotificationSound>().Sound();
          await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.NoDetilsFound, SystemMessages.OK);
        }
      }
      catch (Exception e)
      {
        throw;
      }
      finally
      {
        ViewModel.IsBusy = false;
      }

    }

    private async void BindPhotoTab()
    {
      try
      {
        ViewModel.IsBusy = true;

        if (ListPhoto.ItemsSource == null)
        {
          //ViewModel.BindListOfPhotos();

          //if (ViewModel.ListOfPhotos != null || ViewModel.ListOfPhotos.Count > 1)
          //{
          //  ShowPhotoTab();
          //}
        }

      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }
    }

    private async void BindMyList()
    {
      try
      {
        ViewModel.IsBusy = true;
        if (myItemList.ItemsSource == null)
        {
         //ViewModel.BindListOfMyList();
        }

        myItemList.IsVisible = true;
        ListPhoto.IsVisible = false;
        ListSavedDetails.IsVisible = false;
        ButtosLayout.IsVisible = true;
        stkMyList.BackgroundColor = Color.FromHex("#BA54F1");
        lblMyList.TextColor = Color.White;
        stkSavedList.BackgroundColor = Color.White;
        lblSavedList.TextColor = Color.Black;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }

    }

    private async void BindSavedList()
    {
      try
      {
        ViewModel.IsBusy = true;
        if (ListSavedDetails.ItemsSource == null)
        {
         // ViewModel.BindListOfSavedItem();
        }

        myItemList.IsVisible = false;
        ListPhoto.IsVisible = false;
        ListSavedDetails.IsVisible = true;
        ButtosLayout.IsVisible = true;
        stkMyList.BackgroundColor = Color.White;
        lblMyList.TextColor = Color.Black;
        stkSavedList.BackgroundColor = Color.FromHex("#BA54F1");
        lblSavedList.TextColor = Color.White;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }
    }

    private void ShowPhotoTab()
    {
      ListPhoto.IsVisible = true;
      ListSavedDetails.IsVisible = false;
      ButtosLayout.IsVisible = false;
    }

    private void btnBack_Clicked(object sender, EventArgs e)
    {

    }
  }

  #endregion
}