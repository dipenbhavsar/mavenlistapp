﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class Page2 : ContentPage
  {
    public ObservableCollection<page2ImageSource> ListDetails { get; set; }
    public Page2()
    {
      InitializeComponent();
      ListDetails = new ObservableCollection<page2ImageSource>
      {
        new page2ImageSource{ ImageSource= "Rooh1.png"},
        new page2ImageSource{ ImageSource= "Rooh2.png"},
        new page2ImageSource{ ImageSource= "Rooh3.png"},
      };

      BindingContext = this;
    }
  }


  public class page2ImageSource
  {
    public string ImageSource { get; set; }
  }
}