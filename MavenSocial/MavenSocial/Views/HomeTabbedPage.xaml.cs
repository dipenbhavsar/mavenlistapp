﻿using MavenSocial.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeTabbedPage
    {
        public HomeTabbedPage()
        {
            try
            {
                InitializeComponent();
                Utility.GetCurrentLocation();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}