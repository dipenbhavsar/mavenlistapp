﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IndividualPosts : ContentPage
    {
        #region Properties
        private RecommendationViewModel ViewModel => BindingContext as RecommendationViewModel;

        #endregion
        public IndividualPosts(Int64 rid = 0)
        {
            InitializeComponent();
            BindingContext = new RecommendationViewModel();
            BindRecommendatationDetail(rid);
        }

        #region Control event

        /// <summary>
        /// Handle to Back Button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

    private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
    {
      if (ViewModel.Recommendatation.BusinessProfileId > 0)
      {
        BusinessProfileViewModel bViewModel = new BusinessProfileViewModel();
        BusinessProfile businessProfile = new BusinessProfile() { Id = ViewModel.Recommendatation.BusinessProfileId };
        businessProfile = await bViewModel.GetBusinessProfileDetailsByID(businessProfile);
        businessProfile.RecommendatationCount = ViewModel.Recommendatation.RCount;
        await Navigation.PushAsync(new BusinessProfilePage(businessProfile));
      }

    }
    #endregion

    #region Private Method

    /// <summary>
    /// Bind Business Profile Related Detail/Recommendatation Detail
    /// </summary>
    /// <param name="rid"></param>
    private async void BindRecommendatationDetail(Int64 rid)
        {
            try
            {
                ViewModel.IsBusy = true;
                await ViewModel.GetRecommendatationDetailById(rid,ApplicationConfiguration.UserId);
                lblDescription.Text = ViewModel.Recommendatation.Description;
                lblUserName.Text = ViewModel.Recommendatation.UserName;
                imgProfilePicture.Source = Utility.GetImageSourceFromBase64String(ViewModel.Recommendatation.Photo64String);
                lblRec.Text = ViewModel.Recommendatation.RCount.ToString();
                if (ViewModel.Recommendatation.businessProfile != null)
                {
                    BusinessProfile businessProfile = ViewModel.Recommendatation.businessProfile;
                    lblBusinessProfileName.Text = businessProfile.Name;
                    lblTag.Text = businessProfile.Tag + " " + businessProfile.cuisine + "    Open until " + (businessProfile.OpenTime.HasValue ? businessProfile.OpenTime.Value.ToShortTimeString() : "");
                    lblAddress.Text = businessProfile.Address;
                    lblWebsite.Text = businessProfile.WebSiteUrl;
                }
                if (ViewModel.Recommendatation.recommendadPhotos != null)
                {
                    lstRecommendatationPhotos.ItemsSource = ViewModel.Recommendatation.recommendadPhotos;
                }
                else
                {
                    List<RecommendadPhotos> recommendadPhotos = new List<RecommendadPhotos>();
                    var item = new RecommendadPhotos() { ListPhoto64String = "AA==" };
                    recommendadPhotos.Add(item);
                    lstRecommendatationPhotos.ItemsSource = recommendadPhotos;
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }



        #endregion

       
    }
}