﻿using DurianCode.PlacesSearchBar;
using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Newtonsoft.Json;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GoogleMap : ContentPage
    {
        #region Variables Used
        Geocoder geoCoder;
        CancellationTokenSource cts;
        /// This is fired multiple times while the user pans the bottom sheet. This variable captures the first intention of determining whether to open (pan up) or close (pan down)
        bool _panelActivated = false;

        public enum eViewStyle
        {
            Friends,
            Public,
            All
        }
        #endregion

        #region Properties
        private BusinessProfileViewModel ViewModel => BindingContext as BusinessProfileViewModel;
        public static double Longitude { get; set; }
        public static double Latitude { get; set; }
        public static bool IsLocation { get; set; }
        public static bool IsPinClicked { get; set; }
        public static string NextPageToken { get; set; }
        public static bool IsInitialCall { get; set; }
        public static string LocationString { get; set; }
        public double DistanceFromMiles { get; set; }
        public BusinessProfile SelectedBusinessProfile { get; set; } // To Store PlaceID
        public List<BusinessProfile> BusinessProfilesCollection { get; set; }

        private eViewStyle _viewStyle = eViewStyle.Friends;
        private eViewStyle SelectedView
        {
            get
            {
                return _viewStyle;
            }
            set
            {
                _viewStyle = value;
            }
        }

        private bool isFirstTimeLoad = true;

        #endregion

        #region Methods For Map Page

        /// <summary>
        /// Default Constructor
        /// </summary>
        public GoogleMap()
        {
            try
            {
                InitializeComponent();

                #region Assign Default values
                NextPageToken = string.Empty;
                DistanceFromMiles = 3;
                SelectedBusinessProfile = null;
                BindingContext = new BusinessProfileViewModel(new List<BusinessProfile>());
                SelectedView = eViewStyle.All;
                IsInitialCall = true;
                BusinessProfilesCollection = new List<BusinessProfile>();
                #endregion

                #region Events 
                mapp.PinClicked += Map_PinClicked;
                mapp.MapClicked += Map_MapClickedAsync;
                mapp.CameraIdled += Map_CameraIdled;

                #endregion

                #region Call Default Functions               
                DisplayView();
                GetCurrentLocation();
                #endregion

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method to hide Bottom sheet by default
        /// </summary>
        protected override async void OnAppearing()
        {
            try
            {
                if (isFirstTimeLoad)
                {
                    await GetCurrentLocation();
                    await SearchItem("", LocationString == "," ? "" : LocationString, "", isFirstTimeLoad);
                    isFirstTimeLoad = false;
                    DistanceFromMiles = 2;
                }

                MoveBottomSheet(true);
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method to deal with map move Event and Get New Results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Map_CameraIdled(object sender, CameraIdledEventArgs e)
        {
            try
            {
                var p = e.Position;
                Latitude = p.Target.Latitude;
                Longitude = p.Target.Longitude;
                LocationString = Latitude + "," + Longitude;
                if (!IsInitialCall && IsPinClicked == false)
                    await SearchItem(!string.IsNullOrEmpty(txtSearch.Text) ? txtSearch.Text : "", LocationString, "", false);
                else
                    IsInitialCall = false;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method To change Background of Toggle Buttons 
        /// </summary>
        private void DisplayView()
        {
            if (SelectedView == eViewStyle.Friends)
            {
                btnFriends.BackgroundColor = Color.FromHex("#BA54F1");
                btnFriends.BorderWidth = 0;
                btnPublic.BackgroundColor = Color.FromHex("#DADADA");
                btnPublic.BorderWidth = 1;
            }
            else if (SelectedView == eViewStyle.Public)
            {
                btnPublic.BackgroundColor = Color.Orange;
                btnPublic.BorderWidth = 0;
                btnFriends.BackgroundColor = Color.FromHex("#DADADA");
                btnFriends.BorderWidth = 1;
            }
            else
            {
                btnPublic.BackgroundColor = Color.FromHex("#DADADA");
                btnPublic.BorderWidth = 1;
                btnFriends.BackgroundColor = Color.FromHex("#DADADA");
                btnFriends.BorderWidth = 1;
            }
        }

        /// <summary>
        /// Method To move to a Location 
        /// </summary>
        protected void MoveToRegion()
        {
            try
            {
                mapp.MoveToRegion(MapSpan.FromCenterAndRadius(
                                  new Position(Latitude, Longitude), Distance.FromMiles(DistanceFromMiles)));
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }


        /// <summary>
        /// Method to Call Api And Show Pins on Map 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="location"></param>
        public async Task SearchItem(string name, string location, string nextPageToken = "", bool firstCall = false)
        {
            try
            {
                PermissionStatus permissionStatus = await Utility.CheckAndRequestLocationPermission();
                if (permissionStatus == PermissionStatus.Granted)
                {
                    MoveBottomSheet(true);
                    ViewModel.IsBusy = true;
                    //For the First Time Clear pins
                    if (nextPageToken == string.Empty && firstCall)
                    {
                        mapp.Pins.Clear();
                        BusinessProfilesCollection.Clear();
                    }

                    Google objRequestData = new Google
                    {
                        PlaceName = name,
                        Location = location,
                        ApiKey = ApplicationConfiguration.ApiKey,
                        NextPageToken = nextPageToken,
                        RecomendedBy = SelectedView == eViewStyle.Friends ? 0 : SelectedView == eViewStyle.Public ? 1 : 2,
                        UserId = Convert.ToInt32(ApplicationConfiguration.UserId),


                    };


                    await ViewModel.GetAllBusinessProfileByPageFilter(objRequestData);
                    if (ViewModel.businessProfileResponse != null)
                    {

                        NextPageToken = ViewModel.businessProfileResponse.NextPageToken;
                        btnClear.IsVisible = true;
                        //If Next Page available 
                        if (NextPageToken == nextPageToken && NextPageToken != string.Empty)
                        {
                            btnMore.IsVisible = false;
                            nextPageToken = string.Empty;
                        }
                        else
                        {
                            btnMore.IsVisible = true;
                        }

                        Pin pin;

                        List<BusinessProfile> businessProfilesList = ViewModel.businessProfileResponse.BusinessProfiles;

                        if (businessProfilesList != null && businessProfilesList.Count > 0)
                        {
                            BusinessProfilesCollection = BusinessProfilesCollection.Union(businessProfilesList).ToList();
                            foreach (BusinessProfile businessProfile in businessProfilesList)
                            {
                                if (mapp.Pins.Count == 0)
                                {
                                    Latitude = (double)businessProfile.Lat;
                                    Longitude = (double)businessProfile.Longi;
                                }

                                pin = new Pin
                                {
                                    Label = businessProfile.Name,
                                    Address = businessProfile.Address,
                                    Type = PinType.Place,
                                    Position = new Position((double)businessProfile.Lat, (double)businessProfile.Longi),
                                    Icon = SelectedView == eViewStyle.Friends ? BitmapDescriptorFactory.DefaultMarker(Color.Purple) : BitmapDescriptorFactory.DefaultMarker(Color.Orange),
                                    Tag = businessProfile.PlaceId // currently used To get Id of Restaurant
                                };
                                mapp.Pins.Add(pin);
                            }
                        }
                        if (BusinessProfilesCollection != null && BusinessProfilesCollection.Count > 0)
                        {
                            lstBusinessProfiles.ItemsSource = BusinessProfilesCollection;
                        }
                        if (firstCall)
                        {
                            MoveToRegion();
                        }
                    }
                }
                else
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    Utility.ShowToastInfo(SystemMessages.LocationPermissionDenied);
                }
                IsPinClicked = false;

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }

        }

        /// <summary>
        /// Method to add some Random Pins for Testing 
        /// </summary>
        /// <param name="color"></param>
        public void AddPins(Color color)
        {

            try
            {
                Pin pin;
                System.Random random = new System.Random();
                for (int i = 1; i <= 10; i++)
                {

                    pin = new Pin
                    {
                        Label = "Pin " + i,
                        Address = "Address of Pin " + i,
                        Type = PinType.Place,
                        Position = new Position(Latitude + random.NextDouble() / 2, Longitude + random.NextDouble() / 2),
                        //Icon = BitmapDescriptorFactory.DefaultMarker(color),
                        Icon = (Device.RuntimePlatform == Device.Android) ? BitmapDescriptorFactory.FromBundle("Rooh1.png") : BitmapDescriptorFactory.FromView(new Image() { Source = "Logo_B.png", WidthRequest = 30, HeightRequest = 30 }),
                        Tag = i

                    };
                    mapp.Pins.Add(pin);
                }
                MoveToRegion();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method To Move bottom sheet   
        /// The -negative value determines how many vertical units should the panel occuply on the screen.
        /// </summary>
        /// <param name="close"></param>
        private async void MoveBottomSheet(bool close)
        {
            try
            {
                double finalTranslation = close ? (Device.Idiom == TargetIdiom.Phone ? 1 : 1) : (Device.Idiom == TargetIdiom.Phone ? -159.0 : -150);
                await BottomSheet.TranslateTo(BottomSheet.X, finalTranslation, 450, Easing.SinIn);
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method To GetCurrent Location Of User
        /// </summary>
        /// <returns></returns>
        async Task GetCurrentLocation()
        {
            try
            {
                ViewModel.IsBusy = true;

                if (ApplicationConfiguration.Latitude == 0 && ApplicationConfiguration.Longitude == 0)
                {
                    await Utility.GetCurrentLocation();
                }
                Latitude = ApplicationConfiguration.Latitude;
                Longitude = ApplicationConfiguration.Longitude;
                LocationString = Latitude + "," + Longitude;
                MoveToRegion();

                ViewModel.IsBusy = false;

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method to maintain Cancelation token for location reuest
        /// </summary>
        protected override void OnDisappearing()
        {
            if (cts != null && !cts.IsCancellationRequested)
                cts.Cancel();
            base.OnDisappearing();
        }
        #endregion

        #region Events

        /// <summary>
        /// Method To get Friends Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnFriends_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (SelectedView != eViewStyle.Friends)
                {
                    SelectedView = eViewStyle.Friends;
                }
                else
                {
                    SelectedView = eViewStyle.All;
                }
                DisplayView();
                mapp.Pins.Clear();
                BusinessProfilesCollection.Clear();
                await SearchItem(!string.IsNullOrEmpty(txtSearch.Text) ? txtSearch.Text : "", LocationString, "", false);
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method To get Public Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnPublic_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (SelectedView != eViewStyle.Public)
                {
                    SelectedView = eViewStyle.Public;
                }
                else
                {
                    SelectedView = eViewStyle.All;
                }
                DisplayView();
                mapp.Pins.Clear();
                BusinessProfilesCollection.Clear();
                await SearchItem(!string.IsNullOrEmpty(txtSearch.Text) ? txtSearch.Text : "", LocationString, "", false);
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }

        /// <summary>
        /// Handles Search textbox completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void txtSearch_Completed(object sender, EventArgs e)
        {
            try
            {
                IsLocation = false;

                #region Code Not Used
                //if (this.txtSearch.Text.ToString() != string.Empty)
                //{
                //    await GetMapLocation(this.txtSearch.Text.ToString());
                //}

                //string keyword = !string.IsNullOrEmpty(this.txtSearch.Text) ? IsLocation == true ? string.Empty : this.txtSearch.Text.ToString() : string.Empty;
                //if (!IsLocation)
                //{
                //   await GetCurrentLocation();
                //} 
                #endregion

                string keyword = !string.IsNullOrEmpty(this.txtSearch.Text) ? this.txtSearch.Text.ToString() : string.Empty;
                await SearchItem(keyword, LocationString, "", true);

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method To get Pin Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Map_PinClicked(object sender, PinClickedEventArgs e)
        {
            try
            {
                e.Handled = true;
                IsPinClicked = true;
                int index = BusinessProfilesCollection.FindIndex(x => x.PlaceId == e.Pin.Tag.ToString());
                MoveBottomSheet(false);
                lstBusinessProfiles.ScrollTo(index);
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }

        /// <summary>
        /// Method When Map Clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Map_MapClickedAsync(object sender, EventArgs e)
        {
            MoveBottomSheet(true);
        }

        /// <summary>
        /// Method To Go to Business Profile Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                if (SelectedBusinessProfile != null)
                {
                    await Navigation.PushAsync(new BusinessProfilePage(SelectedBusinessProfile));
                }
                else
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.NoDetilsFound, SystemMessages.OK);
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }

        /// <summary>
        /// Method To make Text Box Visible 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_Focused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.9;

        }

        /// <summary>
        /// Method To make Textbox Transprant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void txtSearch_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                SearchFrame.Opacity = 0.3;
                IsLocation = false;
                string keyword = !string.IsNullOrEmpty(this.txtSearch.Text) ? this.txtSearch.Text.ToString() : string.Empty;

                if (!string.IsNullOrEmpty(keyword))
                {
                    await SearchItem(keyword, LocationString, "", true);
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method used to Drang The Bottom sheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PanGestureRecognizer_PanUpdated(object sender, PanUpdatedEventArgs e)
        {
            try
            {
                switch (e.StatusType)
                {
                    case GestureStatus.Started:
                        break;
                    case GestureStatus.Running:
                        if (_panelActivated)
                        {
                            return;
                        }
                        MoveBottomSheet(e.TotalY > 0);
                        _panelActivated = true;
                        break;
                    case GestureStatus.Completed:
                        _panelActivated = false;
                        break;
                    case GestureStatus.Canceled:
                        break;
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }


        /// <summary>
        /// Change Current Selection as per the Scroll
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstBusinessProfiles_Scrolled(object sender, ItemsViewScrolledEventArgs e)
        {
            try
            {
                if (e.CenterItemIndex >= 0)
                {
                    SelectedBusinessProfile = BusinessProfilesCollection[e.CenterItemIndex];
                    IsPinClicked = false;
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }

        /// <summary>
        /// if any BusnessProfile Is Recommended
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void imgLike_Clicked(object sender, EventArgs e)
        {
            try
            {
                ViewModel.IsBusy = true;
                if (SelectedBusinessProfile != null)
                {
                    if (SelectedBusinessProfile.IsRecommended == false)
                    {
                        RecommendationViewModel recommendationViewModel = new RecommendationViewModel();

                        Recommendatation recommendatation = new Recommendatation();

                        Int64 businessProfileId = await CommonFunctions.GetBusinessProfileId(SelectedBusinessProfile);

                        recommendatation.Uid = ApplicationConfiguration.UserId;
                        recommendatation.PlaceId = SelectedBusinessProfile.PlaceId;
                        recommendatation.BusinessProfileId = businessProfileId;

                        bool returnedVal = await recommendationViewModel.SaveRecommendatationPost(recommendatation);
                        if (returnedVal)
                        {
                            ImageButton imageButton = sender as ImageButton;
                            imageButton.Source = "Heart_R.png";
                            SelectedBusinessProfile.IsRecommended = true;
                            SelectedBusinessProfile.RecommendedImageSrc = "Heart_R.png";
                            //imageButton.IsEnabled = false;
                            Utility.ShowToastInfo(SystemMessages.RecommendedSuccessfully);
                        }
                    }
                    else
                    {
                        Utility.ShowToastInfo(SystemMessages.AlreadyRecommended);
                    }
                }
                else
                {
                    throw new Exception(SystemMessages.NoDetilsFound);
                }

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }

        #endregion

        #region Currently Not In Use but Useful
        /// <summary>
        /// Method To get More Results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnMore_Clicked(object sender, EventArgs e)
        {
            await SearchItem("", "", NextPageToken);
        }

        /// <summary>
        /// Method to clear the Result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Clicked(object sender, EventArgs e)
        {
            btnMore.IsVisible = false;
            btnClear.IsVisible = false;
            mapp.Pins.Clear();
            BusinessProfilesCollection.Clear();
            txtSearch.Text = string.Empty;
            SelectedView = eViewStyle.All;
            DisplayView();
            MoveToRegion();
        }

        /// <summary>
        /// Method to set map location based on Location using Geocode (Not used right Now)
        /// </summary>
        /// <param name="location"></param>
        public async void SetMapLocation(string location)
        {
            if (!string.IsNullOrEmpty(location))
            {
                geoCoder = new Geocoder();
                var approximateLocation = await geoCoder.GetPositionsForAddressAsync(location);
                if (approximateLocation != null && approximateLocation.Count() > 0)
                {
                    IsLocation = true;
                    foreach (var p in approximateLocation)
                    {
                        Longitude = p.Longitude;
                        Latitude = p.Latitude;
                        MoveToRegion();
                        mapp.Pins.Clear();
                        //AddPins(Color.Purple);
                    }
                }
                else
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.NoDetilsFound, SystemMessages.OK);

                }

            }
            else
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.NoDetilsFound, SystemMessages.OK);
            }
        }

        /// <summary>
        ///  Method to Get map location based on text using Xamarin.Essentials (Not used right Now)
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public async Task GetMapLocation(string location)
        {
            if (!string.IsNullOrEmpty(location))
            {
                var approximateLocation = await Geocoding.GetLocationsAsync(location);
                if (approximateLocation.Any())
                {
                    IsLocation = true;
                    Longitude = approximateLocation.FirstOrDefault().Longitude;
                    Latitude = approximateLocation.FirstOrDefault().Latitude;
                }
            }
        }


        #endregion

    }
}