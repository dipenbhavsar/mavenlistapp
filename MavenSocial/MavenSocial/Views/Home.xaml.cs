﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class Home : ContentPage
  {
    #region Properties
    //  private HomeViewModel ViewModel => BindingContext as HomeViewModel;
    private MavenUpdateTrackingViewModel ViewModel => BindingContext as MavenUpdateTrackingViewModel;

    #endregion

    #region Form Events

    public Home()
    {
      InitializeComponent();
    //  BindingContext = new HomeViewModel();
      BindingContext = new MavenUpdateTrackingViewModel();
    }

    protected async override void OnAppearing()
    {
            try
            {
                base.OnAppearing();
                GetUserProfileDetail();
                await GetAllNotiications();
            }
            catch(Exception ex)
            {

            }
    }

    #endregion

    #region Control Events

    /// <summary>
    /// Handles New Post button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void btnNewPost_Clicked(object sender, EventArgs e)
    {
      //Device.BeginInvokeOnMainThread(async () => await Navigation.PushAsync(new MakePost()));
      //await Navigation.PushModalAsync(new NavigationPage(new MakePost()));
      await Navigation.PushAsync(new MakePost());
    }

    /// <summary>
    /// Handles Notification List Item Selected Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void listNotification_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      // When Int64? Rid then check like this homeNotification.Rid.HasValue & homeNotification.Rid.Value > 0
      // If Rid is Int64 not Int64? then homeNotification.Rid > 0 as its always 0 if no value
      MavenUpdateTracking tracking = e.SelectedItem as MavenUpdateTracking;

        switch(tracking.UpdateType)
            {
                case 1:
                    if (tracking.UpdateId > 0)
                    {
                        await Navigation.PushAsync(new IndividualPosts(tracking.UpdateId));
                    }
                    break;
                case 2:
                    if (tracking.UpdateId > 0)
                    {
                        await Navigation.PushAsync(new FriendProfile(tracking.UserId));
                    }
                    break;
                case 3:
                    if (tracking.UpdateId > 0)
                    {
                        await Navigation.PushAsync(new FriendProfile(tracking.UserId));
                    }
                    break;
                case 4:
                    if (tracking.UpdateId > 0)
                    {
                        await Navigation.PushAsync(new FriendProfile(tracking.UserId));
                    }
                    break;
                default:
                    break;

            }
      
    }

    /// <summary>
    /// Handles Logout button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void btnLogout_Clicked(object sender, EventArgs e)
    {
      DependencyService.Get<INotificationSound>().Sound();
      bool returnedResult = await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.ConfirmLogout, SystemMessages.Yes, SystemMessages.No);
      if (returnedResult)
      {
        ApplicationConfiguration.ClearLoggedInUserValues();
        await Navigation.PushModalAsync(new Login());
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Method to get User Profile Details
    /// </summary>
    private void GetUserProfileDetail()
    {
      lblName.Text = !string.IsNullOrWhiteSpace(ApplicationConfiguration.Name) ? ApplicationConfiguration.Name : ApplicationConfiguration.Username;
      imgProfile.Source = Utility.GetImageSourceFromBase64String(ApplicationConfiguration.ProfilePicture);
    }

    /// <summary>
    /// Function To get All Notification from DB
    /// </summary>
    private async Task GetAllNotiications()
    {
      try
      {
        ViewModel.IsBusy = true;
        //HomeNotification homeNotification = new HomeNotification { Uid = ApplicationConfiguration.UserId };

        //if (listNotification.ItemsSource == null)
        //{
        //await ViewModel.GetAllNotificationByUserId(homeNotification);
        //}                

        await ViewModel.GetMavenUpdateTracking(ApplicationConfiguration.UserId);
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }
    }

    #endregion
  }
}