﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class Rooh : ContentPage
  {

    public ObservableCollection<Card> ListDetails { get; set; }
    public Rooh()
    {
      InitializeComponent();
      ListDetails = new ObservableCollection<Card>
      {
        new Card{ ImageSource= "Rooh1.png"},
        new Card{ ImageSource= "Rooh2.png"},
        new Card{ ImageSource= "Rooh3.png"},
      };
      
      BindingContext = this;
    }
        private void txtSearch_Focused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.9;

        }

        private void txtSearch_Unfocused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.3;
        }
    }

  public class Card
  {
    public string ImageSource { get; set; }
  }
}