﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FriendProfile : ContentPage
    {
        bool _panelActivated = false;
        public string selectedText { get; set; }
        public ObservableCollection<UserLocationBookmark> userLocationBookmarksList { get; set; }

        #region Properties
        private UserProfileViewModel ViewModel => BindingContext as UserProfileViewModel;

        public static long UserId { get; set; }

        public UserProfile UserDetails { get; set; }
        #endregion

        #region Constructor
        public FriendProfile(long id)
        {
            InitializeComponent();
            BindingContext = new UserProfileViewModel();
            if (id > 0)
            {
                GetUserProfileDetail(id);
                UserId = id;
            }
        }
        #endregion

        #region Control Events

        /// <summary>
        /// Method to hide Bottom sheet by default
        /// </summary>
        protected override void OnAppearing()
        {
            MoveBottomSheet(true);
        }

        /// <summary>
        /// Method To make Text Box Visible 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_Focused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.9;
        }

        /// <summary>
        /// Method To make Textbox Transprant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_Unfocused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.3;
            lstName.IsVisible = false;
            lstName.ItemsSource = null;

        }

        /// <summary>
        /// Handles Photos button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imgPhotos_Clicked(object sender, EventArgs e)
        {
            imgPhotos.Source = "Photos_Black.png";
            imgSaved.Source = "Saved.png";
            imgList.Source = "List_Gray.png";
            MoveBottomSheet(false);
            ShowPhotoTab();
        }

        /// <summary>
        /// Handles List button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imgList_Clicked(object sender, EventArgs e)
        {
            imgSaved.Source = "Saved.png";
            imgPhotos.Source = "PhototsIconLightGray.png";
            imgList.Source = "List_Black.png";
            MoveBottomSheet(false);
            BindMyList();
        }

        /// <summary>
        /// Handles Saved button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imgSaved_Clicked(object sender, EventArgs e)
        {
            imgSaved.Source = "Saved_Black.png";
            imgPhotos.Source = "PhototsIconLightGray.png";
            imgList.Source = "List_Gray.png";
            MoveBottomSheet(false);
            BindSavedPhotoList();

        }

        /// <summary>
        /// Handles SavedList button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void btnSavedLists_Clicked(object sender, EventArgs e)
        //{
        //    BindSavedList();
        //}

        /// <summary>
        /// Handles Edit button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnEdit_Clicked(object sender, EventArgs e)
        {
            try
            {
                Relations objReation = new Relations()
                {
                    Follower_id = ApplicationConfiguration.UserId,
                    Following_id = UserId,
                    Status = 1,
                    IsDeleted = false
                };
                UserRelationsViewModel userRelationsViewModel = new UserRelationsViewModel();
                await userRelationsViewModel.SaveUserRelations(objReation);
                if (userRelationsViewModel.relationsObj.Id > 0)
                {
                    FrameButton.BackgroundColor = Color.White;
                    btnEdit.IsEnabled = false;
                    btnEdit.Text = "Following";
                    Utility.ShowToastInfo(SystemMessages.Followed);

                    //Insert new Record in update Tracking 
                    try
                    {
                        MavenUpdateTracking trackingObj = new MavenUpdateTracking()
                        {
                            Isdeleted = false,
                            Message = " started following  ",
                            UserName = ApplicationConfiguration.Name,
                            Photostring = string.Empty,
                            ProfilePicture = ApplicationConfiguration.ProfilePicture,
                            UpdateId = UserId,
                            UpdateType = (int)UpdateCategories.Follow,
                            UserId = ApplicationConfiguration.UserId,
                        };
                        await CommonFunctions.SaveMavenUpdateTracking(trackingObj);

                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    //------------------------------------
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }
        }


        /// <summary>
        /// Handles Back button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnBack_Clicked(object sender, EventArgs e)
        {

            await Navigation.PopAsync();
        }

        /// <summary>
        /// Handles MyListItem Selected event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void myItemList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            UserLocationBookmark item = e.SelectedItem as UserLocationBookmark;
            await Navigation.PushModalAsync(new UserProfileBookmarkPage(item.Id, UserDetails));

        }

        /// <summary>
        /// Method used to Drang The Bottom sheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PanGestureRecognizer_PanUpdated(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    break;
                case GestureStatus.Running:
                    if (_panelActivated)
                    {
                        return;
                    }
                    MoveBottomSheet(e.TotalY > 0);
                    _panelActivated = true;
                    break;
                case GestureStatus.Completed:
                    _panelActivated = false;
                    break;
                case GestureStatus.Canceled:
                    break;
            }
        }

        /// <summary>
        /// Handles SavedList Item Selected event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private async void lstSavedDetails_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        //{
        //    SavedUserProfileBookmarks item = e.SelectedItem as SavedUserProfileBookmarks;
        //    await Navigation.PushAsync(new NavigationPage(new UserProfileBookmarkPage(item.Id)));
        //}

        /// <summary>
        /// Handle to Settings Button Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSettings_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new PopUpPageFromBottom("settings", null));
        }

        /// <summary>
        /// Handle to Search Friends By Name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(e.NewTextValue) || selectedText == e.NewTextValue)
                {
                    lstName.ItemsSource = null;
                    lstName.IsVisible = false;
                    absLayout.IsVisible = false;
                    lblTitle.IsVisible = true;
                    FrameButton.IsVisible = true;
                }
                else
                {
                    await ViewModel.GetUserProfileInformationByName(e.NewTextValue, ApplicationConfiguration.UserId);
                    if (ViewModel.ListOfUserProfile != null)
                    {
                        if (ViewModel.ListOfUserProfile.Count < 4)
                        {
                            lstName.HeightRequest = ViewModel.ListOfUserProfile.Count * 50;
                        }
                        lstName.ItemsSource = ViewModel.ListOfUserProfile;
                        lstName.IsVisible = true;
                        absLayout.IsVisible = true;
                        lblTitle.IsVisible = false;
                        FrameButton.IsVisible = false;
                    }
                    else
                    {
                        lstName.ItemsSource = null;
                        lstName.IsVisible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }

        }

        /// <summary>
        /// assign selected list item to search textBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void lstName_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                UserProfile userProfileSelectedItem = e.SelectedItem as UserProfile;
                if (userProfileSelectedItem != null)
                {
                    selectedText = userProfileSelectedItem.Name;
                    txtSearch.Text = selectedText;
                    lstName.ItemsSource = null;
                    lstName.IsVisible = false;
                    absLayout.IsVisible = false;
                    lblTitle.IsVisible = true;
                    FrameButton.IsVisible = true;
                    ((ListView)sender).SelectedItem = null;

                    await Navigation.PushAsync(new FriendProfile(userProfileSelectedItem.Id));


                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }

        }

        private async void btnimgBookMarkSaved_Clicked(object sender, EventArgs e)
        {
            try
            {
                ImageButton imageButton = sender as ImageButton;
                Grid listItem = (Grid)imageButton.Parent;
                Label lblId = (Label)listItem.Children[0];


                bool isSaved = userLocationBookmarksList.Where(s => s.Id == Convert.ToInt32(lblId.Text)).Select(s => s.IsSaved).FirstOrDefault();


                if (!isSaved)
                {
                    SavedUserProfileBookmarksViewModel savedUserProfileBookmarksViewModel = new SavedUserProfileBookmarksViewModel();
                    SavedUserProfileBookmarks savedUserProfileBookmarks = new SavedUserProfileBookmarks();
                    savedUserProfileBookmarks.BookmarkId = Convert.ToInt64(lblId.Text);
                    savedUserProfileBookmarks.UserId = ApplicationConfiguration.UserId;

                    SavedUserProfileBookmarks savedUserProfileBookmarksObj = await savedUserProfileBookmarksViewModel.SaveSavedUserProfileBookmarks(savedUserProfileBookmarks);
                    if (savedUserProfileBookmarksObj.Id > 0)
                    {
                        //Insert new Record in update Tracking 
                        try
                        {
                            UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
                            await userLocationBookmarkViewModel.GetUserLocationBookmarkById(savedUserProfileBookmarks.BookmarkId);

                            if (userLocationBookmarkViewModel.UserLocationBookmark != null)
                            {
                                MavenUpdateTracking trackingObj = new MavenUpdateTracking()
                                {
                                    Isdeleted = false,
                                    Message = " saved your " + userLocationBookmarkViewModel.UserLocationBookmark.Name + " list",
                                    UserName = ApplicationConfiguration.Name,
                                    Photostring = string.Empty,
                                    ProfilePicture = ApplicationConfiguration.ProfilePicture,
                                    UpdateId = UserId,
                                    UpdateType = (int)UpdateCategories.SavedList,
                                    UserId = ApplicationConfiguration.UserId,
                                };
                                await CommonFunctions.SaveMavenUpdateTracking(trackingObj);
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        //------------------------------------
                        imageButton.Source = "Save_P.png";
                        userLocationBookmarksList.FirstOrDefault(s => s.Id == Convert.ToInt32(lblId.Text)).IsSaved = true;
                        Utility.ShowToastInfo(SystemMessages.SavedSucessfully);
                    }
                }
                else
                {
                    Utility.ShowToastInfo(SystemMessages.AlreadyListSaved);
                }

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

    /// <summary>
    /// Handle Saved Photo selection changed Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void ctnSavedPhoto_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      try
      {
        long selected = (long)(e.CurrentSelection.FirstOrDefault() as RecommendadPhotos)?.Rid;
        await Navigation.PushAsync(new IndividualPosts(selected));
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        Utility.ShowToastInfo(ex.Message);
      }
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Method To Move bottom sheet   
    /// The -negative value determines how many vertical units should the panel occuply on the screen.
    /// </summary>
    /// <param name="close"></param>
    private async void MoveBottomSheet(bool close)
        {
            if (!close)
            {
                stkPersonalInformation.IsVisible = false;
            }
            else
            {
                stkPersonalInformation.IsVisible = true;

                imgPhotos.Source = "Photos_Black.png";
                imgSaved.Source = "Saved.png";
                imgList.Source = "List_Gray.png";
                ShowPhotoTab();
            }
            double finalTranslation = close ? (Device.Idiom == TargetIdiom.Phone ? 1 : 1) : (Device.Idiom == TargetIdiom.Phone ? -159.0 : -134.0);
            await BottomSheet.TranslateTo(BottomSheet.X, finalTranslation, 450, Easing.SinIn);
        }

        /// <summary>
        /// Method to Get UserProfile Detail
        /// </summary>
        private async void GetUserProfileDetail(long id)
        {
            try
            {
                ViewModel.IsBusy = true;
                UserProfile userProfile = new UserProfile();
                userProfile.Id = id;
                userProfile.CurrentUserId = ApplicationConfiguration.UserId;
                UserDetails = await ViewModel.GetUserProfileInformationById(userProfile);

                if (UserDetails != null)
                {
                    imgProfilePicture.Source = Utility.GetImageSourceFromBase64String(UserDetails.ProfilePictureBase64String);
                    lblFollowers.Text = UserDetails.Followers.ToString();
                    lblFollowing.Text = UserDetails.Following.ToString();
                    lblTitle.Text = !string.IsNullOrWhiteSpace(UserDetails.Name) ? UserDetails.Name : UserDetails.Username;
                    lblName.Text = UserDetails.Name;
                    lblCity.Text = UserDetails.City;
                    lblUserName.Text = "@" + UserDetails.Username;
                    lblPersonalBio.Text = UserDetails.PersonalBio;
                    lblWebSiteUrl.Text = UserDetails.WebSiteUrl;
                    if (UserDetails.IsFollowing)
                    {
                        FrameButton.BackgroundColor = Color.White;
                        btnEdit.IsEnabled = false;
                        btnEdit.Text = "Following";
                    }


                    if (UserDetails.RecommendadPhotosList != null)
                    {
                        ctnPhoto.ItemsSource = UserDetails.RecommendadPhotosList;
                    }
                }
                else
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.NoDetilsFound, SystemMessages.OK);
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }

        }

        /// <summary>
        /// Method To Bind MyList Item
        /// </summary>
        private async void BindMyList()
        {
            try
            {
                ViewModel.IsBusy = true;
                if (lstmyItemList.ItemsSource == null)
                {
                    UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
                    userLocationBookmarksList = await userLocationBookmarkViewModel.GetUserLocationBookmarkByUserId(UserId);
                    lstmyItemList.ItemsSource = userLocationBookmarksList;
                }
                lstmyItemList.IsVisible = true;
                ctnPhoto.IsVisible = false;
              ctnSavedPhoto.IsVisible = false;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }

        }

    //Hide SavedList in Friend's page
    /// <summary>
    /// Method To Bind SavedList Item
    /// </summary>
    //private async void BindSavedList()
    //{
    //    try
    //    {
    //        ViewModel.IsBusy = true;
    //        if (lstSavedDetails.ItemsSource == null)
    //        {
    //            SavedUserProfileBookmarksViewModel savedUserProfileBookmarksViewModel = new SavedUserProfileBookmarksViewModel();
    //            lstSavedDetails.ItemsSource = await savedUserProfileBookmarksViewModel.GetSavedUserProfileBookmarksByUserId(UserId);
    //        }

    //        lstmyItemList.IsVisible = false;
    //        ctnPhoto.IsVisible = false;
    //        lstSavedDetails.IsVisible = true;
    //        ButtosLayout.IsVisible = true;
    //        //stkMyList.BackgroundColor = Color.White;
    //        //lblMyList.TextColor = Color.Black;
    //        //stkSavedList.BackgroundColor = Color.FromHex("#BA54F1");
    //        //lblSavedList.TextColor = Color.White;
    //    }
    //    catch (Exception ex)
    //    {
    //        DependencyService.Get<INotificationSound>().Sound();
    //        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
    //    }
    //    finally
    //    {
    //        ViewModel.IsBusy = false;
    //    }
    //}

    /// <summary>
    /// Method to Bind Saved Photo tab
    /// </summary>
    private async void BindSavedPhotoList()
    {
      try
      {
        ViewModel.IsBusy = true;
        if (ctnSavedPhoto.ItemsSource == null)
        {
          SavedUserProfileBookmarksViewModel savedUserProfileBookmarksViewModel = new SavedUserProfileBookmarksViewModel();
          ctnSavedPhoto.ItemsSource = await savedUserProfileBookmarksViewModel.GetSavedRecommendationPhotosByUserId(UserId);
        }
        ctnSavedPhoto.IsVisible = true;
        lstmyItemList.IsVisible = false;
        ctnPhoto.IsVisible = false;
      //  lstSavedDetails.IsVisible = false;
        ButtosLayout.IsVisible = false;
        //stkMyList.BackgroundColor = Color.White;
        //lblMyList.TextColor = Color.Black;
        //stkSavedList.BackgroundColor = Color.FromHex("#BA54F1");
        //lblSavedList.TextColor = Color.White;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }
    }

    /// <summary>
    /// Method To Show Photos Grid
    /// </summary>
    /// 
    private void ShowPhotoTab()
        {
            ctnPhoto.IsVisible = true;
            lstmyItemList.IsVisible = false;
            ctnSavedPhoto.IsVisible = false;
            //lstSavedDetails.IsVisible = false;
            //ButtosLayout.IsVisible = false;
        }

    
  }

    #endregion
}
