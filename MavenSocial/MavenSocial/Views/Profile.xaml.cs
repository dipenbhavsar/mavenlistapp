﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class Profile : ContentPage
  {
    bool _panelActivated = false;
    public string selectedText { get; set; }
    #region Properties
    private UserProfileViewModel ViewModel => BindingContext as UserProfileViewModel;

    public UserProfile userProfileObj { get; set; }
    #endregion

    #region Constructor
    public Profile(long id)
    {
      InitializeComponent();
      BindingContext = new UserProfileViewModel();
      if (id > 0)
      {
        GetUserProfileDetail(id);
      }

      //BindPhotoTab();
    }
    public Profile()
    {
      InitializeComponent();
      BindingContext = new UserProfileViewModel();
      GetUserProfileDetail(Convert.ToInt32(ApplicationConfiguration.UserId));


      //BindPhotoTab();
    }
    #endregion

    #region Control Events

    /// <summary>
    /// Method to hide Bottom sheet by default
    /// </summary>
    protected override void OnAppearing()
    {
      MoveBottomSheet(true);
    }

    /// <summary>
    /// Method To make Text Box Visible 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtSearch_Focused(object sender, FocusEventArgs e)
    {
      SearchFrame.Opacity = 0.9;
    }

    /// <summary>
    /// Method To make Textbox Transprant
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtSearch_Unfocused(object sender, FocusEventArgs e)
    {
      SearchFrame.Opacity = 0.3;
      lstName.IsVisible = false;
      lstName.ItemsSource = null;

    }

    /// <summary>
    /// Handles Photos button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void imgPhotos_Clicked(object sender, EventArgs e)
    {
      imgPhotos.Source = "Photos_Black.png";
      imgSaved.Source = "Saved.png";
      imgList.Source = "List_Gray.png";
      MoveBottomSheet(false);

      ShowPhotoTab();
    }

    /// <summary>
    /// Handles List button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void imgList_Clicked(object sender, EventArgs e)
    {
      imgSaved.Source = "Saved.png";
      imgPhotos.Source = "PhototsIconLightGray.png";
      imgList.Source = "List_Black.png";
      MoveBottomSheet(false);
      BindMyList();
    }

    /// <summary>
    /// Handles Saved button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void imgSaved_Clicked(object sender, EventArgs e)
    {
      imgSaved.Source = "Saved_Black.png";
      imgPhotos.Source = "PhototsIconLightGray.png";
      imgList.Source = "List_Gray.png";
      MoveBottomSheet(false);
      BindSavedPhotoList();
    }

    /// <summary>
    /// Handles My List button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnMyList_Clicked(object sender, EventArgs e)
    {
      BindMyList();
    }

    /// <summary>
    /// Handles SavedList button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnSavedLists_Clicked(object sender, EventArgs e)
    {
      BindSavedList();
    }

    /// <summary>
    /// Handles Edit button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void btnEdit_Clicked(object sender, EventArgs e)
    {
      await Navigation.PushModalAsync(new EditProfile());
    }


    /// <summary>
    /// Handles Back button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnBack_Clicked(object sender, EventArgs e)
    {
      btnBack.IsVisible = false;
      MoveBottomSheet(true);
    }

    /// <summary>
    /// Handles MyListItem Selected event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void myItemList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      UserLocationBookmark item = e.SelectedItem as UserLocationBookmark;
      await Navigation.PushAsync(new UserProfileBookmarkPage(item.Id, userProfileObj));

    }

    /// <summary>
    /// Method used to Drang The Bottom sheet
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PanGestureRecognizer_PanUpdated(object sender, PanUpdatedEventArgs e)
    {
      switch (e.StatusType)
      {
        case GestureStatus.Started:
          break;
        case GestureStatus.Running:
          if (_panelActivated)
          {
            return;
          }
          MoveBottomSheet(e.TotalY > 0);
          _panelActivated = true;
          break;
        case GestureStatus.Completed:
          _panelActivated = false;
          break;
        case GestureStatus.Canceled:
          break;
      }
    }

    /// <summary>
    /// Handles SavedList Item Selected event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void lstSavedDetails_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      SavedUserProfileBookmarks item = e.SelectedItem as SavedUserProfileBookmarks;
      await Navigation.PushAsync(new UserProfileBookmarkPage(item.BookmarkId,userProfileObj));
    }


    /// <summary>
    /// Handles Setting Button Click Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void btnSettings_Clicked(object sender, EventArgs e)
    {
      await Navigation.PushPopupAsync(new PopUpPageFromBottom("settings", null));
    }

    /// <summary>
    /// Handle to Search Friends By Name
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
    {
      try
      {
        if (string.IsNullOrWhiteSpace(e.NewTextValue) || selectedText == e.NewTextValue)
        {
          lstName.ItemsSource = null;
          lstName.IsVisible = false;
          absLayout.IsVisible = false;
          lblTitle.IsVisible = true;
          FrameButton.IsVisible = true;
        }
        else
        {
          await ViewModel.GetUserProfileInformationByName(e.NewTextValue, ApplicationConfiguration.UserId);
          if (ViewModel.ListOfUserProfile != null)
          {
            if (ViewModel.ListOfUserProfile.Count < 4)
            {
              lstName.HeightRequest = ViewModel.ListOfUserProfile.Count * 50;
            }
            lstName.ItemsSource = ViewModel.ListOfUserProfile;
            lstName.IsVisible = true;
            absLayout.IsVisible = true;
            lblTitle.IsVisible = false;
            FrameButton.IsVisible = false;
          }
          else
          {
            lstName.ItemsSource = null;
            lstName.IsVisible = false;
          }
        }
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        Utility.ShowToastInfo(ex.Message);
      }

    }

    /// <summary>
    /// assign selected list item to search textBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void lstName_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      try
      {
        UserProfile userProfileSelectedItem = e.SelectedItem as UserProfile;
        if (userProfileSelectedItem != null)
        {
          selectedText = userProfileSelectedItem.Name;
          txtSearch.Text = selectedText;
          lstName.ItemsSource = null;
          lstName.IsVisible = false;
          absLayout.IsVisible = false;
          lblTitle.IsVisible = true;
          FrameButton.IsVisible = true;
          ((ListView)sender).SelectedItem = null;
          await Navigation.PushAsync(new FriendProfile(userProfileSelectedItem.Id));
        }
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        Utility.ShowToastInfo(ex.Message);
      }

    }

    /// <summary>
    /// Handles on click photos List and Redirect to Individual page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void ctnPhoto_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      try
      {
        long selected = (long)(e.CurrentSelection.FirstOrDefault() as RecommendadPhotos)?.Rid;
        await Navigation.PushAsync(new IndividualPosts(selected));
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        Utility.ShowToastInfo(ex.Message);
      }

    }

    /// <summary>
    /// Handles Saved Photos Selection Changed event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void ctnSavedPhoto_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      try
      {
        long selected = (long)(e.CurrentSelection.FirstOrDefault() as RecommendadPhotos)?.Rid;
        await Navigation.PushAsync(new IndividualPosts(selected));
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        Utility.ShowToastInfo(ex.Message);
      }
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Method To Move bottom sheet   
    /// The -negative value determines how many vertical units should the panel occuply on the screen.
    /// </summary>
    /// <param name="close"></param>
    private async void MoveBottomSheet(bool close)
    {
      if (!close)
      {
        btnBack.IsVisible = true;
        stkPersonalInformation.IsVisible = false;
      }
      else
      {
        btnBack.IsVisible = false;
        stkPersonalInformation.IsVisible = true;

        imgPhotos.Source = "Photos_Black.png";
        imgSaved.Source = "Saved.png";
        imgList.Source = "List_Gray.png";
        ShowPhotoTab();
      }
      double finalTranslation = close ? (Device.Idiom == TargetIdiom.Phone ? 1 : 1) : (Device.Idiom == TargetIdiom.Phone ? -159.0 : -134.0);
      await BottomSheet.TranslateTo(BottomSheet.X, finalTranslation, 450, Easing.SinIn);
    }

    /// <summary>
    /// Method to Get UserProfile Detail
    /// </summary>
    private async void GetUserProfileDetail(long id)
    {
      try
      {
        ViewModel.IsBusy = true;
        UserProfile userProfile = new UserProfile();
        userProfile.Id = id;
        userProfileObj = await ViewModel.GetUserProfileInformationById(userProfile);

        if (userProfileObj != null)
        {
          imgProfilePicture.Source = Utility.GetImageSourceFromBase64String(userProfileObj.ProfilePictureBase64String);
          // Commenting below code because don't know why  there is black colour change if picture is empty.
          //if (userProfileObj.ProfilePictureBase64String == "AA==")
          //{
          //    imgProfilePicture.Source = "Logo_Purple.png";
          //    lblTitle.TextColor = Color.Black;
          //    //imgProfilePicture.HorizontalOptions = LayoutOptions.CenterAndExpand;
          //    //imgProfilePicture.VerticalOptions = LayoutOptions.CenterAndExpand;
          //    //imgProfilePicture.HeightRequest = 100;
          //    //imgProfilePicture.WidthRequest = 100;
          //    //btnBack.Source = "leftArrow.png";
          //    //FrameOfSearchTextBox.BorderColor = Color.Black;
          //}
          //else
          //{
          //    byte[] byteData = Convert.FromBase64String(userProfileObj.ProfilePictureBase64String);
          //    if (byteData != null)
          //    {
          //        imgProfilePicture.Source = ImageSource.FromStream(() => new MemoryStream(byteData));
          //    }
          //}
          lblFollowers.Text = userProfileObj.Followers.ToString();
          lblFollowing.Text = userProfileObj.Following.ToString();
          lblTitle.Text = !string.IsNullOrWhiteSpace(userProfileObj.Name) ? userProfileObj.Name : userProfileObj.Username;
          lblName.Text = userProfileObj.Name;
          lblCity.Text = userProfileObj.City;
          lblUserName.Text = "@" + userProfileObj.Username;
          lblPersonalBio.Text = userProfileObj.PersonalBio;
          lblWebSiteUrl.Text = userProfileObj.WebSiteUrl;

          if (userProfileObj.RecommendadPhotosList != null)
          {
            ctnPhoto.ItemsSource = userProfileObj.RecommendadPhotosList;
          }
        }
        else
        {
          DependencyService.Get<INotificationSound>().Sound();
          await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.NoDetilsFound, SystemMessages.OK);
        }
      }
      catch (Exception e)
      {
        throw;
      }
      finally
      {
        ViewModel.IsBusy = false;
      }

    }

    /// <summary>
    /// Method To Bind MyList Item
    /// </summary>
    private async void BindMyList()
    {
      try
      {
        ViewModel.IsBusy = true;
        if (lstmyItemList.ItemsSource == null)
        {
          UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
          lstmyItemList.ItemsSource = await userLocationBookmarkViewModel.GetUserLocationBookmarkByUserId(ApplicationConfiguration.UserId);
        }
        lstmyItemList.IsVisible = true;
        ctnPhoto.IsVisible = false;
        lstSavedDetails.IsVisible = false;
        ButtosLayout.IsVisible = true;
        ctnSavedPhoto.IsVisible = false;
        stkMyList.BackgroundColor = Color.FromHex("#BA54F1");
        lblMyList.TextColor = Color.White;
        stkSavedList.BackgroundColor = Color.White;
        lblSavedList.TextColor = Color.Black;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }

    }

    /// <summary>
    /// Method To Bind SavedList Item
    /// </summary>
    private async void BindSavedList()
    {
      try
      {
        ViewModel.IsBusy = true;
        //if (lstSavedDetails.ItemsSource == null)
        //{
          SavedUserProfileBookmarksViewModel savedUserProfileBookmarksViewModel = new SavedUserProfileBookmarksViewModel();
          lstSavedDetails.ItemsSource = await savedUserProfileBookmarksViewModel.GetSavedUserProfileBookmarksByUserId(ApplicationConfiguration.UserId);
        //}

        lstmyItemList.IsVisible = false;
        ctnPhoto.IsVisible = false;
        lstSavedDetails.IsVisible = true;
        ButtosLayout.IsVisible = true;
        ctnSavedPhoto.IsVisible = false;
        stkMyList.BackgroundColor = Color.White;
        lblMyList.TextColor = Color.Black;
        stkSavedList.BackgroundColor = Color.FromHex("#BA54F1");
        lblSavedList.TextColor = Color.White;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }
    }

    /// <summary>
    /// Method to Bind Saved Photo tab
    /// </summary>
    private async void BindSavedPhotoList()
    {
      try
      {
        ViewModel.IsBusy = true;
        if (ctnSavedPhoto.ItemsSource == null)
        {
          SavedUserProfileBookmarksViewModel savedUserProfileBookmarksViewModel = new SavedUserProfileBookmarksViewModel();
          ctnSavedPhoto.ItemsSource = await savedUserProfileBookmarksViewModel.GetSavedRecommendationPhotosByUserId(ApplicationConfiguration.UserId);
        }
        ctnSavedPhoto.IsVisible = true;
        lstmyItemList.IsVisible = false;
        ctnPhoto.IsVisible = false;
        lstSavedDetails.IsVisible = false;
        ButtosLayout.IsVisible = false;
        stkMyList.BackgroundColor = Color.White;
        lblMyList.TextColor = Color.Black;
        stkSavedList.BackgroundColor = Color.FromHex("#BA54F1");
        lblSavedList.TextColor = Color.White;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }
    }

    /// <summary>
    /// Method To Show Photos Grid
    /// </summary>
    private void ShowPhotoTab()
    {
      ctnPhoto.IsVisible = true;
      lstSavedDetails.IsVisible = false;
      ButtosLayout.IsVisible = false;
      ctnSavedPhoto.IsVisible = false;
    }

   
  }

  #endregion
}
