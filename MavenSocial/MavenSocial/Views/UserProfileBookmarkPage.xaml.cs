﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserProfileBookmarkPage : ContentPage
    {
        #region Properties
        public List<BusinessProfile> businessProfileList { get; set; }
        private UserLocationBookmarkViewModel ViewModel => BindingContext as UserLocationBookmarkViewModel;

        public UserProfile UserProfile { get; set; }

        #endregion

        #region constructor
        public UserProfileBookmarkPage(Int64 id, Object obj)
        {
            InitializeComponent();
            BindingContext = new UserLocationBookmarkViewModel();

            if (obj != null)
            {
                UserProfile = obj as UserProfile;

                BindUserLocationBookmarkById(id);
            }
        }
        //public UserProfileBookmarkPage(UserProfile userProfile)
        //{
        //    InitializeComponent();
        //    BindingContext = new UserLocationBookmarkViewModel();
        //    BindUserLocationBookmarkById(userProfile.Id);
        //}

        #endregion

        #region Control Events
        private void txtSearch_Focused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.9;
        }

        private void txtSearch_Unfocused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.3;
        }

        private void btnSaved_Clicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            button.ImageSource = "BlueBookMark.png";
        }

        private void btnSettings_Clicked(object sender, EventArgs e)
        {

        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();// (new HomeTabbedPage());
        }

        private async void btnBookmark_Clicked(object sender, EventArgs e)
        {
            ImageButton imageButton = (ImageButton)sender;
            Grid grid = (Grid)((Frame)imageButton.Parent).Parent;
            Label label = (Label)grid.Children[0];

            BusinessProfile businessProfileObj = businessProfileList.FirstOrDefault(b => b.Id == Convert.ToInt64(label.Text));

            if (businessProfileObj != null)
            {
                await Navigation.PushPopupAsync(new PopUpPageFromBottom("Bookmark", businessProfileObj));
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Method To Bind List Of UserLocationBookmark
        /// </summary>
        /// <param name="id"></param>
        private async void BindUserLocationBookmarkById(Int64 id)
        {
            try
            {
                ViewModel.IsBusy = true;
                await ViewModel.GetUserLocationBookmarkById(id);
                businessProfileList = ViewModel.UserLocationBookmark.businessProfiles;
                lblBookmarkName.Text = ViewModel.UserLocationBookmark.Name;
                lblTitle.Text = !string.IsNullOrWhiteSpace(UserProfile.Name) ? UserProfile.Name : UserProfile.Username;

                if (ApplicationConfiguration.UserId == UserProfile.Id)
                {
                    lstUserLocationBookmark.IsVisible = true;
                    lstFriendLocationBookmark.IsVisible = false;
                    lstUserLocationBookmark.ItemsSource = businessProfileList;
                }
                else
                {
                    lstUserLocationBookmark.IsVisible = false;
                    lstFriendLocationBookmark.IsVisible = true;
                    lstFriendLocationBookmark.ItemsSource = businessProfileList;
                }
                //if (lstLocationBookmark.ItemsSource == null)
                //{
                //  await ViewModel.GetUserLocationBookmarkById(id);
                //  businessProfileList = ViewModel.UserLocationBookmark.businessProfiles;
                //  lstLocationBookmark.ItemsSource = businessProfileList;
                //  lblBookmarkName.Text = ViewModel.UserLocationBookmark.Name;
                //  lblTitle.Text = ApplicationConfiguration.Name;

                //}

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }

        }
        #endregion


    }
}