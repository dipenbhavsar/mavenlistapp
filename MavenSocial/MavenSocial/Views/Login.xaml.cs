﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        #region Properties

        private UserProfileViewModel ViewModel => BindingContext as UserProfileViewModel;

        #endregion

        #region Form Event

        public Login()
        {
            InitializeComponent();
            BindingContext = new UserProfileViewModel();
            if (Debugger.IsAttached)
            {
                txtPassword.Text = "j";
                txtUserName.Text = "j";
            }
        }

        #endregion

        #region Control Events

        /// <summary>
        /// Handles Forgot Passowrd tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void tapForgotPassword_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new ForgotPassword());
        }

        /// <summary>
        /// Handles Sign In button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSignIn_Clicked(object sender, EventArgs e)
        {
            bool returnedValue = ValidateLoginForm();
            if (returnedValue)
            {
                try
                {
                    ViewModel.IsBusy = true;
                    UserProfile userProfile = new UserProfile { Username = txtUserName.Text, Password = txtPassword.Text };
                    bool returnedVal = await ViewModel.DoUserLoginByUserNameAndPassword(userProfile);
                    if (returnedVal)
                    {
                        await Navigation.PushModalAsync(new HomeTabbedPage());
                    }
                }
                catch (Exception ex)
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
                }
                finally
                {
                    ViewModel.IsBusy = false;
                }
            }
        }

        /// <summary>
        /// Handles Create Account Tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void tapCreateAccount_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new RegisterUser());
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method to validate login form
        /// </summary>
        /// <returns></returns>
        private bool ValidateLoginForm()
        {
            lblUsername.IsVisible = string.IsNullOrWhiteSpace(txtUserName.Text);
            lblPassword.IsVisible = string.IsNullOrWhiteSpace(txtPassword.Text);

            if (string.IsNullOrWhiteSpace(txtUserName.Text) || string.IsNullOrWhiteSpace(txtPassword.Text))
            {
                return false;
            }

            return true;
        }


        #endregion
    }
}