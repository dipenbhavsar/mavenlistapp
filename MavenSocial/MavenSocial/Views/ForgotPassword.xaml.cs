﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ForgotPassword : ContentPage
  {
    #region Properties

    #endregion

    #region Form Event

    public ForgotPassword()
    {
      InitializeComponent();
    }

    #endregion

    #region Control Events

    /// <summary>
    /// Handles Sign In button tap event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void tapSignIn_Tapped(object sender, EventArgs e)
    {
      await Navigation.PushModalAsync(new Login());
    }

    #endregion
  }
}