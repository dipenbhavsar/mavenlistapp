﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.IO;
using DurianCode.PlacesSearchBar;
using System.Text.RegularExpressions;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class EditProfile : ContentPage
  {
    #region Properties

    private UserProfileViewModel ViewModel => BindingContext as UserProfileViewModel;
    private MediaFile _mediaFile;
    string image64String;
    private UserProfile _userProfile;

    #endregion

    #region Form Events

    public EditProfile()
    {
      InitializeComponent();
      BindingContext = new UserProfileViewModel();
      search_bar.ApiKey = ApplicationConfiguration.ApiKey;
      search_bar.Type = PlaceType.Cities;
      search_bar.Components = new Components("country:us");
      search_bar.PlacesRetrieved += Search_Bar_PlacesRetrieved;
      search_bar.TextChanged += Search_Bar_TextChanged;
      search_bar.MinimumSearchText = 2;
      results_list.ItemSelected += Results_List_ItemSelected;
      _userProfile = null;
    }

    public EditProfile(UserProfile userProfile = null)
    {
      InitializeComponent();
      BindingContext = new UserProfileViewModel();
      search_bar.ApiKey = ApplicationConfiguration.ApiKey;
      search_bar.Type = PlaceType.Cities;
      search_bar.Components = new Components("country:us");
      search_bar.PlacesRetrieved += Search_Bar_PlacesRetrieved;
      search_bar.TextChanged += Search_Bar_TextChanged;
      search_bar.MinimumSearchText = 2;
      results_list.ItemSelected += Results_List_ItemSelected;
      _userProfile = userProfile;
    }

    protected override async void OnAppearing()
    {
      await BindUserProfileDetailsAndSetControlValues(_userProfile);
      base.OnAppearing();
    }

    #endregion

    #region Control Events

    /// <summary>
    /// Handles Profile Image tap event
    /// </summary>
    /// <param name="sender"></param> 
    /// <param name="e"></param>
    private async void tapProfileImage_Tapped(object sender, EventArgs e)
    {
      try
      {
        await CrossMedia.Current.Initialize();

        if (!CrossMedia.Current.IsPickPhotoSupported)
        {
          await DisplayAlert(SystemMessages.MavenSocial, SystemMessages.PickPhoto, SystemMessages.OK);
          return;
        }

        _mediaFile = await CrossMedia.Current.PickPhotoAsync();

        if (_mediaFile == null)
        {
          return;
        }

        Byte[] imageByte = File.ReadAllBytes(_mediaFile.Path);
        image64String = Convert.ToBase64String(imageByte);
        imgProfile.Source = ImageSource.FromStream(() => new MemoryStream(imageByte));
        FrameWithImage.IsVisible = true;
        FrameWithoutImage.IsVisible = false;
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
    }

    /// <summary>
    /// Handles Cancel button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void btnCancel_Clicked(object sender, EventArgs e)
    {
      await Navigation.PushModalAsync(new HomeTabbedPage());
      //await App.Current.MainPage.Navigation.PopModalAsync();
    }

    /// <summary>
    /// Handles Next button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void btnNext_Clicked(object sender, EventArgs e)
    {
      try
      {
        bool returnedValue = ValidateLoginForm();
        if (returnedValue)
        {
          try
          {
            ViewModel.IsBusy = true;
            UserProfile userProfile = ViewModel.UserProfileObj;

            if (userProfile == null)
            {
              userProfile = new UserProfile();
              if (!string.IsNullOrWhiteSpace(_userProfile.FacebookId))
              {
                userProfile.FacebookId = _userProfile.FacebookId;
                userProfile.FacebookAccessToken = _userProfile.FacebookAccessToken;
                userProfile.FacebookProfilePictureUrl = _userProfile.FacebookProfilePictureUrl;
              }
            }

            userProfile.Name = txtName.Text;
            userProfile.Username = txtUserName.Text;
            userProfile.Country = txtCountry.Text;
            userProfile.State = txtState.Text;
            userProfile.City = txtCity.Text;
            userProfile.Email = txtEmail.Text;
            userProfile.ContactNo = txtPhoneNumber.Text;
            userProfile.WebSiteUrl = txtWebSite.Text;
            userProfile.PersonalBio = txtBio.Text;
            if (txtBirthDate.Date.Date < DateTime.Now.Date)
            {
              userProfile.BirthDate = txtBirthDate.Date;
            }

            if (!string.IsNullOrWhiteSpace(image64String))
            {
              userProfile.ProfilePictureBase64String = image64String;
            }

            if (rbMale.IsChecked == true)
            {
              userProfile.Gender = rbMale.Content.ToString();
            }
            else if (rbFeMale.IsChecked == true)
            {
              userProfile.Gender = rbFeMale.Content.ToString();
            }
            else
            {
              userProfile.Gender = rbOther.Content.ToString();
            }

            bool returnedVal = await ViewModel.SaveUserProfileInformation(userProfile);
            if (returnedVal)
            {
              ApplicationConfiguration.Email = ViewModel.UserProfileObj.Email;
              ApplicationConfiguration.Name = ViewModel.UserProfileObj.Name;
              ApplicationConfiguration.UserId = ViewModel.UserProfileObj.Id;
              if (!string.IsNullOrWhiteSpace(ViewModel.UserProfileObj.ProfilePictureBase64String) && ViewModel.UserProfileObj.ProfilePictureBase64String != "AA==")
              {
                ApplicationConfiguration.ProfilePicture = ViewModel.UserProfileObj.ProfilePictureBase64String;
              }

              await Navigation.PushModalAsync(new HomeTabbedPage());
            }
          }
          catch (Exception ex)
          {
            DependencyService.Get<INotificationSound>().Sound();
            await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
          }
          finally
          {
            ViewModel.IsBusy = false;
          }
        }
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Method to validate login form
    /// </summary>
    /// <returns></returns>
    private bool ValidateLoginForm()
    {
      try
      {
        lblName.IsVisible = string.IsNullOrWhiteSpace(txtName.Text);
        lblUsername.IsVisible = string.IsNullOrWhiteSpace(txtUserName.Text);
        lblEmail.IsVisible = string.IsNullOrWhiteSpace(txtEmail.Text);
        if (string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrWhiteSpace(txtUserName.Text) || string.IsNullOrWhiteSpace(txtEmail.Text))
        {
          scrollView.ScrollToAsync(stkEdit, ScrollToPosition.Start, true);
          return false;
        }
        if (txtUserName.Text.Length <= 3)
        {
          scrollView.ScrollToAsync(stkEdit, ScrollToPosition.Start, true);
          lblUsernameLength.IsVisible = true;
          return false;
        }
        if (!Regex.IsMatch(txtEmail.Text, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
        {
          lblEmailValidation.IsVisible = true;
          return false;
        }


      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
      return true;
    }

    /// <summary>
    /// Method to bind User Profile Details And Set Control Values 
    /// </summary>
    private async Task BindUserProfileDetailsAndSetControlValues(UserProfile userProfile)
    {
      try
      {
        UserProfile userProfileObj = null;
        if (userProfile == null && ApplicationConfiguration.UserId > 0)
        {
          userProfile = new UserProfile();
          userProfile.Id = ApplicationConfiguration.UserId;
        }

        if (userProfile.Id > 0)
        {
          userProfileObj = await ViewModel.GetUserProfileInformationById(userProfile);
        }
        else
        {
          userProfileObj = userProfile;
        }

        if (userProfileObj != null)
        {
          txtEmail.IsEnabled = string.IsNullOrWhiteSpace(userProfileObj.Email);
          txtUserName.IsEnabled = string.IsNullOrWhiteSpace(userProfileObj.Username);
          txtName.Text = userProfileObj.Name;
          txtUserName.Text = userProfileObj.Username;
          txtCountry.Text = userProfileObj.Country;
          txtState.Text = userProfileObj.State;
          txtCity.Text = userProfileObj.City;
          txtEmail.Text = userProfileObj.Email;

          if (userProfileObj.Gender == "Male")
          {
            rbMale.IsChecked = true;
          }
          else if (userProfileObj.Gender == "Female")
          {
            rbFeMale.IsChecked = true;
          }
          else if (userProfileObj.Gender == "Other")
          {
            rbOther.IsChecked = true;
          }

          txtBirthDate.Date = !string.IsNullOrEmpty(userProfileObj.BirthDate.ToString()) ? Convert.ToDateTime(userProfileObj.BirthDate) : DateTime.Today;
          txtPhoneNumber.Text = userProfileObj.ContactNo;
          txtBio.Text = userProfileObj.PersonalBio;
          txtWebSite.Text = userProfileObj.WebSiteUrl;
          if (string.IsNullOrWhiteSpace(ApplicationConfiguration.ProfilePicture) && userProfileObj.ProfilePictureBase64String != "AA==")
          {
            ApplicationConfiguration.ProfilePicture = userProfileObj.ProfilePictureBase64String;
            imgProfile.Source = Utility.GetImageSourceFromBase64String(ApplicationConfiguration.ProfilePicture);
          }
        }
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
    }

    #endregion

    #region Search Place Methods

    /// <summary>
    /// Handles Search Bar Place Retrieve Methods
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="result"></param>
    void Search_Bar_PlacesRetrieved(object sender, AutoCompleteResult result)
    {
      try
      {
        results_list.ItemsSource = result.AutoCompletePlaces;
        ViewModel.IsBusy = false;

        if (result.AutoCompletePlaces != null && result.AutoCompletePlaces.Count > 0)
          results_list.IsVisible = true;
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    /// <summary>
    /// Handles Search Bar Text change event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void Search_Bar_TextChanged(object sender, TextChangedEventArgs e)
    {
      try
      {
        if (!string.IsNullOrEmpty(e.NewTextValue))
        {
          results_list.IsVisible = false;
          ViewModel.IsBusy = true;
        }
        else
        {
          results_list.IsVisible = true;
          ViewModel.IsBusy = false;
        }
      }
      catch (Exception ex)
      {
        ExceptionMessage.ShowErrorMessage(ex);
      }
    }

    /// <summary>
    /// Handles Result List Item selected event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    async void Results_List_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      try
      {
        if (e.SelectedItem == null)
          return;

        ViewModel.IsBusy = true;
        results_list.IsVisible = false;
        search_bar.Text = string.Empty;
        var prediction = (AutoCompletePrediction)e.SelectedItem;
        results_list.SelectedItem = null;

        var place = await Places.GetPlace(prediction.Place_ID, ApplicationConfiguration.ApiKey);

        if (place != null)
        {
          results_list.IsVisible = false;
          search_bar.Text = string.Empty;
          txtCity.Text = place.Name;
          string formattedAddress = place.FormattedAddress;
          txtCity.Text = place.Name;
          if (!string.IsNullOrWhiteSpace(formattedAddress))
          {
            string[] spltAddr = formattedAddress.Split(',');
            txtState.Text = spltAddr[1].Trim();
            txtCountry.Text = spltAddr[2].Trim();
          }
        }
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
      }
      finally
      {
        ViewModel.IsBusy = false;
      }
    }
    #endregion

  }
}  