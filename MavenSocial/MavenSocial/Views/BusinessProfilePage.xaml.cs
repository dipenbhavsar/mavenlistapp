﻿using DurianCode.PlacesSearchBar;
using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class BusinessProfilePage : ContentPage
    {
        #region Properties
        private BusinessProfileViewModel ViewModel => BindingContext as BusinessProfileViewModel;
        public Int64 BusinessProfileId { get; set; }
        #endregion

        #region Form Events
        public BusinessProfilePage()
        {
            InitializeComponent();
            BindingContext = new BusinessProfileViewModel();
            GetCurrentLocation();

        }

        public BusinessProfilePage(BusinessProfile businessProfile)
        {
            try
            {
                InitializeComponent();
                BindingContext = new BusinessProfileViewModel(businessProfile);
                GetCurrentLocation();
                GetRestaurantDetails();
                BindList(businessProfile.Id);

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }


        /// <summary>
        /// Method To make Text Box Visible 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_Focused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.9;

        }

        /// <summary>
        /// Method To make Textbox Transprant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_Unfocused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.3;
        }

        /// <summary>
        /// Handles Back button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnPrevious_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        /// <summary>
        /// when Maven Tab is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TapGestureRecognizer_Maven(object sender, EventArgs e)
        {
            stkMavens.IsVisible = true;
            stkPhotos.IsVisible = false;
            stkMap.IsVisible = false;
            imgList.Source = "Logo_Grid_B.png";
            imgPhotos.Source = "Photos_Grid_G.png";
            imgMap.Source = "Map_Grid_G.png";
        }

        /// <summary>
        /// when photo tab Clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TapGestureRecognizer_Photos(object sender, EventArgs e)
        {
            try
            {
                BindPhotoTab();
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// when Map tab Clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TapGestureRecognizer_Map(object sender, EventArgs e)
        {
            try
            {
                stkMavens.IsVisible = false;
                stkPhotos.IsVisible = false;
                stkMap.IsVisible = true;
                imgList.Source = "Logo_Grid_G.png";
                imgPhotos.Source = "Photos_Grid_G.png";
                imgMap.Source = "Map_Grid_B.png";
                GetBusinessLocation();
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }
        /// <summary>
        /// Method To GetCurrent Location Of User
        /// </summary>
        /// <returns></returns>
        async Task GetCurrentLocation()
        {
            try
            {
                ViewModel.IsBusy = true;
                if (ApplicationConfiguration.Latitude == 0 && ApplicationConfiguration.Longitude == 0)
                {
                    await Utility.GetCurrentLocation();
                }
                ViewModel.IsBusy = false;

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method To Move Region
        /// </summary>
        private void MoveToRegion()
        {
            Task.Delay(200);
            mapp.MoveToRegion(MapSpan.FromCenterAndRadius(
                new Position((double)ViewModel.businessProfileObj.Lat, (double)ViewModel.businessProfileObj.Longi), Distance.FromMiles(2)));
        }

        /// <summary>
        /// Handle To Recommeded Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnRecommend_Clicked(object sender, EventArgs e)
        {
            try
            {
                ViewModel.IsBusy = true;

                if (ViewModel.businessProfileObj.IsRecommended == false)
                {
                    RecommendationViewModel RecommendationViewModel = new RecommendationViewModel();

                    Recommendatation recommendatation = new Recommendatation();

                    Int64 businessProfileId = await CommonFunctions.GetBusinessProfileId(ViewModel.businessProfileObj);

                    recommendatation.Uid = ApplicationConfiguration.UserId;
                    recommendatation.PlaceId = ViewModel.businessProfileObj.PlaceId;
                    recommendatation.BusinessProfileId = businessProfileId;

                    bool returnedVal = await RecommendationViewModel.SaveRecommendatationPost(recommendatation);
                    if (returnedVal)
                    {
                        btnRecommend.Source = "Heart_R.png";
                        ViewModel.businessProfileObj.IsRecommended = true;
                        ViewModel.businessProfileObj.RecommendedImageSrc = "Heart_R.png";

                        Utility.ShowToastInfo(SystemMessages.RecommendedSuccessfully);
                    }
                }
                else
                {
                    Utility.ShowToastInfo(SystemMessages.AlreadyRecommended);
                }

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }


        /// <summary>
        /// Handle to Bookmark saved Tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSave_Clicked(object sender, EventArgs e)
        {
            try
            {
                btnSave.Source = "Save_P.png";
                Int64 businessProfileId = await CommonFunctions.GetBusinessProfileId(ViewModel.businessProfileObj);
                ViewModel.businessProfileObj.Id = businessProfileId;
                //Commented Due To popup Issue 
                await Navigation.PushPopupAsync(new PopUpPageFromBottom("Bookmark", ViewModel.businessProfileObj));
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }


        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// when Friends Recomendataion Is Clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void lstMavens_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                Recommendatation recommendatation = e.SelectedItem as Recommendatation;

                if (recommendatation.Id > 0)
                {
                    await Navigation.PushAsync(new IndividualPosts(recommendatation.Id));
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }

    /// <summary>
    /// Handles CollectionView List selection changed event and Redirect to IndividualPost page 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void ctnPhoto_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      try
      {
        long selected = (long)(e.CurrentSelection.FirstOrDefault() as RecommendadPhotos)?.Rid;
        await Navigation.PushAsync(new IndividualPosts(selected));
      }
      catch (Exception ex)
      {
        DependencyService.Get<INotificationSound>().Sound();
        Utility.ShowToastInfo(ex.Message);
      }
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Get Business Profile Location on map
    /// </summary>
    private void GetBusinessLocation()
        {
            try
            {
                if (ViewModel.businessProfileObj.Lat != null && ViewModel.businessProfileObj.Longi != null)
                {
                    Pin pin = new Pin
                    {
                        Label = ViewModel.businessProfileObj.Name,
                        Address = ViewModel.businessProfileObj.Address,
                        Type = PinType.Place,
                        Position = new Position((double)ViewModel.businessProfileObj.Lat, (double)ViewModel.businessProfileObj.Longi),
                        Icon = BitmapDescriptorFactory.DefaultMarker(Color.Blue)
                    };
                    mapp.Pins.Add(pin);
                    MoveToRegion();
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method to get Restaurant Details By Bysiness Profile
        /// </summary>
        private async void GetRestaurantDetails()
        {
            try
            {
                ViewModel.IsBusy = true;
                if (ViewModel.businessProfileObj != null)
                {

                    if (ViewModel.businessProfileObj.Id > 0)
                    {
                        lblTag.Text = ViewModel.businessProfileObj.Tag + "  " + ViewModel.businessProfileObj.cuisine + "   " + "Open Untill " + (ViewModel.businessProfileObj.OpenTime.HasValue ? ViewModel.businessProfileObj.OpenTime.Value.ToShortTimeString() : "");
                        lblRec.Text = ViewModel.businessProfileObj.RecommendatationCount.ToString();
                        lblWebsite.Text = ViewModel.businessProfileObj.WebSiteUrl;
                    }
                    if (ViewModel.businessProfileObj.Lat != null && ViewModel.businessProfileObj.Longi != null
                           && ApplicationConfiguration.Latitude != 0 && ApplicationConfiguration.Longitude != 0)
                    {
                        double miles = Location.CalculateDistance((double)ViewModel.businessProfileObj.Lat, (double)ViewModel.businessProfileObj.Longi, ApplicationConfiguration.Latitude, ApplicationConfiguration.Longitude, DistanceUnits.Miles);
                        lblDistance.Text = string.Format("{0:N2}", miles) + " mi";
                    }
                    txtTitle.Text = ViewModel.businessProfileObj.Name;
                    lblAddress.Text = ViewModel.businessProfileObj.Address + "," + ViewModel.businessProfileObj.PostCode;
                    imgBanner.Source = Utility.GetImageSourceFromBase64String(ViewModel.businessProfileObj.Banner64String);
                    btnRecommend.Source = ViewModel.businessProfileObj.RecommendedImageSrc;
                }
                else
                {
                    throw new Exception(SystemMessages.NoDetilsFound);
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }

        /// <summary>
        /// Bind Maven List and Business Photo List
        /// </summary>
        private async void BindList(Int64 Id)
        {
            try
            {
                ViewModel.IsBusy = true;

                BusinessProfile businessProfileObj = await ViewModel.GetBusinessProfilePageListItems(ApplicationConfiguration.UserId, Id);
                if (businessProfileObj.ListRecommendadPhotos != null)
                {
                    ctnPhoto.ItemsSource = businessProfileObj.ListRecommendadPhotos;
                }
                if (businessProfileObj.ListRecommendatations != null)
                {
                    lstMavens.ItemsSource = businessProfileObj.ListRecommendatations;
                }
                BindPhotoTab();
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }

        /// <summary>
        /// Method to Show Photo Grid
        /// </summary>
        private void BindPhotoTab()
        {
            stkMavens.IsVisible = false;
            stkPhotos.IsVisible = true;
            stkMap.IsVisible = false;
            imgList.Source = "Logo_Grid_G.png";
            imgPhotos.Source = "Photos_Grid_B.png";
            imgMap.Source = "Map_Grid_G.png";
        }
    #endregion

    
  }



}
