﻿using DurianCode.PlacesSearchBar;
using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MakePost : ContentPage
    {
        #region Properties
        private RecommendationViewModel ViewModel => BindingContext as RecommendationViewModel;
        private MediaFile _mediaFile;
        string image64String;
        public long? SelectedListId { get; set; }
        public string SelectedPlaceId { get; set; }
        public Place PlaceObj { get; set; }

        public Byte[] ImageByteArray { get; set; }
        public ObservableCollection<UserLocationBookmark> UserLocationBookmarksList { get; set; }

        #endregion
        public MakePost()
        {
            InitializeComponent();
            BindingContext = new RecommendationViewModel();
            GetUserBookmarks();
            search_bar.ApiKey = ApplicationConfiguration.ApiKey;
            search_bar.Type = PlaceType.Establishment;
            search_bar.Components = new Components("country:us");
            search_bar.PlacesRetrieved += Search_Bar_PlacesRetrieved;
            search_bar.TextChanged += Search_Bar_TextChanged;
            search_bar.MinimumSearchText = 2;
            results_list.ItemSelected += Results_List_ItemSelected;
        }

        /// <summary>
        /// To slect List to save Post
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstBookmarks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string selected = (e.CurrentSelection.FirstOrDefault() as UserLocationBookmark)?.Name;
            SelectedListId = (e.CurrentSelection.FirstOrDefault() as UserLocationBookmark)?.Id;
            Utility.ShowToastInfo(selected);

        }

        /// <summary>
        /// Method To upload New Poto in post
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnImageUpload_Clicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    Utility.ShowToastInfo(SystemMessages.PickPhoto);
                    return;
                }

                _mediaFile = await CrossMedia.Current.PickPhotoAsync();

                if (_mediaFile == null)
                {
                    return;
                }

                ImageByteArray = File.ReadAllBytes(_mediaFile.Path);
                image64String = Convert.ToBase64String(ImageByteArray);
                btnImageUpload.Source = ImageSource.FromStream(() => new MemoryStream(ImageByteArray));
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }

        }

        /// <summary>
        /// Save New Post
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnPost_Clicked(object sender, EventArgs e)
        {
           
            bool returnedValue = ValidateLoginForm();
            if (returnedValue)
            {
                try
                {
                    ViewModel.IsBusy = true;
                    Recommendatation recommendatationPost = new Recommendatation();
                    #region BusinessProfilePart

                    BusinessProfile businessProfile = new BusinessProfile { PlaceId = SelectedPlaceId };
                    businessProfile = await checkProfileExistOrNot(businessProfile);
                    if (businessProfile != null)
                    {
                        recommendatationPost.BusinessProfileId = businessProfile.Id;
                    }
                    else
                    {
                        businessProfile = new BusinessProfile()
                        {
                            Name = PlaceObj.Name,
                            Address = PlaceObj.FormattedAddress,
                            WebSiteUrl = PlaceObj.Website,
                            Country = PlaceObj.Country.ToString(),
                            Lat = PlaceObj.Latitude,
                            Longi = PlaceObj.Longitude,
                            PostCode = PlaceObj.PostalCode.ToString(),
                            PlaceId = PlaceObj.Place_ID,
                            Isdeleted = 0
                        };
                        long id = await InsertNewRecordInBusinessProfile(businessProfile);
                        recommendatationPost.BusinessProfileId = id;

                    }
                    #endregion

                    #region Recomendatation Part                   

                    recommendatationPost.Description = txtDescription.Text;
                    recommendatationPost.Remarks = txtTags.Text;
                    recommendatationPost.Uid = ApplicationConfiguration.UserId;
                    recommendatationPost.PlaceId = PlaceObj.Place_ID;
                    if (!string.IsNullOrWhiteSpace(image64String))
                    {
                        recommendatationPost.Photo64String = image64String;
                    }
                    #endregion
                    if (recommendatationPost.BusinessProfileId > 0)
                    {
                        bool returnedVal = await ViewModel.SaveRecommendatationPost(recommendatationPost);
                        if (returnedVal)
                        {
                            //Insert new Record in update Tracking 
                            try
                            {
                                MavenUpdateTracking trackingObj = new MavenUpdateTracking()
                                {
                                    Isdeleted = false,
                                    Message = " Recommended " + businessProfile.Name,
                                    UserName = ApplicationConfiguration.Name,
                                    Photostring = image64String,
                                    ProfilePicture = ApplicationConfiguration.ProfilePicture,
                                    UpdateId = ViewModel.Recommendatation.Id,
                                    UpdateType = (int)UpdateCategories.RecommendedPost,
                                    UserId = ApplicationConfiguration.UserId,
                                };
                                await CommonFunctions.SaveMavenUpdateTracking(trackingObj);
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                            //------------------------------------


                            if (SelectedListId > 0)
                            {
                                BusinessProfileBookmarksViewModel businessProfileBookmarksViewModel = new BusinessProfileBookmarksViewModel();
                                BusinessProfileBookmarks businessProfileBookmarks = new BusinessProfileBookmarks();
                                businessProfileBookmarks.BookmarkId = Convert.ToInt64(SelectedListId);
                                businessProfileBookmarks.PlaceId = businessProfile.PlaceId;
                                businessProfileBookmarks.UserId = ApplicationConfiguration.UserId;
                                businessProfileBookmarks.BusinessProfileId = businessProfile.Id;

                                await businessProfileBookmarksViewModel.InsertUpdateBusinessProfileBookmark(businessProfileBookmarks);

                                if (businessProfileBookmarksViewModel.businessProfileBookmarkObj.Id > 0)
                                {
                                    //Insert new Record in update Tracking 
                                    try
                                    {
                                        UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
                                        await userLocationBookmarkViewModel.GetUserLocationBookmarkById((long)SelectedListId);

                                        if (userLocationBookmarkViewModel.UserLocationBookmark != null)
                                        {
                                            MavenUpdateTracking trackingObj = new MavenUpdateTracking()
                                            {
                                                Isdeleted = false,
                                                Message = " Updated  " + userLocationBookmarkViewModel.UserLocationBookmark.Name + " list",
                                                UserName = ApplicationConfiguration.Name,
                                                Photostring = string.Empty,
                                                ProfilePicture = ApplicationConfiguration.ProfilePicture,
                                                UpdateId = userLocationBookmarkViewModel.UserLocationBookmark.Id,
                                                UpdateType = (int)UpdateCategories.UpdateList,
                                                UserId = ApplicationConfiguration.UserId,
                                            };
                                            await CommonFunctions.SaveMavenUpdateTracking(trackingObj);
                                        }
                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }
                                    //------------------------------------
                                    Utility.ShowToastInfo(SystemMessages.OK);
                                    await Navigation.PushModalAsync(new HomeTabbedPage());
                                }
                            }                           

                            // Share Post
                            if (btnShare.IsToggled)
                            {
                                ShareContent(businessProfile.Name, "", businessProfile.WebSiteUrl);
                            }
                            await Navigation.PopAsync();
                        }
                    }
                    else
                    {
                        Utility.ShowToastInfo(SystemMessages.NoResult);
                    }

                }
                catch (Exception ex)
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    Utility.ShowToastInfo(ex.Message);
                }
                finally
                {
                    ViewModel.IsBusy = false;
                }
            }
            else
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(SystemMessages.EnterProperDetails);
            }
         
        }

        /// <summary>
        /// Method To Insert New businessPRofile In DB 
        /// </summary>
        /// <param name="businessProfile"></param>
        /// <returns></returns>
        private async Task<long> InsertNewRecordInBusinessProfile(BusinessProfile businessProfile)
        {
            try
            {
                BusinessProfileViewModel business = new BusinessProfileViewModel();
                businessProfile = await business.InsertUpdateBusinessProfileToDB(businessProfile);
                if (businessProfile != null)
                    return businessProfile.Id;
                else
                    return 0;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }
            return 0;
        }

        /// <summary>
        /// Cancel Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnCancel_Clicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PopAsync();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Handle To Open New List Popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewList_Clicked(object sender, EventArgs e)
        {
            txtListName.Text = string.Empty;
            popupNewList.IsVisible = true;
        }


        /// <summary>
        /// Handle to Close New List Popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCloseNewList_Clicked(object sender, EventArgs e)
        {
            popupNewList.IsVisible = false;
        }

        /// <summary>
        /// Handles Save To List button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSaveToList_Clicked(object sender, EventArgs e)
        {
            bool returnedValue = ValidatePopupItem();

            if (returnedValue)
            {
                try
                {
                    UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
                    UserLocationBookmark userLocationBookmark = new UserLocationBookmark();
                    userLocationBookmark.Name = txtListName.Text;
                    userLocationBookmark.UserId = ApplicationConfiguration.UserId;
                    UserLocationBookmark userLocationBookmarkObj = await userLocationBookmarkViewModel.SaveUserLocationBookmark(userLocationBookmark);
                    if (userLocationBookmarkObj.Id > 0)
                    {
                        UserLocationBookmarksList.Add(userLocationBookmarkObj);
                        lstBookmarks.ItemsSource = UserLocationBookmarksList;
                        Utility.ShowToastInfo(SystemMessages.ListItemAdded);

                        popupNewList.IsVisible = false;


                    }
                }
                catch (Exception ex)
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
                }
            }
        }

        #region Search Place Methods

        /// <summary>
        /// Handles Search Bar Place Retrieve Methods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="result"></param>
        void Search_Bar_PlacesRetrieved(object sender, AutoCompleteResult result)
        {
            results_list.ItemsSource = result.AutoCompletePlaces;
            ViewModel.IsBusy = false;

            if (result.AutoCompletePlaces != null && result.AutoCompletePlaces.Count > 0)
                results_list.IsVisible = true;
        }

        /// <summary>
        /// Handles Search Bar Text change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Search_Bar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.NewTextValue))
            {
                results_list.IsVisible = false;
                ViewModel.IsBusy = true;
            }
            else
            {
                results_list.IsVisible = false;
                ViewModel.IsBusy = false;
            }
        }

        /// <summary>
        /// Handles Result List Item selected event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void Results_List_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                if (e.SelectedItem == null)
                    return;

                ViewModel.IsBusy = true;
                results_list.IsVisible = false;
                search_bar.Text = string.Empty;
                var prediction = (AutoCompletePrediction)e.SelectedItem;
                results_list.SelectedItem = null;

                var place = await Places.GetPlace(prediction.Place_ID, ApplicationConfiguration.ApiKey);

                if (place != null)
                {
                    PlaceObj = place;
                    SelectedPlaceId = place.Place_ID;
                    results_list.IsVisible = false;
                    search_bar.Text = string.Empty;
                    txtLocation.Text = place.Name;
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }

        #endregion


        #region Private Methods
        /// <summary>
        /// Method to get all user bookmark list
        /// </summary>
        private async void GetUserBookmarks()
        {
            try
            {
                UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
                UserLocationBookmarksList = await userLocationBookmarkViewModel.GetUserLocationBookmarkByUserId(ApplicationConfiguration.UserId);
                lstBookmarks.ItemsSource = UserLocationBookmarksList;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }
        }

        /// <summary>
        /// Method to get all user bookmark list
        /// </summary>
        private async Task<BusinessProfile> checkProfileExistOrNot(BusinessProfile businessProfile)
        {
            try
            {
                BusinessProfileViewModel business = new BusinessProfileViewModel();
                businessProfile = await business.GetBusinessProfileByPlaceId(businessProfile);// (ApplicationConfiguration.UserId);
                return businessProfile;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Method to validate Post form
        /// </summary>
        /// <returns></returns>
        private bool ValidateLoginForm()
        {
            if (string.IsNullOrEmpty(txtLocation.Text) || PlaceObj == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method to validate New List Items
        /// </summary>
        /// <returns></returns>
        private bool ValidatePopupItem()
        {
            lblListName.IsVisible = string.IsNullOrWhiteSpace(txtListName.Text);

            if (string.IsNullOrWhiteSpace(txtListName.Text))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method to share Post
        /// </summary>
        /// <param name="title"></param>
        /// <param name="filePath"></param>
        /// <param name="description"></param>
        private async void ShareContent(string title, string filePath, string description)
        {
            await Share.RequestAsync(new ShareTextRequest
            {
                Text = description,
                Title = title
            });

            //if (filePath != string.Empty)
            //{
            //    await Share.RequestAsync(new ShareFileRequest
            //    {
            //        Title = title,
            //        File = new ShareFile(filePath)
            //    });
            //}

        }
        #endregion

    }
}