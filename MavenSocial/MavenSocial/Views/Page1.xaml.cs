﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public ObservableCollection<Card1> ListDetails { get; set; }
        public Page1()
        {
            InitializeComponent();
            ListDetails = new ObservableCollection<Card1>
      {
        new Card1{ ImageSource= "Rooh1.png"},
        new Card1{ ImageSource= "Rooh2.png"},
        new Card1{ ImageSource= "Rooh3.png"}
      };

            BindingContext = this;
        }
        private void txtSearch_Focused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.9;

        }

        private void txtSearch_Unfocused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.3;
        }
    }
    public class Card1
    {
        public string ImageSource { get; set; }
    }
}