﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public ObservableCollection<listNameAndImage> ListDetails { get; set; }
        public Page4()
        {

            InitializeComponent();

            ListDetails = new ObservableCollection<listNameAndImage>()
        {
          new listNameAndImage { imgPhoto= "Ellipse53.png", lblName="Recommended by Max Marmer"},
          new listNameAndImage { imgPhoto= "Ellipse54.png", lblName="Recommended by Taylor James"},
          new listNameAndImage { imgPhoto = "Ellipse55.png",lblName="Recommended by Isabel Scott" },
          new listNameAndImage { imgPhoto = "Ellipse60.png",lblName="Recommended by Dan Troy" },
          new listNameAndImage { imgPhoto = "Ellipse61.png",lblName="Recommended by Zoe Adams" },
          new listNameAndImage { imgPhoto = "Ellipse62.png",lblName="Recommended by Jenna Jones" },
          new listNameAndImage { imgPhoto = "Ellipse63.png",lblName="Recommended by Ryan Ri" },
          new listNameAndImage { imgPhoto = "Ellipse66.png",lblName="Recommended by Tom Cain" },
          new listNameAndImage { imgPhoto = "Ellipse68.png",lblName="Recommended by Kim Kay" },
          new listNameAndImage { imgPhoto = "Ellipse61.png",lblName="Recommended by Ryan Ri" },
          new listNameAndImage { imgPhoto = "Ellipse59.png",lblName="Recommended by Zoe Adams" },
          new listNameAndImage { imgPhoto = "Ellipse58.png",lblName="Recommended by Jim Tan" }
        };

            BindingContext = this;
        }
        private void txtSearch_Focused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.9;

        }

        private void txtSearch_Unfocused(object sender, FocusEventArgs e)
        {
            SearchFrame.Opacity = 0.3;
        }

    }

    public class listNameAndImage
    {
        public string imgPhoto { get; set; }
        public string lblName { get; set; }
    }
}
