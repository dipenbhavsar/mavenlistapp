﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ChangePassword : ContentPage
  {
    #region Properties

    private UserProfileViewModel ViewModel => BindingContext as UserProfileViewModel;

    #endregion

    #region Form Events

    public ChangePassword()
    {
      InitializeComponent();
      BindingContext = new UserProfileViewModel();
    }

    #endregion

    #region Control Events

    private async void tapBack_Tapped(object sender, EventArgs e)
    {
      await Navigation.PopModalAsync();
    }

    private async void btnForgotPassword_Clicked(object sender, EventArgs e)
    {
      bool returnedValue = ValidateLoginForm();
      if (returnedValue)
      {
        try
        {
          ViewModel.IsBusy = true;

          UserProfile userProfile = new UserProfile();
          userProfile.Password = txtCurrentPassword.Text;
          userProfile.ConfirmPassword = txtConfirmPassword.Text;
          userProfile.NewPassword = txtNewPassword.Text;
          userProfile.Id = ApplicationConfiguration.UserId;

          bool returnedVal = await ViewModel.UpdatePasswordById(userProfile);
          if (returnedVal)
          {
            await Navigation.PushModalAsync(new Login());
          }
        }
        catch (Exception ex)
        {
          DependencyService.Get<INotificationSound>().Sound();
          await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
        }
        finally
        {
          ViewModel.IsBusy = false;
        }
      }
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Method to validate login form
    /// </summary>
    /// <returns></returns>
    private bool ValidateLoginForm()
    {
      lblCurrentPassword.IsVisible = string.IsNullOrWhiteSpace(txtCurrentPassword.Text);
      lblConfirmPassword.IsVisible = string.IsNullOrWhiteSpace(txtConfirmPassword.Text);
      lblNewPassword.IsVisible = string.IsNullOrWhiteSpace(txtNewPassword.Text);
      if (string.IsNullOrWhiteSpace(txtCurrentPassword.Text) || string.IsNullOrWhiteSpace(txtConfirmPassword.Text) || string.IsNullOrWhiteSpace(txtNewPassword.Text))
      {
        return false;
      }
      else if (txtConfirmPassword.Text != txtNewPassword.Text)
      {
        lblConfirmPassword.Text = "New Password and Confirm Password does not match";
        lblConfirmPassword.IsVisible = true;
        return false;
      }

      return true;
    }

    #endregion
  }
}