﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpPageFromBottom : Rg.Plugins.Popup.Pages.PopupPage
    {
        BusinessProfile businessProfile = new BusinessProfile();
        public Int64 BookmarkId { get; set; }
        public ObservableCollection<UserLocationBookmark> UserLocationBookmarksList { get; set; }
        public PopUpPageFromBottom()
        {
            InitializeComponent();
        }

        public PopUpPageFromBottom(string popupName, Object obj)
        {
            try
            {
                InitializeComponent();
                if (popupName == "settings")
                {
                    BindSettingPopup();
                }
                else if (popupName == "Bookmark")
                {
                    businessProfile = obj as BusinessProfile;
                    BindBookmarkPopup();
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }

        }

        #region Control Event

        /// <summary>
        /// Method to Hide Settings popup on Background click
        /// </summary>
        /// <returns></returns>
        protected override bool OnBackgroundClicked()
        {
            PopupNavigation.Instance.PopAsync();
            return true;
        }

        /// <summary>
        /// close Settings popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnClose_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        /// <summary>
        /// Add New User Location List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TapGestureRecognizer_AddList(object sender, EventArgs e)
        {
            txtListName.Text = "";
            popupNewList.IsVisible = true;
        }

        /// <summary>
        /// Handles to Close New List Popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btncloseSaveToList_Clicked(object sender, EventArgs e)
        {
            popupNewList.IsVisible = false;

        }

        /// <summary>
        /// Handles to save New list Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSaveToList_Clicked(object sender, EventArgs e)
        {
            bool returnedValue = ValidateLoginForm();

            if (returnedValue)
            {
                try
                {
                    UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
                    UserLocationBookmark userLocationBookmark = new UserLocationBookmark();
                    userLocationBookmark.Name = txtListName.Text;
                    userLocationBookmark.UserId = ApplicationConfiguration.UserId;
                    UserLocationBookmark userLocationBookmarkObj = await userLocationBookmarkViewModel.SaveUserLocationBookmark(userLocationBookmark);
                    if (userLocationBookmarkObj.Id > 0)
                    {
                        if (UserLocationBookmarksList == null)
                            UserLocationBookmarksList = new ObservableCollection<UserLocationBookmark>();
                        UserLocationBookmarksList.Add(userLocationBookmarkObj);
                        cntBookMarkList.ItemsSource = UserLocationBookmarksList;
                        popupNewList.IsVisible = false;
                        Utility.ShowToastInfo(SystemMessages.ListItemAdded);
                    }
                }
                catch (Exception ex)
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    Utility.ShowToastInfo(ex.Message);
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Settings List
        /// </summary>
        private void BindSettingPopup()
        {
            try
            {
                popupBookmark.IsVisible = false;
                popuplayout.IsVisible = true;
                popupNewList.IsVisible = false;
                SettingsViewModel settingsViewModel = new SettingsViewModel();
                lstSettings.ItemsSource = settingsViewModel.BindSettingList();
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }

        /// <summary>
        /// Bind Bookmark Popup List
        /// </summary>
        private async void BindBookmarkPopup()
        {
            try
            {
                UserLocationBookmarkViewModel userLocationBookmarkViewModel = new UserLocationBookmarkViewModel();
                UserLocationBookmarksList = await userLocationBookmarkViewModel.GetUserLocationBookmarkByUserId(ApplicationConfiguration.UserId);
                cntBookMarkList.ItemsSource = UserLocationBookmarksList;
                popupBookmark.IsVisible = true;
                popuplayout.IsVisible = false;
                popupNewList.IsVisible = false;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Method to validate New List Items
        /// </summary>
        /// <returns></returns>
        private bool ValidateLoginForm()
        {
            try
            {
                lblListName.IsVisible = string.IsNullOrWhiteSpace(txtListName.Text);

                if (string.IsNullOrWhiteSpace(txtListName.Text))
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Utility.ShowToastInfo(ex.Message);
            }

            return true;
        }
        #endregion

        private void cntBookMarkList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BookmarkId = Convert.ToInt64((e.CurrentSelection.FirstOrDefault() as UserLocationBookmark)?.Id);
                btnSave.IsEnabled = true;
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }

        /// <summary>
        /// Save List
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSave_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (BookmarkId > 0)
                {
                    BusinessProfileBookmarks businessProfileBookmarks = new BusinessProfileBookmarks();
                    BusinessProfileBookmarksViewModel businessProfileBookmarksViewModel = new BusinessProfileBookmarksViewModel();

                    businessProfileBookmarks.BookmarkId = BookmarkId;
                    businessProfileBookmarks.PlaceId = businessProfile.PlaceId;
                    businessProfileBookmarks.UserId = ApplicationConfiguration.UserId;
                    businessProfileBookmarks.BusinessProfileId = businessProfile.Id;
                    await businessProfileBookmarksViewModel.InsertUpdateBusinessProfileBookmark(businessProfileBookmarks);

                    if (businessProfileBookmarksViewModel.businessProfileBookmarkObj.Id > 0)
                    {

                        Utility.ShowToastInfo(SystemMessages.SavedBusinessProfileToList);
                        await Navigation.PopPopupAsync();
                    }
                    else
                    {
                        Utility.ShowToastInfo(SystemMessages.SomethingWenttoWrong);
                    }
                }
                else
                {
                    Utility.ShowToastInfo(SystemMessages.SelectBookMark);
                }

            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }

        }
    }
}