﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
      
          public MapPage()
          {
              InitializeComponent();
              BindPhotos();

          }

          private void txtSearch_Focused(object sender, FocusEventArgs e)
          {
            SearchFrame.Opacity = 0.9;

          }

          private void txtSearch_Unfocused(object sender, FocusEventArgs e)
          {
            SearchFrame.Opacity = 0.3;
          }
          private void btnLike_Clicked(object sender, EventArgs e)
          {
            btnLike.Source = "Like1.png";
          }


          private void btnPhotos_Clicked(object sender, EventArgs e)
          {
            BindPhotos();
          }

          private void btnMap_Clicked(object sender, EventArgs e)
          {

          }

          private void btnMavensList_Clicked(object sender, EventArgs e)
          {
            btnLike.Source = "Like.png";
            ListPhoto.IsVisible = false;
            ListMavens.IsVisible = true;

            ListMavens.ItemsSource = new ObservableCollection<Mavens>()
                    {
                      new Mavens { imgPhoto= "Ellipse53.png", lblName="Recommended by Max Marmer"},
                      new Mavens { imgPhoto= "Ellipse54.png", lblName="Recommended by Taylor James"},
                      new Mavens { imgPhoto = "Ellipse55.png",lblName="Recommended by Isabel Scott" },
                      new Mavens { imgPhoto = "Ellipse60.png",lblName="Recommended by Dan Troy" },
                      new Mavens { imgPhoto = "Ellipse61.png",lblName="Recommended by Zoe Adams" },
                      new Mavens { imgPhoto = "Ellipse62.png",lblName="Recommended by Jenna Jones" },
                      new Mavens { imgPhoto = "Ellipse63.png",lblName="Recommended by Ryan Ri" },
                      new Mavens { imgPhoto = "Ellipse66.png",lblName="Recommended by Tom Cain" },
                      new Mavens { imgPhoto = "Ellipse68.png",lblName="Recommended by Kim Kay" },
                      new Mavens { imgPhoto = "Ellipse61.png",lblName="Recommended by Ryan Ri" },
                      new Mavens { imgPhoto = "Ellipse59.png",lblName="Recommended by Zoe Adams" },
                      new Mavens { imgPhoto = "Ellipse58.png",lblName="Recommended by Jim Tan" }
                    };
          }

          private void BindPhotos()
          {
            ListPhoto.IsVisible = true;
            ListMavens.IsVisible = false;
            ListPhoto.ItemsSource = new ObservableCollection<Photos>
                        {
                          new Photos{ imgIcon = "Rooh1.png",imgIcon1 = "Rooh4.png", imgIcon2="Rooh7.png"},
                          new Photos{ imgIcon = "Rooh2.png",imgIcon1 = "Rooh5.png", imgIcon2="Rooh8.png"},
                          new Photos{ imgIcon = "Rooh3.png",imgIcon1 = "Rooh6.png"},
                        };
          }

          private void ListMavens_ItemSelected(object sender, SelectedItemChangedEventArgs e)
          {
            Navigation.PushModalAsync(new NavigationPage(new IndividualPosts()));
          }
  }

    public class Photos
    {
      public string imgIcon { get; set; }
      public string imgIcon1 { get; set; }
      public string imgIcon2 { get; set; }
    }

    public class Mavens
    {
      public string imgPhoto { get; set; }
      public string lblName { get; set; }
    }
}