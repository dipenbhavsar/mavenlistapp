﻿using MavenSocial.Common;
using MavenSocial.Models;
using MavenSocial.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MavenSocial.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterUser : ContentPage
    {
        #region Properties

        private UserProfileViewModel ViewModel => BindingContext as UserProfileViewModel;
        private UserProfile _userProfile;

        #endregion

        #region Form Events

        public RegisterUser(UserProfile userProfile = null)
        {
            InitializeComponent();
            _userProfile = userProfile;
            if (_userProfile != null && !string.IsNullOrWhiteSpace(_userProfile.Email))
            {
                txtEmail.Text = _userProfile.Email;
                txtEmail.IsEnabled = false;
            }

            BindingContext = new UserProfileViewModel();
        }

        #endregion

        #region Control Events

        /// <summary>
        /// Handles Sign In button tap event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void tapSignIn_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new Login());
        }

        /// <summary>
        /// Handles Create Account button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnCreateAccount_Clicked(object sender, EventArgs e)
        {
            bool returnedValue = ValidateLoginForm();
            if (returnedValue)
            {
                try
                {
                    ViewModel.IsBusy = true;
                    UserProfile userProfile = _userProfile != null ? _userProfile : new UserProfile();
                    userProfile.Username = txtUserName.Text;
                    userProfile.Email = txtEmail.Text;
                    userProfile.Password = txtPassword.Text;

                    UserProfile userProfileObj = await ViewModel.SaveUserRegistrationInformation(userProfile);
                    if (userProfileObj != null)
                    {
                        
                        ApplicationConfiguration.UserId = userProfileObj.Id;                        
                        await Navigation.PushModalAsync(new EditProfile(userProfileObj));
                    }
                }
                catch (Exception ex)
                {
                    DependencyService.Get<INotificationSound>().Sound();
                    await DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
                }
                finally
                {
                    ViewModel.IsBusy = false;
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method to validate login form
        /// </summary>
        /// <returns></returns>
        private bool ValidateLoginForm()
        {
            lblUsername.IsVisible = string.IsNullOrWhiteSpace(txtUserName.Text);
            lblEmail.IsVisible = string.IsNullOrWhiteSpace(txtEmail.Text);
            lblPassword.IsVisible = string.IsNullOrWhiteSpace(txtPassword.Text);

      if (string.IsNullOrWhiteSpace(txtUserName.Text) || string.IsNullOrWhiteSpace(txtEmail.Text) || string.IsNullOrWhiteSpace(txtPassword.Text))
      {
        return false;
      }

      if (txtUserName.Text.Length <= 3)
      {
        lblUsernameLength.IsVisible = true;
        return false;
      }

      if (!Regex.IsMatch(txtPassword.Text, "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{6,}$"))
      {
        DependencyService.Get<INotificationSound>().Sound();
        DisplayAlert(SystemMessages.MavenSocial,SystemMessages.PasswordValidationMessage, SystemMessages.OK);
        return false;
      }

      if (!Regex.IsMatch(txtEmail.Text, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
      {
        lblEmailValidation.IsVisible = true;
        return false;
      }


      if (!chkTermsAndCondition.IsChecked)
      {
        DependencyService.Get<INotificationSound>().Sound();
        DisplayAlert(SystemMessages.MavenSocial, SystemMessages.TermsAndConditions, SystemMessages.OK);
        return false;
      }

            return true;
        }

        #endregion
    }
}