﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MavenSocial.Converter
{
    public class ColorConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int _value = (int)value;

            if (_value == 1)
            {
                return Color.FromHex("fcf67b"); //yellow
            }
            else if (_value == 2)
            {
                return Color.FromHex("44e3ff"); //blue
            }
            else if (_value == 3)
            {
                return Color.FromHex("ffa826"); //orange
            }

            return Color.Transparent;
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
