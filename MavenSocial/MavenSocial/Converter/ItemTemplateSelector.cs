﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace MavenSocial.Converter
{
  public class ItemTemplateSelector : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value != null && value is bool boolean)
      {
        // See the table of Values under the Remarks section for the code number (this example uses "15")
        // https://docs.microsoft.com/en-us/dotnet/api/system.globalization.numberformatinfo.currencynegativepattern?view=netcore-2.0#remarks

        return (bool)value ? Color.DarkViolet : Color.Black;
      }

      return Color.Black;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
