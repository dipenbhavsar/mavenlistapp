﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace MavenSocial.Converter
{
  public class AlternateRowTemplateProvider : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value != null && value is int number)
      {
        // See the table of Values under the Remarks section for the code number (this example uses "15")
        // https://docs.microsoft.com/en-us/dotnet/api/system.globalization.numberformatinfo.currencynegativepattern?view=netcore-2.0#remarks
                
        return (int)value % 2 == 0 ? Color.LightBlue : Color.White;
      }

      return Color.LightGray;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
