﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace MavenSocial.Converter
{
  public class DateTimeFormatConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value != null && value is DateTime dateTime)
      {
        // See the table of Values under the Remarks section for the code number (this example uses "15")
        // https://docs.microsoft.com/en-us/dotnet/api/system.globalization.numberformatinfo.currencynegativepattern?view=netcore-2.0#remarks

        //NumberFormatInfo nfi = culture.NumberFormat;
        //nfi.NumberDecimalDigits = 2;
        return $"{dateTime.ToString("dd/MM/yyyy")}";
      }

      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
