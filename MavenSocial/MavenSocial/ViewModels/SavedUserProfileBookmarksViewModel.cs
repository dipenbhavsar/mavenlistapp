﻿using MavenSocial.Models;
using MavenSocial.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.ViewModels
{
    public class SavedUserProfileBookmarksViewModel : MainViewModel
    {
        #region SavedUserProfileBookmarks Method
        public async Task<ObservableCollection<SavedUserProfileBookmarks>> GetSavedUserProfileBookmarksByUserId(Int64 userId)
        {
            try
            {

                SavedUserProfileBookmarksService savedUserProfileBookmarksService = new SavedUserProfileBookmarksService(this);
                await savedUserProfileBookmarksService.GetSavedUserProfileBookmarksByUserId(userId);
                if (savedUserProfileBookmarksService.responseStatusObj == null)
                {
                    if (savedUserProfileBookmarksService.savedUserProfileBookmarksList != null)
                    {
                        foreach (SavedUserProfileBookmarks item in savedUserProfileBookmarksService.savedUserProfileBookmarksList)
                        {
                            item.UserName = item.UserName + "'s";
                            item.Locations = item.Locations + " Locations";
                            item.SavedBookmarks = item.SavedBookmarks + " Saved this";
                        }
                        ObservableCollection<SavedUserProfileBookmarks> listSavedUserProfileBookmarksObj = new ObservableCollection<SavedUserProfileBookmarks>(savedUserProfileBookmarksService.savedUserProfileBookmarksList);
                        return listSavedUserProfileBookmarksObj;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    throw new Exception(savedUserProfileBookmarksService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public async Task<SavedUserProfileBookmarks> SaveSavedUserProfileBookmarks(SavedUserProfileBookmarks savedUserProfileBookmarks)
    {
      try
      {
        SavedUserProfileBookmarksService savedUserProfileBookmarksService = new SavedUserProfileBookmarksService(this);
        await savedUserProfileBookmarksService.SaveSavedUserProfileBookmarks(savedUserProfileBookmarks);
        if (savedUserProfileBookmarksService.responseStatusObj == null)
        {
          SavedUserProfileBookmarks savedUserProfileBookmarksObj = savedUserProfileBookmarksService.savedUserProfileBookmarksObj;
          return savedUserProfileBookmarksObj;
        }
        else
        {
          throw new Exception(savedUserProfileBookmarksService.responseStatusObj.ErrorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public async Task<ObservableCollection<RecommendadPhotos>> GetSavedRecommendationPhotosByUserId(long userId)
    {
      try
      {
        SavedUserProfileBookmarksService savedUserProfileBookmarksService = new SavedUserProfileBookmarksService(this);
        await savedUserProfileBookmarksService.GetSavedRecommendationPhotosByUserId(userId);
        if (savedUserProfileBookmarksService.responseStatusObj == null)
        {
          ObservableCollection<RecommendadPhotos> savedRecommendationPhotosList = savedUserProfileBookmarksService.savedRecommendationPhotosList;
          return savedRecommendationPhotosList;
        }
        else
        {
          throw new Exception(savedUserProfileBookmarksService.responseStatusObj.ErrorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    #endregion

  }
}
