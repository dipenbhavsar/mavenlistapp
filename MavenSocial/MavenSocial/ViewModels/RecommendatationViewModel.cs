﻿using MavenSocial.Models;
using MavenSocial.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MavenSocial.Common;

namespace MavenSocial.ViewModels
{
    public class RecommendatationViewModel : MainViewModel
    {
        #region  Properties
        private Recommendatation _recommendatation;

        public Recommendatation Recommendatation
        {
            get { return _recommendatation; }
            set
            {
                _recommendatation = value;

                RaisePropertyChanged(() => Recommendatation);
            }
        }

        #endregion

        #region Recommendatation Method

        /// <summary>
        /// Method To Get recommendation by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task GetRecommendatationDetailById(Int64 id)
        {
            try
            {
                RecommendatationService recommendatationService = new RecommendatationService(this);
        await recommendatationService.GetRecommendatationDetailById(id, ApplicationConfiguration.UserId);
                if (recommendatationService.responseStatusObj == null)
                {
                    Recommendatation recommendatation = recommendatationService.recommendatationObj;

                    Recommendatation = recommendatationService.recommendatationObj;
                }
                else
                {
                    throw new Exception(recommendatationService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Save User Recommendatation Information
        /// </summary>
        /// <param name="Recommendatation"></param>
        /// <returns></returns>
        public async Task<bool> SaveRecommendatationPost(Recommendatation recommendatation)
        {
            try
            {
                RecommendatationService recommendatationService = new RecommendatationService(this);
                await recommendatationService.SaveRecommendatationPost(recommendatation);
                if (recommendatationService.responseStatusObj == null)
                {
                    Recommendatation = recommendatationService.recommendatationObj;
                    return true;
                }
                else
                {
                    throw new Exception(recommendatationService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
