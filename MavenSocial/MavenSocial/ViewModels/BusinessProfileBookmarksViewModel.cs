﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MavenSocial.Models;
using MavenSocial.Services;

namespace MavenSocial.ViewModels
{
  public class BusinessProfileBookmarksViewModel : MainViewModel
  {

    public BusinessProfileBookmarks businessProfileBookmarkObj = null;
    public async Task InsertUpdateBusinessProfileBookmark(BusinessProfileBookmarks businessProfileBookmarks)
    {
      try
      {
        BusinessProfileBookmarksService businessProfileBookmarksService = new BusinessProfileBookmarksService(this);
        await businessProfileBookmarksService.SaveBusinessProfileBookmarks(businessProfileBookmarks);
        if (businessProfileBookmarksService.responseStatusObj == null)
        {
          businessProfileBookmarkObj = businessProfileBookmarksService.businessProfileBookmarksObj;
         // return businessProfileBookmarkObj;
        }
        else
        {
          throw new Exception(businessProfileBookmarksService.responseStatusObj.ErrorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}
