﻿using GalaSoft.MvvmLight;
using MavenSocial.Common;
using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;

namespace MavenSocial.ViewModels
{
  public class MainViewModel : ViewModelBase
  {
    private int _isbusy;
    public bool IsBusy
    {
      get
      {
        return !IsInDesignMode && _isbusy > 0;
      }
      set
      {
        if (value)
          _isbusy++;
        else
          _isbusy--;


        Action action = () => RaisePropertyChanged(() => IsBusy);
        Device.BeginInvokeOnMainThread(action);
      }
    }

    private static bool _isInternetAvailable;

    public bool IsInternetAvailable
    {
      get { return _isInternetAvailable; }
      set
      {
        _isInternetAvailable = value;
        RaisePropertyChanged(() => IsInternetAvailable);
        RaisePropertyChanged(() => UserOnlineImage);
      }
    }

    public ImageSource ProfilePicture
    {
      get
      {
        if (!string.IsNullOrWhiteSpace(ApplicationConfiguration.ProfilePicture))
        {
          byte[] byteData = Convert.FromBase64String(ApplicationConfiguration.ProfilePicture);
          if (byteData != null)
          {
            return ImageSource.FromStream(() => new MemoryStream(byteData));
          }
        }

        return null;
      }
    }

    public bool IsImageAvailable
    {
      get
      {
        return !string.IsNullOrWhiteSpace(ApplicationConfiguration.ProfilePicture);
      }
    }

    public string UserOnlineImage => IsInternetAvailable ? "user_online.png" : "user_offline.png";

        #region Methods
        /// <summary>
        /// Method to Redirect To Page By Target Page Name
        /// </summary>
        /// <param name="targetPageName"></param>
        /// <param name="queryStringPairs"></param>
        public void RedirectToPageByTargetPageName(string targetPageName, string queryStringPairs)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(targetPageName))
                {
                    if (targetPageName.Contains("?"))
                    {
                        string[] splitQueryString = targetPageName.Split('?');
                        targetPageName = splitQueryString[0];
                        queryStringPairs = splitQueryString[1];
                    }

                    INavigation navigation = Application.Current.MainPage.Navigation;
                    Type type = Type.GetType($"EzyB2b.Views.{targetPageName}");
                    object[] arrayParams;
                    List<object> paramList = new List<object>();
                    if (!string.IsNullOrWhiteSpace(queryStringPairs))
                    {
                        string[] queryParams = queryStringPairs.Split(';');
                        if (queryParams != null && queryParams.Length > 0)
                        {
                            foreach (string item in queryParams)
                            {
                                string[] queryParamValues = item.Split('=');
                                if (queryParamValues != null && queryParamValues.Length > 1)
                                {
                                    paramList.Add(queryParamValues[1]);
                                }
                            }
                        }
                    }

                    arrayParams = paramList.ToArray();
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        navigation.PushModalAsync(new NavigationPage((Page)Activator.CreateInstance(type, arrayParams)));
                    }
                    else
                    {
                        navigation.PushModalAsync((Page)Activator.CreateInstance(type, arrayParams));
                    }
                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                Application.Current.MainPage.DisplayAlert(SystemMessages.MavenSocial, ex.Message, SystemMessages.OK);
            }
        }
        #endregion
    }
}
