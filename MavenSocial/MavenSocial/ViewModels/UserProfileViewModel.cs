﻿using MavenSocial.Common;
using MavenSocial.DependencyServices;
using MavenSocial.Models;
using MavenSocial.Services;
using MavenSocial.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MavenSocial.ViewModels
{
    public class UserProfileViewModel : MainViewModel
    {

        #region Properties

        public UserProfile UserProfileObj = null;
        private ObservableCollection<UserProfile> _listOfPhotos;
        private int _height;
        public ObservableCollection<UserProfile> ListOfPhotos
        {
            get { return _listOfPhotos; }
            set
            {

                _listOfPhotos = value;

                RaisePropertyChanged(() => ListOfPhotos);
            }
        }

        private ObservableCollection<UserProfile> _listOfUserProfile;
        public ObservableCollection<UserProfile> ListOfUserProfile
        {
            get { return _listOfUserProfile; }
            set
            {

                _listOfUserProfile = value;

                RaisePropertyChanged(() => ListOfUserProfile);
            }
        }

        #endregion

        #region Contructor

        #endregion

        #region User Profile Methods

        public async Task<bool> DoUserLoginByUserNameAndPassword(UserProfile userProfile)
        {
            try
            {
                UserProfileService userProfileService = new UserProfileService(this);
                await userProfileService.DoUserLoginWithUsernameAndPassowrd(userProfile);
                if (userProfileService.responseStatusObj == null)
                {
                    UserProfile userProfileObj = userProfileService.userProfileObj;
                    if (userProfileObj != null)
                    {
                        ApplicationConfiguration.Email = userProfileObj.Email;
                        ApplicationConfiguration.Name = userProfileObj.Name;
                        ApplicationConfiguration.UserId = userProfileObj.Id;
                        ApplicationConfiguration.Username = userProfile.Username;
                        if (!string.IsNullOrWhiteSpace(userProfileObj.ProfilePictureBase64String) && userProfileObj.ProfilePictureBase64String != "AA==")
                        {
                            ApplicationConfiguration.ProfilePicture = userProfileObj.ProfilePictureBase64String;
                        }

                        return true;
                    }
                    else
                    {
                        throw new Exception(SystemMessages.InvalidUsernamePassword);
                    }
                }
                else
                {
                    throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public async Task<bool> CheckUsernameAvailablity(UserProfile userProfile)
        //{
        //  try
        //  {
        //    UserProfileService userProfileService = new UserProfileService(this);
        //    await userProfileService.CheckUsernameAvailablity(userProfile);
        //    if (userProfileService.responseStatusObj == null)
        //    {
        //      UserProfile userProfileObj = userProfileService.userProfileObj;
        //      if (userProfileObj.noOfAvailableUser > 0)
        //      {
        //        throw new Exception(SystemMessages.AvailableUsername);
        //      }
        //      else
        //      {
        //       // Int64 id = await CreateAccount(userProfile);
        //        return true; 
        //      }
        //    }
        //    else
        //    {
        //      throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
        //    }
        //  }
        //  catch (Exception ex)
        //  {
        //    throw ex;
        //  }
        //}
        //public async Task<Int64> CreateAccount(UserProfile userProfile)
        //{
        //  try
        //  {
        //    UserProfileService userProfileService = new UserProfileService(this);
        //    await userProfileService.CreateAccount(userProfile);
        //    if (userProfileService.responseStatusObj == null)
        //    {
        //      UserProfile userProfileObj = userProfileService.userProfileObj;
        //      if (userProfileObj.Id > 0)
        //      {
        //        //ApplicationConfiguration.Email = userProfileObj.Email;
        //        //ApplicationConfiguration.Name = userProfileObj.Name;
        //        //ApplicationConfiguration.UserId = userProfileObj.Id;
        //        //if (!string.IsNullOrWhiteSpace(userProfileObj.ProfilePictureBase64String))
        //        //{
        //        //  byte[] byteData = Convert.FromBase64String(userProfileObj.ProfilePictureBase64String);
        //        //  if (byteData != null)
        //        //  {
        //        //    ApplicationConfiguration.ProfilePicture = ImageSource.FromStream(() => new MemoryStream(byteData));
        //        //  }
        //        //}

        //        return userProfileObj.Id;
        //      }
        //      else
        //      {
        //        throw new Exception(SystemMessages.RegistrationFailed);
        //      }
        //    }
        //    else
        //    {
        //      throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
        //    }
        //  }
        //  catch (Exception ex)
        //  {
        //    throw ex;
        //  }
        //}

        /// <summary>
        /// Method to get Save User Registration Information
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task<UserProfile> SaveUserRegistrationInformation(UserProfile userProfile)
        {
            try
            {
                UserProfileService userProfileService = new UserProfileService(this);
                await userProfileService.SaveUserRegistrationInformation(userProfile);
                if (userProfileService.responseStatusObj == null)
                {
                    UserProfile userProfileObj = userProfileService.userProfileObj;
                    if (userProfileObj.Id > 0)
                    {
                        return userProfileObj;
                    }
                    else
                    {
                        throw new Exception(SystemMessages.RegistrationFailed);
                    }
                }
                else
                {
                    throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Save User Profile Information
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task<bool> SaveUserProfileInformation(UserProfile userProfile)
        {
            try
            {
                UserProfileService userProfileService = new UserProfileService(this);
                await userProfileService.SaveUserProfileInformation(userProfile);
                if (userProfileService.responseStatusObj == null)
                {
                    UserProfileObj = userProfileService.userProfileObj;
                    return true;
                }
                else
                {
                    throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to get Profile Information By FacebookId
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task<UserProfile> GetUserProfileInformationByFacebookId(UserProfile userProfile)
        {
            try
            {
                UserProfileService userProfileService = new UserProfileService(this);
                await userProfileService.GetUserProfileInformationByFacebookId(userProfile);
                if (userProfileService.responseStatusObj == null)
                {
                    UserProfile userProfileObj = userProfileService.userProfileObj;
                    return userProfileObj;
                }
                else
                {
                    throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to update Password By Id
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task<bool> UpdatePasswordById(UserProfile userProfile)
        {
            try
            {
                UserProfileService userProfileService = new UserProfileService(this);
                await userProfileService.UpdatePasswordById(userProfile);
                if (userProfileService.responseStatusObj == null)
                {
                    //  UserProfile userProfileObj = userProfileService.userProfileObj;
                    return true;
                }
                else
                {
                    throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Get User Profile Information By Id
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task<UserProfile> GetUserProfileInformationById(UserProfile userProfile)
        {
            try
            {
                UserProfileService userProfileService = new UserProfileService(this);
                await userProfileService.GetUserProfileInformationById(userProfile);
                if (userProfileService.responseStatusObj == null)
                {
                    UserProfileObj = userProfileService.userProfileObj;
                }
                else
                {
                    throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
                }

                return UserProfileObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to Get User Profile Information By Id
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public async Task<UserProfile> GetUserProfileInformationByName(string name, Int64 userId)
        {
            try
            {
                UserProfileService userProfileService = new UserProfileService(this);
                await userProfileService.GetUserProfileInformationByName(name, userId);
                if (userProfileService.responseStatusObj == null)
                {
                    ListOfUserProfile = userProfileService.userProfileList;
                }
                else
                {
                    throw new Exception(userProfileService.responseStatusObj.ErrorMessage);
                }

                return UserProfileObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #endregion

        #region Facebook Login Feature

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _message;
        private bool _isLoggedIn;
        private FacebookUser _facebookUser;
        private IFacebookService _facebookService;

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }
        public bool IsLoggedIn
        {
            get
            {
                return _isLoggedIn;
            }
            set
            {
                _isLoggedIn = value;
                OnPropertyChanged(nameof(IsLoggedIn));
            }
        }
        public FacebookUser FacebookUser
        {
            get
            {
                return _facebookUser;
            }
            set
            {
                _facebookUser = value;
                OnPropertyChanged(nameof(FacebookUser));
            }
        }

        public Command FacebookLoginCommand { get; protected set; }
        public Command FacebookLogoutCommand { get; protected set; }

        public UserProfileViewModel()
        {
            _facebookService = DependencyService.Get<IFacebookService>();

            Message = "Xamarin Forms Facebook Login";

            FacebookLoginCommand = new Command(FacebookLogin);
            FacebookLogoutCommand = new Command(FacebookLogout);
        }

        private void FacebookLogin()
        {
            _facebookService?.Login(OnLoginCompleted);
        }
        private void FacebookLogout()
        {
            _facebookService?.Logout();
            IsLoggedIn = false;
        }

        private async void OnLoginCompleted(FacebookUser facebookUser, Exception exception)
        {
            try
            {
                if (exception == null)
                {
                    FacebookUser = facebookUser;
                    IsLoggedIn = true;
                    UserProfile userProfileObj = new UserProfile();
                    userProfileObj.Email = FacebookUser.Email;
                    userProfileObj.FacebookId = FacebookUser.FacebookId;
                    userProfileObj.FacebookAccessToken = FacebookUser.Token;
                    userProfileObj.FacebookProfilePictureUrl = FacebookUser.Picture;
                    userProfileObj.FacebookTokenExpiryDate = FacebookUser.TokenExpiryDate;
                    UserProfile userProfile = await GetUserProfileInformationByFacebookId(userProfileObj);

                    INavigation navigation = Application.Current.MainPage.Navigation;
                    if (userProfile != null)
                    {
                        ApplicationConfiguration.Email = userProfile.Email;
                        ApplicationConfiguration.Name = userProfile.Name;
                        ApplicationConfiguration.UserId = userProfile.Id;
                        if (!string.IsNullOrWhiteSpace(userProfile.ProfilePictureBase64String) && userProfile.ProfilePictureBase64String != "AA==")
                        {
                            ApplicationConfiguration.ProfilePicture = userProfile.ProfilePictureBase64String;
                        }

                        await navigation.PushModalAsync(new HomeTabbedPage());
                    }
                    else
                    {
                        userProfileObj.Name = FacebookUser.FirstName + " " + FacebookUser.LastName;
                        await navigation.PushModalAsync(new RegisterUser(userProfileObj));
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                DependencyService.Get<INotificationSound>().Sound();
                await App.Current.MainPage.DisplayAlert(SystemMessages.MavenSocial, ex.Message, "OK");
            }
        }

        #endregion

    }
}

