﻿using MavenSocial.Models;
using MavenSocial.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.ViewModels
{
    class UserRelationsViewModel : MainViewModel
    {
        #region Properties
        public Relations relationsObj = null;
        #endregion

        public UserRelationsViewModel()
        {

        }
        public UserRelationsViewModel(Relations relations)
        {
            this.relationsObj = relations;
        }

        #region User Relations method
        public async Task<bool> SaveUserRelations(Relations relations)
        {
            try
            {
                RelationsService relationsService = new RelationsService(this);
                await relationsService.SaveUserRelations(relations);
                if (relationsService.responseStatusObj == null)
                {
                    relationsObj = relationsService.relationsObj;
                    return true;
                }
                else
                {
                    throw new Exception(relationsService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
