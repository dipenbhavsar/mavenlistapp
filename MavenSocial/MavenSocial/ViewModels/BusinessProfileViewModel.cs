﻿using MavenSocial.Models;
using MavenSocial.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.ViewModels
{
    class BusinessProfileViewModel : MainViewModel
    {
        #region Properties

        public BusinessProfile businessProfileObj = null;
        public List<BusinessProfile> businessProfileList = null;
        public BusinessProfileResponse businessProfileResponse = null;

        public BusinessProfileViewModel()
        {

        }
        public BusinessProfileViewModel(BusinessProfile businessProfileObj)
        {
            this.businessProfileObj = businessProfileObj;
        }

        public BusinessProfileViewModel(List<BusinessProfile> businessProfileList)
        {
            this.businessProfileList = businessProfileList;
        }

        public BusinessProfileViewModel(BusinessProfileResponse businessProfileResponse)
        {
            this.businessProfileResponse = businessProfileResponse;
        }

        private ObservableCollection<BusinessProfile> _listOfPhotos;
        public ObservableCollection<BusinessProfile> ListOfPhotos
        {
            get { return _listOfPhotos; }
            set
            {

                _listOfPhotos = value;

                RaisePropertyChanged(() => ListOfPhotos);
            }
        }

        private ObservableCollection<BusinessProfile> _listOfMaven;
        public ObservableCollection<BusinessProfile> ListOfMaven
        {
            get { return _listOfMaven; }
            set
            {

                _listOfMaven = value;

                RaisePropertyChanged(() => ListOfMaven);
            }
        }

        #endregion

        #region Business Profile Methods

        public async Task<BusinessProfile> GetBusinessProfileDetailsByID(BusinessProfile businessProfile)
        {
            try
            {
                BusinessProfileService businessProfileService = new BusinessProfileService(this);
                await businessProfileService.GetBusinessProfileDetailsByID(businessProfile);
                if (businessProfileService.responseStatusObj == null)
                {
                    businessProfileObj = businessProfileService.businessProfileObj;
                    return businessProfileObj;

                }
                else
                {
                    throw new Exception(businessProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<BusinessProfile>> GetAllBusinessProfileByFilter(Google data)
        {
            try
            {
                BusinessProfileService businessProfileService = new BusinessProfileService(this);
                await businessProfileService.GetAllBusinessProfileDetailsByFilter(data);
                if (businessProfileService.responseStatusObj == null)
                {
                    businessProfileList = businessProfileService.businessProfileList;
                    return businessProfileList;

                }
                else
                {
                    throw new Exception(businessProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<BusinessProfileResponse> GetAllBusinessProfileByPageFilter(Google data)
        {
            try
            {
                BusinessProfileService businessProfileService = new BusinessProfileService(this);
                await businessProfileService.GetAllBusinessProfileDetailsByPageFilter(data);
                if (businessProfileService.responseStatusObj == null)
                {
                    businessProfileResponse = businessProfileService.businessProfileResponse;
                    //BusinessProfileResponse businessProfileObj = new BusinessProfileResponse()
                    //{
                    //    BusinessProfiles = businessProfileService.businessProfileResponse.BusinessProfiles,
                    //    NextPageToken = businessProfileService.businessProfileResponse.NextPageToken
                    //};
                    return businessProfileResponse;

                }
                else
                {
                    throw new Exception(businessProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<BusinessProfile> GetBusinessProfilePageListItems(Int64 userId, Int64 businessProfileId)
        {
            try
            {
                BusinessProfileService businessProfileService = new BusinessProfileService(this);
                await businessProfileService.GetBusinessProfilePageListItems(userId, businessProfileId);
                if (businessProfileService.responseStatusObj == null)
                {
                    //businessProfileObj = businessProfileService.businessProfileObj;
                    BusinessProfile businessProfile = businessProfileService.businessProfileObj;
                    return businessProfile;
                }
                else
                {
                    throw new Exception(businessProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<BusinessProfile> GetBusinessProfileByPlaceId(BusinessProfile businessProfile)
        {
            try
            {
                BusinessProfileService businessProfileService = new BusinessProfileService(this);
                await businessProfileService.GetBusinessProfileByPlaceId(businessProfile);
                if (businessProfileService.responseStatusObj == null)
                {
                    businessProfileObj = businessProfileService.businessProfileObj;
                    return businessProfileObj;

                }
                else
                {
                    throw new Exception(businessProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<BusinessProfile> InsertUpdateBusinessProfileToDB(BusinessProfile businessProfile)
        {
            try
            {
                BusinessProfileService businessProfileService = new BusinessProfileService(this);
                await businessProfileService.InsertUpdateBusinessProfileToDB(businessProfile);
                if (businessProfileService.responseStatusObj == null)
                {
                    businessProfileObj = businessProfileService.businessProfileObj;
                    return businessProfileObj;

                }
                else
                {
                    throw new Exception(businessProfileService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    #endregion

    #region Private Method 


    //public void BindListOfPhotos()
    //{
    //  ListOfPhotos = new ObservableCollection<BusinessProfile>()
    //  {
    //      new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      new BusinessProfile{ imgIcon = "Rooh4.png" },
    //      //new BusinessProfile{ imgIcon="Rooh7.png"},
    //      //new BusinessProfile { imgIcon = "Rooh2.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh4.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh4.png" },
    //      //new BusinessProfile{ imgIcon="Rooh7.png"},
    //      //new BusinessProfile { imgIcon = "Rooh2.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh4.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh4.png" },
    //      //new BusinessProfile{ imgIcon="Rooh7.png"},
    //      //new BusinessProfile { imgIcon = "Rooh2.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh4.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh4.png" },
    //      //new BusinessProfile{ imgIcon="Rooh7.png"},
    //      //new BusinessProfile { imgIcon = "Rooh2.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{imgIcon = "Rooh5.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh8.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh3.png"},
    //      //new BusinessProfile{imgIcon = "Rooh6.png"},
    //      //new BusinessProfile{ imgIcon = "Rooh1.png" },
    //      //new BusinessProfile{ imgIcon = "Rooh4.png" },
    //  };
    //}

    //public void BindMavensList()
    //{
    //  ListOfMaven = new ObservableCollection<BusinessProfile>()
    //  {
    //      new BusinessProfile{ ProfilePhoto = "Ellipse59.png",UserName = "Max Marmer"},
    //      new BusinessProfile{ ProfilePhoto = "Ellipse54.png",UserName = "Taylor James"},
    //      new BusinessProfile{ ProfilePhoto = "Ellipse55.png",UserName = "Dan Troy"},
    //  };
    //}



    #endregion

  }

}

