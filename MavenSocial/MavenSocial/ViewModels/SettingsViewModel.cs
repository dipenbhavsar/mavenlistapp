﻿using MavenSocial.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MavenSocial.ViewModels
{
  public class SettingsViewModel
  {
    public ObservableCollection<Settings> BindSettingList()
    {
      ObservableCollection<Settings> ListOfSettings = new ObservableCollection<Settings>()
      {
          new Settings{ Icon = "Ellipse59.png",Name = "Switch Accounts"},
          new Settings{ Icon = "Ellipse54.png",Name = "Edit Profile"},
          new Settings{ Icon = "Ellipse55.png",Name = "Claim a Business"},
          new Settings{ Icon = "Ellipse59.png",Name = "Privacy"},
          new Settings{ Icon = "Ellipse54.png",Name = "Security"},
          new Settings{ Icon = "Ellipse55.png",Name = "Notifications"},
          new Settings{ Icon = "Ellipse59.png",Name = "Ads"},
          new Settings{ Icon = "Ellipse54.png",Name = "Account"},
          new Settings{ Icon = "Ellipse55.png",Name = "Help"},
          new Settings{ Icon = "Ellipse55.png",Name = "About"}
      };

      return ListOfSettings;
    }
  }
}
