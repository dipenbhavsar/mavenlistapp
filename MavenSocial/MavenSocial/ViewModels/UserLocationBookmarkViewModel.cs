﻿using MavenSocial.Models;
using MavenSocial.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.ViewModels
{
    public class UserLocationBookmarkViewModel : MainViewModel
    {
        #region  Properties
        private UserLocationBookmark _userLocationBookmark;
        public ObservableCollection<UserLocationBookmark> userLocationBookmarksList = null;
        public UserLocationBookmark UserLocationBookmark
        {
            get { return _userLocationBookmark; }
            set
            {
                _userLocationBookmark = value;

                RaisePropertyChanged(() => UserLocationBookmark);
            }
        }

        

        #endregion

        #region UserLocationBookmark Method
        public async Task<ObservableCollection<UserLocationBookmark>> GetUserLocationBookmarkByUserId(Int64 userId)
        {
            try
            {

                UserLocationBookmarkService userLocationBookmarkService = new UserLocationBookmarkService(this);
                await userLocationBookmarkService.GetUserLocationBookmarkByUserId(userId);
                if (userLocationBookmarkService.responseStatusObj == null)
                {
                    if (userLocationBookmarkService.userLocationBookmarkList != null)
                    {
                        foreach (UserLocationBookmark item in userLocationBookmarkService.userLocationBookmarkList)
                        {
                            item.Locations = item.Locations + " Locations";
                            item.SavedBookmarks = item.SavedBookmarks + " Saved this";
                        }
                        userLocationBookmarksList = new ObservableCollection<UserLocationBookmark>(userLocationBookmarkService.userLocationBookmarkList);
                        return userLocationBookmarksList;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    throw new Exception(userLocationBookmarkService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task GetUserLocationBookmarkById(Int64 id)
        {
            try
            {

                UserLocationBookmarkService userLocationBookmarkService = new UserLocationBookmarkService(this);
                await userLocationBookmarkService.GetUserLocationBookmarkById(id);
                if (userLocationBookmarkService.responseStatusObj == null)
                {
                    UserLocationBookmark userLocationBookmarkObj = userLocationBookmarkService.userLocationBookmarkObj;
                    int count = 1;
                    foreach (var item in userLocationBookmarkObj.businessProfiles)
                    {
                        item.RowNo = count++;
                    }
                    UserLocationBookmark = userLocationBookmarkObj;
                }
                else
                {
                    throw new Exception(userLocationBookmarkService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UserLocationBookmark> SaveUserLocationBookmark(UserLocationBookmark userLocationBookmark)
        {
            try
            {
                UserLocationBookmarkService userLocationBookmarkService = new UserLocationBookmarkService(this);
                await userLocationBookmarkService.SaveUserLocationBookmark(userLocationBookmark);
                if (userLocationBookmarkService.responseStatusObj == null)
                {
                    UserLocationBookmark userLocationBookmarkObj = userLocationBookmarkService.userLocationBookmarkObj;
                    return userLocationBookmarkObj;
                }
                else
                {
                    throw new Exception(userLocationBookmarkService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
