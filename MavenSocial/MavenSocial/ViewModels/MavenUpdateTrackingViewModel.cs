﻿using MavenSocial.Models;
using MavenSocial.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace MavenSocial.ViewModels
{
    class MavenUpdateTrackingViewModel :MainViewModel
    {
        #region Properties
        public MavenUpdateTracking trackingObj = null;
    private ObservableCollection<MavenUpdateTracking> _listOfTracking;

    public ObservableCollection<MavenUpdateTracking> ListOfTracking
    {
      get { return _listOfTracking; }
      set
      {
        _listOfTracking = value;

        RaisePropertyChanged(() => ListOfTracking);
      }
    }

    #endregion

    public MavenUpdateTrackingViewModel()
        {

        }
        public MavenUpdateTrackingViewModel(MavenUpdateTracking tracking)
        {
            this.trackingObj = tracking;
        }

        #region User Relations method
        public async Task<MavenUpdateTracking> SaveMavenUpdateTracking(MavenUpdateTracking tracking)
        {
            try
            {
                MavenUpdateTrackingService trackingService = new MavenUpdateTrackingService(this);
                await trackingService.SaveMavenUpdateTracking(tracking);
                if (trackingService.responseStatusObj == null)
                {
                    trackingObj = trackingService.trackingObj;
                    return trackingObj;
                }
                else
                {
                    throw new Exception(trackingService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public async Task<ObservableCollection<MavenUpdateTracking>> GetMavenUpdateTracking(long userId)
    {
      try
      {
        MavenUpdateTrackingService trackingService = new MavenUpdateTrackingService(this);
        await trackingService.GetMavenUpdateTrackingByUserId(userId);
        if (trackingService.responseStatusObj == null)
        {
          ListOfTracking = trackingService.trackingList;
          return ListOfTracking;
        }
        else
        {
          throw new Exception(trackingService.responseStatusObj.ErrorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    #endregion
  }
}
