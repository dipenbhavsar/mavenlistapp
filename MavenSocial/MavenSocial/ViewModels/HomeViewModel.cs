﻿using MavenSocial.Common;
using MavenSocial.DependencyServices;
using MavenSocial.Models;
using MavenSocial.Services;
using MavenSocial.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MavenSocial.ViewModels
{
    class HomeViewModel : MainViewModel
    {
        #region Private Members

        private ObservableCollection<HomeNotification> _listOfHomeNotification;

        #endregion

        #region Properties
        public ObservableCollection<HomeNotification> ListOfHomeNotification
        {
            get { return _listOfHomeNotification; }
            set
            {
                _listOfHomeNotification = value;

                RaisePropertyChanged(() => ListOfHomeNotification);
            }
        }
        #endregion

        #region Contructor

        #endregion

        public async Task<ObservableCollection<HomeNotification>> GetAllNotificationByUserId(HomeNotification data)
        {
            try
            {
                HomeService homeNotificationService = new HomeService(this);
                await homeNotificationService.GetAllHomeNotificationByUserId(data);
                if (homeNotificationService.responseStatusObj == null)
                {
                      //foreach (HomeNotification homeNotification in homeNotificationService.homeNotificationObjList)
                      //{
                      //    if (homeNotification.Profile64String == "AA==" || string.IsNullOrEmpty(homeNotification.Profile64String))
                      //    {
                      //      //  string imagePath = "F:\\WpProj\\MavenListApp\MavenSocial\MavenSocial.Android\Resources\Logo_Purple.png";
                      //      //  byte[] imageBytes = System.IO.File.ReadAllBytes(imagePath);
                      //     //   homeNotification.Profile64String = Convert.ToBase64String(imageBytes);
                      //       // return base64String;
                      //        // = "Logo_Purple.png";
                      //    }
                      //}
                    ListOfHomeNotification = homeNotificationService.homeNotificationObjList;
                    return ListOfHomeNotification;
                }
                else
                {
                    throw new Exception(homeNotificationService.responseStatusObj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
