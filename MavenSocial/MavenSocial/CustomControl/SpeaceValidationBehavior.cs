﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace MavenSocial.CustomControl
{
  public class SpeaceValidationBehavior : Behavior<Entry>
  {

      //Here we have added the characters we wanted to restrict while entering data
      private const string SpecialCharacters = @" ";
      protected override void OnAttachedTo(Entry entry)
      {
        entry.TextChanged += OnEntryTextChanged;
        base.OnAttachedTo(entry);
      }

      protected override void OnDetachingFrom(Entry entry)
      {
        entry.TextChanged -= OnEntryTextChanged;
        base.OnDetachingFrom(entry);
      }

      //In this behaviour you can see that we are just validating that
      //no restricted characters gets to entry input.

        private static void OnEntryTextChanged(object sender, TextChangedEventArgs args)
      {
        if (!string.IsNullOrWhiteSpace(args.NewTextValue))
        {
          bool isValid = args.NewTextValue.ToCharArray().All(x => !SpecialCharacters.Contains(x));

          ((Entry)sender).Text = isValid ? args.NewTextValue : args.NewTextValue.Remove(args.NewTextValue.Length - 1);
        }
      }
    
  }
}
