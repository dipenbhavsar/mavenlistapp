﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    public class FacebookUser
    {
        #region Constructor

        public FacebookUser(string id, string token, string firstName, string lastName, string email, string imageUrl, DateTime tokenExpDate)
        {
            FacebookId = id;
            Token = token;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Picture = imageUrl;
            TokenExpiryDate = tokenExpDate;
        }

        #endregion

        #region Properties

        public string FacebookId { get; set; }

        public string Token { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Picture { get; set; }

        public DateTime? TokenExpiryDate { get; set; }

        #endregion
    }
}
