﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
  public class UserLocationBookmark : ObservableObject
  {
    #region Properties
    public Int64 Id { get; set; }
    public string Name { get; set; }
    public Int64 UserId { get; set; }
    public bool IsDeleted { get; set; }
    public DateTime EntryDate { get; set; }
    public string Locations { get; set; }
    public string SavedBookmarks { get; set; }
    public List<BusinessProfile> businessProfiles { get; set; }
    public bool IsSaved { get; set; }
    public string SavedImageSrc { get; set; }

    #endregion
  }
}
