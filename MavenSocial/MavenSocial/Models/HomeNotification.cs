﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    class HomeNotification
    {
        public Int64 Uid { get; set; }
        public Int64 Rid { get; set; }
        public string UserName { get; set; }
        public string BusinessName { get; set; }
        public string Description { get; set; }
        public string PostTime { get; set; }
        public string Photo64String { get; set; }
        public string Profile64String { get; set; }
        public bool HasBusinessProfileImage { get; set; }
  }
}
