﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    public class MavenUpdateTracking
    {
        #region Properties
        public Int64 Id { get; set; }
        public Int64 UserId { get; set; }
        public string UserName { get; set; }
        public string ProfilePicture { get; set; }
        public int UpdateType { get; set; }
        public string Message { get; set; }
        public Int64 UpdateId { get; set; }
        public string Photostring { get; set; }
        public DateTime EntryDate { get; set; }
        public bool Isdeleted { get; set; }
        public string Notification { get; set; }
        public string PostTime { get; set; }
        public bool HasBusinessProfileImage { get; set; }
        #endregion
    }

    public enum UpdateCategories
    {
        RecommendedPost = 1,
        SavedList = 2,
        Follow = 3,
        UpdateList = 4,

    }

}
