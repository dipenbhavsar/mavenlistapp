﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    public class Google
    {
        public string Location { get; set; }
        public string PlaceName { get; set; }
        public string ApiKey { get; set; }
        public string NextPageToken { get; set; }
        public int RecomendedBy { get; set; }  // 0 = friends & 1 = Public 
        public int UserId { get; set; }
    }
}
