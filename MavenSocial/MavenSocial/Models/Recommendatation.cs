﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    public class Recommendatation : ObservableObject
    {
        #region Properties
        public Int64 Id { get; set; }
        public Int64 Uid { get; set; }
        public Int64 BusinessProfileId { get; set; }
        public string UserName { get; set; }
        public string Remarks { get; set; }
        public string Description { get; set; }
        public DateTime EntryDate { get; set; }
        public string Photo64String { get; set; }
        public BusinessProfile businessProfile { get; set; }
        public List<RecommendadPhotos> recommendadPhotos { get; set; }
        public string PlaceId { get; set; }
        public long RCount { get; set; }
        #endregion
    }
}
