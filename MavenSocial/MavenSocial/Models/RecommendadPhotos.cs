﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
  public class RecommendadPhotos : ObservableObject
  {
    #region Properties
    public Int64 Id { get; set; }
    public Int64 Rid { get; set; }
    public DateTime EntryDate { get; set; }
    public string ListPhoto64String { get; set; }
    public Int64 BookmarkId { get; set; }
    public Int64 BusinessProfileId { get; set; }
    public Int64 UserId { get; set; }
    #endregion
  }
}
