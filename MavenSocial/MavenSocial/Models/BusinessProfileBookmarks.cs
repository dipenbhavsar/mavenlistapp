﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
  public class BusinessProfileBookmarks : ObservableObject
  {
    #region Properties
    public Int64 Id { get; set; }
    public Int64 BookmarkId { get; set; }
    public string PlaceId { get; set; }
    public bool IsDeleted { get; set; }
    public DateTime EntryDate { get; set; }
    public Int64 UserId { get; set; }
    public Int64 BusinessProfileId { get; set; }
    #endregion
  }
}
