﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
   public class Relations
    {
        public Int64 Id { get; set; }
        public Int64 Follower_id { get; set; }
        public Int64 Following_id { get; set; }
        public int Status { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime EntryDate { get; set; }
    }
}
