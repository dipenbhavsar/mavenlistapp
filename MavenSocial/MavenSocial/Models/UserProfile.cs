﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    public class UserProfile
    {
        #region Properties
        public Int64 Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ContactNo { get; set; }
        public string WebSiteUrl { get; set; }
        public string PersonalBio { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public byte[] ProfilePicture { get; set; }
        public string ProfilePictureBase64String { get; set; }
        // public int noOfAvailableUser { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public int Following { get; set; }
        public int Followers { get; set; }
        public Int64 CurrentUserId { get; set; }
        public bool IsFollowing { get; set; }
        public List<RecommendadPhotos> RecommendadPhotosList { get; set; }
        #endregion

        #region Facebook Properties

        public string FacebookId { get; set; }
        public string FacebookAccessToken { get; set; }
        public string FacebookProfilePictureUrl { get; set; }
        public DateTime? FacebookTokenExpiryDate { get; set; }

        #endregion

        #region  temporary filed for Bind Grid
        public string imgIcon { get; set; }
        public string imgIcon1 { get; set; }
        public string imgIcon2 { get; set; }
        #endregion
    }


}
