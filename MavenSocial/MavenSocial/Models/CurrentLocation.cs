﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    public class CurrentLocation
    {
        public  double Longitude { get; set; }
        public  double Latitude { get; set; }
    }
}
