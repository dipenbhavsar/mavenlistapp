﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
  public class SavedUserProfileBookmarks : ObservableObject
  {
    public Int64 Id { get; set; }
    public Int64 BookmarkId { get; set; }
    public Int64 UserId { get; set; }
    public bool IsDeleted { get; set; }
    public DateTime EntryDate { get; set; }
    public string Locations { get; set; }
    public string SavedBookmarks { get; set; }
    public string UserName { get; set; }
    public string BookmarkName { get; set; }
  }
}
