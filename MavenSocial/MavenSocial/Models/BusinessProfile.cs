﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
    public class BusinessProfile
    {
        #region Properties
        public Int64 Id { get; set; }
        public Int64 RowNo { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public string cuisine { get; set; }
        public DateTime? OpenTime { get; set; }
        public DateTime? CloseTime { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ContactNo { get; set; }
        public string PostCode { get; set; }
        public string WebSiteUrl { get; set; }

        // public byte[] ProfilePicture { get; set; }
        public string Banner64String { get; set; }
        public Int32 Isdeleted { get; set; }
        public string KeyWord { get; set; }
        public double? Lat { get; set; }
        public double? Longi { get; set; }
        public string PlaceId { get; set; }
        public Int64 RecommendatationCount { get; set; }
        public bool IsSaved { get; set; }
    public bool IsRecommended { get; set; }
    public string RecommendedImageSrc { get; set; }
    public List<Recommendatation> ListRecommendatations { get; set; }
    public List<RecommendadPhotos> ListRecommendadPhotos { get; set; }
    #endregion

  }
  public class BusinessProfileResponse
    {
        public string NextPageToken { get; set; }
        public List<BusinessProfile> BusinessProfiles { get; set; }
    }


 
}
