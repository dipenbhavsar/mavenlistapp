﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace MavenSocial.Models
{
  public class Settings : ObservableObject
  {
    public string Icon { get; set; }
    public string Name { get; set; }
  }
}
